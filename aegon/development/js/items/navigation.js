$(function() {

	var overlayer = {
	  show: function() {

	    $('body').addClass('overlayer-showing');

	    setTimeout(function() {

	      $('body')
	        .removeClass('overlayer-showing')
	        .addClass('overlayer-showed');

	    }, 300);

	  },

	  hide: function() {

	    $('body')
	      .removeClass('overlayer-showed')
	      .addClass('overlayer-hidding');

	    setTimeout(function() {

	      $('body').removeClass('overlayer-hidding');

	    }, 300);

	  }
	}

  function menuTrigger() {

  	var header = document.getElementById('header'),
  		menuItem = document.getElementById('menu-trigger');

    if(menuItem === null) {
      return;
    }

  	var headerWidth = header.clientWidth;
  	var itemWidth = menuItem.clientWidth;
  	var menuTranslated = headerWidth - itemWidth;

    $('#menu-trigger').click(function () {

    	$("#navigation").css('width', menuTranslated);

    	if ($( this ).css( 'transform' ) == 'none' ) {
    		$('#wrap-hide-menu').css('transform','translate(' + menuTranslated + 'px)');
          $(this).css('transform','translate(' + menuTranslated + 'px)');
      } else {
      	$('#wrap-hide-menu').css('transform', '');
          $(this).css('transform','');
      }

      var menuTrigger = $('#menu-trigger');
      if(menuTrigger.hasClass('menu-opened')) {
        overlayer.hide();
        menuTrigger.removeClass('menu-opened');
      } else {
        overlayer.show();
        menuTrigger.addClass('menu-opened');
      }
    });

  }

  menuTrigger();

});
