requirejs.config({
    baseUrl: 'js'
});

// Start the main app logic.
requirejs(['shame']);
requirejs(['modules/tabs']);
requirejs(['modules/collapse']);
requirejs(['modules/header']);
requirejs(['modules/number-input']);
requirejs(['modules/discount']);
requirejs(['modules/modal-contratar']);
requirejs(['items/navigation']);
requirejs(['items/tables']);

