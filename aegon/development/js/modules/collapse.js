$(function(){
  $('.collapse').each(function() {
    var $module = $(this);
    var userInteraction = true;
    $(this).on('click', '.collapse-header', function() {

      // Deshabilitamos la funcionalidad hasta que las animaciones acaben
      if (!userInteraction) return;
      userInteraction = false;

      var $items = $(this).closest('.collapse').find('.collapse-item');
      var $itemsContent = $items.find('.collapse-content');
      var $item = $(this).closest('.collapse-item');
      var $itemContent = $item.find('.collapse-content');

      var openedItems = ($module.find('.collapse-item.opened').length) ? true : false;


      if($item.hasClass('collapsed')) { // si el item está cerrado, lo abrrimos
        if(openedItems) {     // si hay algun item abierta
          $itemsContent.slideUp(400, function() { // Cerramos los otros items primero
            setTimeout(function() { // establecemos un timeOut para que al abrir el item, las otros ya estén cerrados
              $items.removeClass('opened').addClass('collapsed');
              $itemContent.slideDown(400, function() {  // abrimos el item
                $item.removeClass('collapsed').addClass('opened');
              });
                userInteraction = true;
                return;
            }, 400);
          });  
        } else {             // si no hay ningún item abierto
          $itemContent.slideDown(400, function() { // abrimos el item
            $item.removeClass('collapsed').addClass('opened');  
            userInteraction = true;
            return;
          });
        }
        
      } else {  // si el item está abierto, lo cerramos
        console.log('foo');
        $itemContent.slideUp(400, function() {
          $items.removeClass('opened').addClass('collapsed');
          userInteraction = true;
          return;
        })
      }
      
    });
  });
  
});