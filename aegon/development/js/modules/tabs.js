(function() {
	$(document).ready(function() {

    $('.tabs-menu li a').click(function(event) {
        event.preventDefault();
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');

        var tab = $(this).attr("href");
        $('.tab-content').not(tab).addClass('hide');
        $(tab).removeClass('hide');
    });

    $('[data-fixed-until]').each(function() {
    	var $el = $(this);
    	function yPos() {
    		var $target = $($el.attr('data-fixed-until'))
    		return $target.offset().top - $(window).height() + $target.height();
    	}
    	function compare(yPosElement) {
    		if (yPosElement > $(window).scrollTop()) {
    			$el.removeClass('hide');
    		} else {
    			$el.addClass('hide');
    		}
    	}
    	compare(yPos());
    	$(window).scroll(function() {
    		compare(yPos());
    	})
    	
    })



	});
})();
