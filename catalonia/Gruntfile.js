// Generated  using Front-End-Sass Generator https://github.com/Frodigo/front-end-generator

'use strict';

var path = require('path');

module.exports = function(grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Configurable paths for the application
  var appConfig = {
    app: 'development',
    dist: 'dist'
  };

  grunt.initConfig({

    // Project settings
    yeoman: appConfig,


    // browser sync task

    browserSync: {
      bsFiles: {
        src : ['build/**/*.{html,css,jpg,png,svg,js}']
      },
      options: {
        watchTask: true,
        server: './build',
      },

    },

    // sass task

    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'build/css/styles.css': 'development/sass/styles.scss'
        }
      }
    },

    // post css task

    postcss: {
      options: {
        map: true, // inline sourcemaps
        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({browsers: 'last 5 versions'}), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: 'build/css/*.css'
      }
    },

    // sass doc

    sassdoc: {
      default: {
        src: 'development/sass',
        options: {
          dest: 'development/docs/sass'
        }
      }
    },

    // js doc

    jsdoc : {
      dist : {
        src: ['development/js/*.js', 'development/test/*.js'],
        options: {
          destination: 'development/docs/js'
        }
      }
    },

    // wiredep task

    wiredep: {
      task: {
        src: [
          'development/**/*.html',
          'development/sass/styles.scss'
        ]
      }
    },

    // jade task

    jade: {
      compile: {
        options: {
          client: false,
          pretty: true,
          data: {}
        },
        files: [ {
          cwd: "development/views/",
          src: "*.jade",
          dest: "build/",
          expand: true,
          ext: ".html"
        } ]
      },
      default: {
        options: {
          client: false,
          pretty: true,
          data: {}
        },
        files: [ {
          cwd: "development/views/",
          src: "*.jade",
          dest: "dist/",
          expand: true,
          ext: ".html"
        } ]
      }
    },

    // clean task

    clean: {
      default: {
        files: [
          {
            dot: true,
            src: [
              'dist/**/*'
            ]
          }
        ]
      }
    },

    //requirejs task

    requirejs: {
      compile: {
        options: {
          baseUrl: 'development/js/',
          mainConfigFile: 'development/js/main.js',
          name: 'config',
          out: 'dist/js/main.min.js',
          optimizer: 'none',
          preserveLicenseComments: false,
        }
      },
      join: {
        options: {
          optimize: "none",
          baseUrl: 'development/js/',
          mainConfigFile: 'development/js/main.js',
          name: 'config',
          out: 'dist/js/main.js',
          optimizer: 'none'
        }
      }
    },

    // concat vendor js & css

    concat: {
      css: {
        src: [
          'bower_components/bootstrap/dist/css/bootstrap.min.css',
          'bower_components/jquery-ui/themes/base/jquery-ui.min.css',
          'bower_components/flexboxgrid/dist/flexboxgrid.min.css',
          'bower_components/select2/dist/css/select2.min.css',
          'bower_components/bootstrap-daterangepicker/daterangepicker.css'
          ],
        dest: 'build/css/vendor.css'
      },
      js: {
        src: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/jquery-ui/jquery-ui.min.js',
          'bower_components/moment/min/moment.min.js',
          'bower_components/bootstrap-daterangepicker/daterangepicker.js',
          'bower_components/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js',
          'bower_components/bootstrap/dist/js/bootstrap.min.js',
          'bower_components/select2/dist/js/select2.min.js',
          'bower_components/jquery-mobile-touchevents/js/jquery.mobile.custom.min.js',
          'bower_components/flexibility/dist/flexibility.js'
          ],
        dest: 'build/js/vendor.js'
      }
    },

    // copy task

    copyto: {
      options: {
        ignore: [
          '.gitkeep',
          '**/.git',
          '**/.git/**',
          '**/*.{scss,sass}',
          'development/sass',
          'development/sass/**',
          'development/views/**'
        ]
      },
      buildjs: {
        files: [
          {cwd: 'development/', src: ['js/**/*'], dest: 'build/', expand: true},
          {src: ['development/vendor/require.js'], dest: 'build/js/require.js', expand: false}
        ]
      },
      buildfonts: {
        files: [
          {cwd: 'development/', src: ['assets/fonts/**/*'], dest: 'build/', expand: true}
        ]
      },
      buildicons: {
        files: [
          {cwd: 'development/', src: ['assets/icons/**/*'], dest: 'build/', expand: true}
        ]
      },
      buildimg: {
        files: [
          {cwd: 'development/', src: ['assets/img/**/*'], dest: 'build/', expand: true}
        ]
      },
      default: {
        files: [
          {cwd: 'development/', src: ['**/*', '!**/js/**'], dest: 'dist/', expand: true}
        ]
      },
    },

    // watch task

    watch: {
      styles: {
        files: ['development/**/*.{scss,sass}'],
        tasks: ['sass','postcss',],
        options: {
          spawn: false
        }
      },
      templates: {
        files: ['development/**/*.jade'],
        tasks: ['jade'],
        options: {
          spawn: false
        }
      },
      js: {
        files: ['development/js/**/*.js'],
        tasks: ['copyto:buildjs'],
        options: {
          spawn: false
        }
      },
      data: {
        files: ['development/data/*.json'],
        tasks: ['jade'],
        options: {
          spawn: false
        }
      }
    }

  });


  grunt.registerTask('serve', [
    'browserSync',
    'sass',
    'jade:compile',
    'copyto:buildjs',
    'copyto:buildfonts',
    'copyto:buildicons',
    'copyto:buildimg',
    'concat',
    'watch'
  ]);

  grunt.registerTask('docs', [
    'sassdoc',
    'jsdoc'
  ]);

  grunt.registerTask('default', [
    'clean',
    'sass',
    'jade',
    'requirejs',
    'copyto',
    'docs'
  ]);

};
