requirejs.config({
    baseUrl: 'js',
    paths: {
        modules: 'modules'
    }
});

// Start the main app logic.
requirejs(['js/modules/shame.js']);
requirejs(['js/modules/result.js']);

//requirejs(['js/modules/shared/buscador/datos-buscador.js']);

requirejs(['js/modules/resultados/header.js']);;
requirejs(['js/modules/resultados/filters.js'])
requirejs(['js/modules/resultados/headerBar.js']);;
requirejs(['js/modules/resultados/hotelCard.js']);

//Reserve Bar
requirejs(['js/modules/reserveBar/reserveBar.js']);
requirejs(['js/modules/reserveBar/calendar.js']);
requirejs(['js/modules/reserveBar/autocomplete.js']);


