(function($){
var hotelesData = [
    { label: 'Barcelona', value: 'Barcelona', category_type: 'ciudades' ,category_label: 'Ciudades' },
    { label: 'Madrid', value: 'Madrid', category_type: 'ciudades' ,category_label: 'Ciudades' },
    { label: 'Mallorca', value: 'Mallorca', category_type: 'ciudades' ,category_label: 'Ciudades' },
    { label: 'Malaga', value: 'Malaga', category_type: 'ciudades' ,category_label: 'Ciudades' },
    { label: 'Salamanca', value: 'Salamanca', category_type: 'ciudades' ,category_label: 'Ciudades' },

    { label: 'Barcelona Hotel 1', value: 'Barcelona Hotel 1', category_type: 'hoteles' ,category_label: 'Hoteles' },
    { label: 'Barcelona Hotel 2', value: 'Barcelona Hotel 2', category_type: 'hoteles' ,category_label: 'Hoteles' },
    { label: 'Barcelona Hotel 3', value: 'Barcelona Hotel 3', category_type: 'hoteles' ,category_label: 'Hoteles' },
    { label: 'Barcelona Hotel 4', value: 'Barcelona Hotel 4', category_type: 'hoteles' ,category_label: 'Hoteles' },
    { label: 'Barcelona Hotel 5', value: 'Barcelona Hotel 5', category_type: 'hoteles' ,category_label: 'Hoteles' }
];

    var destinationController = {
        init: function (el) {
            destinationController.setup(el);
            destinationController.placeHolderShow(el);
        },

        setup: function (el) {

            $(el).on('focus', destinationController.show);

            //$(el).on('blur', destinationController.hide);

            $(el).on('click', destinationController.onClick);

            $(el).on('change past keyup', destinationController.compare)

            destinationController.enterController(el);

            $('.last-searches [data-value]')
                .on('click', function () {
                    destinationController.setValue(this, el);
                    destinationController.changeFocus(el);
                    window.setTimeout(function(){ $("#check-in").trigger("click");}, 500);
                })
                .on('mouseenter', function () {
                    if( $(el).val().length == 0 ) {
                        destinationController.placeHolderHide(el);
                        destinationController.setText(this, '.autosuggest-div');
                    }
                })
        },

        setValue: function (el, target) {
            $(target).val( $(el).data('value') );
            $('.autocomplete__container').hide();
            $('.calendar__container').show();
            $('.ocupation__container').hide();
        },

        setText: function (el, target) {
            $(target).text( $(el).data('value') );
        },

        onClick: function () {
            $(this).select();
        },

        show: function () {
            $(this).siblings('.autosuggest-div').show();
            $($(this).data('show')).fadeIn(0);
        },

        hide: function () {
            var el = this;
            $(el).siblings('.autosuggest-div').hide().empty();
            $($(el).data('show')).delay(10).queue(function (next) {
                $(this).fadeOut(0);
                if( $(el).val().length == 0 ) {
                    destinationController.placeHolderShow(el);
                }
                next();
            });

        },

        close: function () {

        },

        changeFocus: function (el) {
            //$($(el).data('next-focus')).focus();
        },

        enterController: function (el) {
            $(window).keydown( function (event) {
                if( (event.keyCode == 13) && $(el).is(':focus') ) {
                    event.preventDefault();
                    if ( $('.autosuggest-div').text().length > 0 ) {
                        $(el).val($('.autosuggest-div').text());
                        destinationController.changeFocus(el);
                    } else {

                    }
                    return false;
                }
            });
        },

        placeHolderShow: function (el) {
            $(el).attr('placeholder', $(el).attr('placeholder-text'));
        },

        placeHolderHide: function (el) {
            $(el).attr('placeholder', '');
        },

        compare: function () {
            var suggest = $('.autosuggest-div').text().toUpperCase();
            var input = $('.destination').val().toUpperCase();
            suggest = suggest.substring(0, input.length);
            if (suggest != input) $('.autosuggest-div').empty();
        }

    }

    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> li:not(.ui-category)" );
        },

        _renderMenu: function( ul, items ) {
            var that = this,
            ulCategory,
            liCategory,
            currentCategory = "",
            wordRegex = this.term.split(' ').join('|'),
            // Contar bien los items
            count = 10;

            $.each( items, function( index, item ) {
                var li;

                if ( item.category_type != currentCategory ) {
                    ulCategory = $("<ul />", {'class': 'ul-category', 'data-text':item.category_label});
                    ul.append( $("<li />", { 'aria-label': ' ', "class": "ui-category " + item.category_type }).append(ulCategory) );

                    //Agrego titulo de grupo
                    ulCategory.before($('<div class="category-header"></div>').append(
                        '<div>' +
                            '<span class="city">' + item.category_label + '</span>' +
                        '</div>'
                        )
                    );

                    currentCategory = item.category_type;
                }

                li = that._renderItemData( ulCategory, item );
                li.html(li.text().replace(new RegExp("(" + wordRegex + ")", "gi"), '<b>$1</b>'));

                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        },

        close: function() {

        }
    });

    $( ".destination" ).catcomplete({
        delay: 0,
        source: function () {
            return hotelesData;
        }(),
        appendTo: '#predictor .list',
        minLength: 2,
        focus: function (event, ui) {
            event.preventDefault();

            if (!ui.item) return;

            var value = $(this).val().toUpperCase();
            var item = ui.item.label.toUpperCase();
            if ( item.substring(0, value.length) === value ) {
                $(this).siblings('.autosuggest-div').text(ui.item.label);
            } else {
                $(this).siblings('.autosuggest-div').empty();
            }
        },
        response: function(event, ui) {
            var noresults = '¡Vaya! No hemos encontrado ningún hotel o destino que cuadre con “braxe”';
            if (!ui.content.length) {
                var noResult = {
                    value: "noResults",
                    label: noresults
                };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
        select: function(event, ui) {
            this.value = (ui.item ? ui.item.name : '');
            if( ui.item.value != 'noResults') {
                $('.autocomplete__container').hide();
                $("#check-in").trigger("click");
            }
        },
        change: function (event, ui) {
            console.log(ui);
        }
    })

    .blur(function () {
        //$('#destination').catcomplete('close');
        //$('#predictor').fadeOut(0);
    })

    .on( "catcompleteopen", function( event, ui ) {
        $('.last-searches').hide();
        $('#predictor').show();
    })

    .on( "catcompleteclose", function( event, ui ) {
        $(this).siblings('.autosuggest-div').empty();
        $('.last-searches').show();
        $('#predictor').hide();

    });

    $('#testText').change(function(value) {
        console.log(value);
    });

    destinationController.init('.destination');

})(jQuery);
