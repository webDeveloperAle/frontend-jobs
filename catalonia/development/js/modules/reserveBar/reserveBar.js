(function() {
	$(document).ready(function() {
		// Hacky para saber desde el archivo javascript la media query
		var isMovile =  $('#hideElement').css('z-index') == 1 ? true : false;

		// Funcionalidad buscador
		(function() {
			//ON autocomplete
		})();

		// Funcionalidad calendario
		(function() {
			// Calendar login on calendar.js file
		    // Colapsa lista de hoteles
		    $('.autocomplete__container').on('click', '.category-header', function(event) {
		        if(isMovile) {
		            $(this).next().slideToggle('slow');
		        }
		    });

		})();
		// Funcionalidad reserva cantidad de personas
		(function() {
			// Rooms
			var dataTemp = [];
			var rooms = {
				data: [{
					rooms: 5,
					adults: 2,
					juniors: 0,
					children: 0,
					babies: 0,
					total: 2,
				}],

				max: {
					rooms: 3,
					occupation: 5
				},

				min: {
					adults: 1
				},

				init: function() {
					rooms.showLessFiveRoom();
					rooms.renderRooms(false);
					$('.ocupation__container').on('click', '#addRoom', rooms.addRoom);
					$('#rooms-container').on('click', '.delete-room', rooms.deleteRoom);
					$('#rooms-container').on('click', '.add', rooms.addPerson);
					$('#rooms-container').on('click', '.substract', rooms.substractPerson);
					$('.ocupation__container').on('click', '#backFourRooms', rooms.backFourRooms);
				},
				showMoreFiveRoom: function() {
					// Boton Agregar habitacion
					$('.room__add').hide();
					// Sumador de habitaciones
					$('.roomsCounter').show();
					// Boton volver a 4 habitaciones
					$('.room__backFourRooms').show();
					// Numeros de habitaciones (menos de 5)
					$('.roomName').hide();
					// Margen entre contadores mas chico porque se agrega el contador de habitaciones y no entra
					//$('.ocupationCounters > div').addClass('marginSelection5Rooms');
					// Agrego una clase sin contenido solo para tener referencia en la hoja de estilo
					$('.ocupationCounters.mobile').addClass('moreThanFiveRoom');
				},
				showLessFiveRoom: function() {
					// Boton Agregar habitacion
					$('.room__add').show();
					// Sumador de habitaciones
					$('.roomsCounter').hide();
					// Boton volver a 4 habitaciones
					$('.room__backFourRooms').hide();
					// Numeros de habitaciones (menos de 5)
					$('.roomName').show();
					// Margen entre contadores mas chico porque se agrega el contador de habitaciones y no entra
					//$('.ocupationCounters > div').removeClass('marginSelection5Rooms');
					// Agrego una clase sin contenido solo para tener referencia en la hoja de estilo
					$('.ocupationCounters.mobile').removeClass('moreThanFiveRoom');
				},
				renderRooms: function(warning) {
					warning ? $('.room__alert').show() : $('.room__alert').hide();

				    var template = $('#roomTemplate').html();
				    var roomContainer = $('#rooms-container');
					roomContainer.children('.room').remove();

					for (var i = 0; i< rooms.data.length; i++) {
						var t = template
						var number = i+1;
						roomContainer.append('<div class="room" data-room-number="' + number + '"></div>');

					    t = rooms.replaceAttr(t, number);
						t = rooms.putValues(t, i);
						var room = roomContainer.children('.room[data-room-number="' + number + '"]').append(t);
					}
				},
				replaceAttr: function(template, number) {
					var mapReplacements = {
						'{{roomNumber}}': number,
						'{{rooms}}': 'rooms' + number,
						'{{adults}}': 'adults' + number,
						'{{juniors}}': 'juniors' + number,
						'{{children}}': 'children' + number,
						'{{babies}}': 'babies' + number,
					};
					return template.replace(/{{roomNumber}}|{{rooms}}|{{adults}}|{{juniors}}|{{children}}|{{babies}}/gi, function(matched) {
						return mapReplacements[matched];
					});
				},
				putValues: function(template, index) {
					var mapReplacements = {
						'{{roomNumberValue}}': index + 1,
						'{{roomsValue}}': rooms.data[index].rooms,
						'{{adultsValue}}': rooms.data[index].adults,
						'{{juniorsValue}}': rooms.data[index].juniors,
						'{{childrenValue}}': rooms.data[index].children,
						'{{babiesValue}}': rooms.data[index].babies
					};
					template = template.replace(/{{roomNumberValue}}|{{roomsValue}}|{{adultsValue}}|{{juniorsValue}}|{{childrenValue}}|{{babiesValue}}/gi, function(matched) {
						return mapReplacements[matched];
					});
					template = template.replace(/data-value/gi, 'value');

					rooms.refreshTotalReserve(rooms, index);

					return template;
				},
				addRoom: function() {
					if(rooms.data.length <= rooms.max.rooms) {
						rooms.showLessFiveRoom();
						rooms.data.push({
							adults: 2,
							juniors: 0,
							children: 0,
							babies: 0,
							total: 2
						});

						rooms.data.length === 4 ? rooms.renderRooms(true) : rooms.renderRooms(false);

					} else {
						rooms.showMoreFiveRoom();
						dataTemp = JSON.parse(JSON.stringify(rooms.data));
						rooms.max.occupation = rooms.max.occupation * rooms.data.length;
						rooms.data = [];
						rooms.data.push({
							rooms: 5,
							adults: 2,
							juniors: 0,
							children: 0,
							babies: 0,
							total: 2
						});
						rooms.renderRooms(false);
					}
				},
				backFourRooms: function() {
					rooms.data = [];
					rooms.data = dataTemp;
					rooms.showLessFiveRoom();
					rooms.renderRooms(false);
				},
				deleteRoom: function() {
					var index = parseInt($(this).closest('.room').attr('data-room-number')) - 1;
					rooms.data.splice(index, 1);
					rooms.renderRooms(false);
				},
				addPerson: function() {
					var index = parseInt($(this).closest('.room').attr('data-room-number')) - 1;
					if (rooms.data[index].total === rooms.max.occupation) return;
					var personType = $(this).attr('data-person-type');
					var field = $('.' + personType + (index + 1));
					var val = rooms.data[index][personType];
					val++;
					rooms.data[index][personType] = val;
					rooms.data[index].total++;
					field.text(val);
					rooms.refreshTotalReserve(rooms, index);
				},
				substractPerson: function() {
					var index = parseInt($(this).closest('.room').attr('data-room-number')) - 1;
					var personType = $(this).attr('data-person-type');
					var field = $('.' + personType + (index + 1));
					var val = rooms.data[index][personType];
					if (val === 0 || (personType === 'adults' && val === rooms.min.adults)) return;
					val--;
					rooms.data[index][personType] = val;
					rooms.data[index].total--;
					field.text(val);
					rooms.refreshTotalReserve(rooms, index);
				},
				refreshTotalReserve: function(rooms, index) {
					$('#totalPerson').text(rooms.data[index].adults + ' ' + 'adultos,');
					$('#totalRooms').text(rooms.data.length + ' ' + 'hab.');
				}
			}

			rooms.init();
		})();

		// Funciones generales a todas las secciones
		function paintBorderSelectedBox(elemenBlue, color) {

			$('.searchBar__location').css('border-bottom-color', color);
			$('.searchBar__dates').css('border-bottom-color', color);
			$('.searchBar__persons').css('border-bottom-color', color);

			elemenBlue.css('border-bottom-color', color);
		}

		function openFullScreenMobile(isLocation) {
			$('#searchBar__containerBox').addClass('fullScreen');
			$('.movileHeadersContainer').show();
			if(isLocation) {
				$('.topBarReserveDate').hide();
				$('.locationMobile').show();

			} else {
				$('.topBarReserveDate').show();
				$('.locationMobile').hide();
			}
		}

		// Abri y Cierra los elementos de buscador segun corresponda
		$('.searchBar').on('click', '.searchBar__location', function(event) {
			$('#searchBar__containerBox').show();
			$('.autocomplete__container').show();
			$('.calendar__container').hide();
			$('.ocupation__container').hide();

			if(isMovile)  {
				openFullScreenMobile(true);
				setTimeout(function() {
					$('.temporalMessage').fadeOut('fast');
					$('#predictor .ui-autocomplete .ui-menu').hide();
				}, 1000);
			}
		});

		$('.searchBar').on('click', '.searchBar__dates', function(event) {
			$('#searchBar__containerBox').show();
			$('.calendar__container').show();
			$('.ocupation__container').hide();
			$('.autocomplete__container').hide();
			//Botones Edit
			$('.editReserveCalendar').show();
			$('.editReserveDestiny').hide();

			if(isMovile) {
				$('.temporalMessage').hide();
				openFullScreenMobile();
			}
		});

		$('.searchBar').on('click', '.searchBar__persons', function(event) {
			$('#searchBar__containerBox').show();
			$('.ocupation__container').show();
			$('.calendar__container').hide();
			$('.autocomplete__container').hide();

			$('.editReserveCalendar').hide();
			$('.editReserveDestiny').show();

			if(isMovile) {
				$('.temporalMessage').hide();
				openFullScreenMobile();
			}
		});

		// Close Mobile
		$('.searchBar').on('click', '.closeMovileReserve', function(event) {

			$('#searchBar__containerBox').removeClass('fullScreen');
			$('#searchBar__containerBox').hide();
			$('.autocomplete__container').hide();
			$('.calendar__container').hide();
			$('.ocupation__container').hide();
			$('.movileHeadersContainer').hide();
			$('.temporalMessage').show();
		});


		// Edit Calendar
		$('.searchBar').on('click', '.editReserveCalendar', function(event) {
			$('.autocomplete__container').show();
			$('.locationMobile').show();

			$('.topBarReserveDate').hide();
			$('.calendar__container').hide();
			$('.ocupation__container').hide();
		});

		// Edit Destinos
		$('.searchBar').on('click', '.editReserveDestiny', function(event) {
			$("#editReserveMobile").collapse('toggle');
		});

		$('.closeSearchDestiny').click(function() {
			$("#editReserveMobile").collapse('hide');
		});

	});

})();