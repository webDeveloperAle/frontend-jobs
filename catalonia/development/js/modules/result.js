
$(document).ready(function(){
    var objToFix = $('.tofix');
    if(objToFix.length) {
        var top,bottom, y, page;

        var left = objToFix.offset().left;
        tofixwidth();

        $(window).scroll(function (event) {

            // what the y position of the scroll is
            controlArticlePositions();

            // whether that's below the form
            if (y >= top && y<=bottom && y > page) {
                $('.tofix').addClass('fixed');

                //control horizontal scroll
                tofixl =  left-x;
                $('.tofix').css('left', tofixl);

            } else {
                $('.tofix').removeClass('fixed');
            }
        });

        $(window).resize(function() {
            tofixwidth();
        });

        function controlArticlePositions(){
            page = $('.result').offset().top;
            pageb = $('.tofix').height();

            top = $('.tofix').offset().top;

            bottom = page + $('.result').height() - pageb;

            y = $(window).scrollTop();
            x = $(window).scrollLeft();
        }

        function tofixwidth(){
            $('.tofix').css('width', '');
            $('.tofix').css('height', '100%');
            var obj= $('.tofix').closest('.result__map').css('width');
            $('.tofix').css('width', obj);
        }
    }

    //MAP
    $('#showMap').click(function() {
        $(".result__content").hide();
        //$("#wrap-filters").hide();
    });

    $('#backToApp').click(function() {
        $(".result__content").show();
    });

});

