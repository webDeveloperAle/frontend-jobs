(function() {

	$(document).ready(function(){
		//FUNCIONALIDAD DE BOTONES
	    $('#showFilter').click(function() {
			if ($('.screen').css('z-index') == 1) {
				$("#wrap-filters").collapse('toggle');
			} else {
				$("#wrap-filters").collapse('toggle');
				$("#header").hide();
				$(".result__content").hide();
				$(".result").addClass('removeMarginTop');
			}
	    });

		$('.toggleFilter').click(function() {
			/**
			 * Uso la propiedad z-index para saber la media query
			 * 1 = desktop
			 * 2 = mobile
			 */
			if ($('.screen').css('z-index') == 1) {
				$("#wrap-filters").collapse('toggle');
			} else {
				$("#wrap-filters").collapse('toggle');
				$("#header").hide();
				$(".result__content").hide();
				$(".result__map").hide();
				$(".result").addClass('removeMarginTop');
			}
		});

		$('.btn-cancel-filter').click(function() {
			$("#wrap-filters").collapse('toggle');
			$("#header").show();
			$(".result__content").show();
			$(".result__map").show();
			$(".result").removeClass('removeMarginTop');
		});

		$('#btn-apply-filter').click(function() {

		});

		// SLIDER PRECIO POR NOCHE
		$(function() {
			// SET SLIDER
			$( "#price-night-slider" ).slider({
				range: true,
				min: 0,
				max: 500,
				values: [ 0, 300 ],
				trigger: 'click',
				slide: function( event, ui ) {
					// Para conocer el id de cada tooltip
					var tooltipStartId = $('#startPrice').attr('aria-describedby'),
						tooltipEndId = $('#endPrice').attr('aria-describedby');

					$('#' + tooltipStartId + ' ' + '.tooltip-inner').text(ui.values[0] + ' €');
					$('#' + tooltipEndId + ' ' + '.tooltip-inner').text(ui.values[1] + ' €');
				}
			});

			// Se valor incial para mostar en tooltip
			var currentStartValue = $( "#price-night-slider" ).slider( "values", 0 ) + '€',
			    currentEndValue = $( "#price-night-slider" ).slider( "values", 1 ) + '€';

			$("#startNightValue").text(0 + '€');
			$("#endNightValue").text(500 + '€');

			// Agrego atributo id y los correspondientes al tooltip de boostrap
			$('#price-night-slider span:first').attr('id', 'startPrice');
			$('#price-night-slider span:first').attr('data-toggle', 'tooltip');
			$('#price-night-slider span:first').attr('data-placement', 'top');
			$('#price-night-slider span:first').attr('data-container', '#startPrice');
			$('#price-night-slider span:first').attr('data-original-title', currentStartValue);

			// Agrego atributo id y los correspondientes al tooltip de boostrap
			$('#price-night-slider span:last-child').attr('id', 'endPrice');
			$('#price-night-slider span:last-child').attr('data-toggle', 'tooltip');
			$('#price-night-slider span:last-child').attr('data-placement', 'top');
			$('#price-night-slider span:last-child').attr('data-container', '#endPrice');
			$('#price-night-slider span:last-child').attr('data-original-title', currentEndValue);

			// Inicializo tootltip
			$('[data-toggle="tooltip"]').tooltip({
				trigger: 'manual'
			});

			$('.nightRow').mouseenter(function() {
				$('#startPrice').tooltip('show');
				$('#endPrice').tooltip('show');
			});

			$('.nightRow').mouseleave(function() {
				$('#startPrice').tooltip('hide');
				$('#endPrice').tooltip('hide');
			});
		});

		// SLIDER DISTANCIA AL CENTRO
		$(function() {
			$( "#distance-slider" ).slider({
				range: true,
				min: 0,
				max: 10,
				values: [ 0, 5 ],
				slide: function( event, ui ) {
					// Para conocer el id de cada tooltip
					var tooltipStartId = $('#startDistant').attr('aria-describedby'),
						tooltipEndId = $('#endDistant').attr('aria-describedby');

					$('#' + tooltipStartId + ' ' + '.tooltip-inner').text(ui.values[0] + ' km');
					$('#' + tooltipEndId + ' ' + '.tooltip-inner').text(ui.values[1] + ' km');
				}
			});

			var currentStartValue = $( "#distance-slider").slider( "values", 0 ) + ' km',
			    currentEndValue = $( "#distance-slider").slider( "values", 1 ) + ' km';

			$("#startDistanceValue").text(0 + ' km');
			$("#endDistanceValue").text(10 + ' km');

			// Agrego atributo id y los correspondientes al tooltip de boostrap
			$('#distance-slider span:first').attr('id', 'startDistant');
			$('#distance-slider span:first').attr('data-toggle', 'tooltip');
			$('#distance-slider span:first').attr('data-placement', 'top');
			$('#distance-slider span:first').attr('data-container', '#startDistant');

			$('#distance-slider span:first').attr('data-original-title', currentStartValue);

			// Agrego atributo id y los correspondientes al tooltip de boostrap
			$('#distance-slider span:last-child').attr('id', 'endDistant');
			$('#distance-slider span:last-child').attr('data-toggle', 'tooltip');
			$('#distance-slider span:last-child').attr('data-placement', 'top');
			$('#distance-slider span:last-child').attr('data-container', '#endDistant');

			$('#distance-slider span:last-child').attr('data-original-title', currentEndValue);

			// Inicializo tootltip
			$('[data-toggle="tooltip"]').tooltip({
				trigger: 'manual'
			});

			$('.distanceRow').mouseenter(function() {
				$('#startDistant').tooltip('show');
				$('#endDistant').tooltip('show');
			});

			$('.distanceRow').mouseleave(function() {
				$('#startDistant').tooltip('hide');
				$('#endDistant').tooltip('hide');
			});
		});

		// SLIDER VALORACIÓN
		$(function() {
			$( "#appreciation-slider" ).slider({
				range: true,
				min: 0,
				max: 10,
				values: [ 0, 8 ],
				slide: function( event, ui ) {
					var tooltipStartId = $('#startAppreciation').attr('aria-describedby'),
						tooltipEndId = $('#endAppreciation').attr('aria-describedby');

					$('#' + tooltipStartId + ' ' + '.tooltip-inner').text(ui.values[0]);
					$('#' + tooltipEndId + ' ' + '.tooltip-inner').text(ui.values[1]);
				}
			});

			var currentStartValue = $( "#appreciation-slider").slider( "values", 0 ),
			    currentEndValue = $( "#appreciation-slider").slider( "values", 1 );

			$("#startAppreciationValue").text('0/6 Excelente');
			$("#endAppreciationValue").text('10 Excelente');

			// Agrego atributo id y los correspondientes al tooltip de boostrap
			$('#appreciation-slider span:first').attr('id', 'startAppreciation');
			$('#appreciation-slider span:first').attr('data-toggle', 'tooltip');
			$('#appreciation-slider span:first').attr('data-placement', 'top');
			$('#appreciation-slider span:first').attr('data-container', '#startAppreciation');
			$('#appreciation-slider span:first').attr('data-original-title', currentStartValue);

			// Agrego atributo id y los correspondientes al tooltip de boostrap
			$('#appreciation-slider span:last-child').attr('id', 'endAppreciation');
			$('#appreciation-slider span:last-child').attr('data-toggle', 'tooltip');
			$('#appreciation-slider span:last-child').attr('data-placement', 'top');
			$('#appreciation-slider span:last-child').attr('data-container', '#endAppreciation');
			$('#appreciation-slider span:last-child').attr('data-original-title', currentEndValue);

			// Inicializo tootltip
			$('[data-toggle="tooltip"]').tooltip({
				trigger: 'manual'
			});

			$('.appreciationRow').mouseenter(function() {
				$('#startAppreciation').tooltip('show');
				$('#endAppreciation').tooltip('show');
			});

			$('.appreciationRow').mouseleave(function() {
				$('#startAppreciation').tooltip('hide');
				$('#endAppreciation').tooltip('hide');
			});
		});


		var tootlTipArrow = $('<div class="toolTipArrow"></div>');

		$('div').on('shown.bs.tooltip', function () {
		  	tootlTipArrow.appendTo($('.tooltip-arrow'));
		});

		$('.group-star').click(function () {
			if($(this).hasClass('star-off')) {
				$(this).removeClass('star-off');
				$(this).addClass('star-on');
			} else {
				$(this).removeClass('star-on');
				$(this).addClass('star-off');
			}
		});
	});

})();