(function() {
	// FUNCIONALIDAD DE BOTONES BARRA BUSCADOR
	$(document).ready(function(){

		var searchMade = false;

		$(".searchTablet").hide();
		$('.resultBar__container').hide();

		setTimeout(function(){
			$(".openSearchMobile").hide();
			$('.resultBar__container').show();
		}, 2000);

		$('#findHotelTablet').click(function() {
			$('.searchTablet').hide();
			$('.resultBar__container').show();
		});

		$('#findHotelMobile').click(function() {
			//$("#searchMobile").collapse('hide');
			//$(".openSearchMobile").hide();
			//$('.resultBar__container').show();
		});

		$('#closeSearchTablet').click(function() {
			$('.resultBar__container').hide();
			$('.searchTablet').show();
		});

		$('#editSearch').click(function() {
			$('.resultBar__container').hide();
			$('.searchTablet').show();
		});

		$('#editSearchMobile').click(function() {
			$("#searchMobile").collapse('toggle');
		});

		$('#closeSearchTablet').click(function() {
			$('.searchTablet').hide();
			$('.resultBar__container').show();
		});

		$('#closeSearchMobile').click(function() {
			$("#searchMobile").collapse('hide');
		});

		/*$("#openMobileSearch").click(function() {
			$("#searchMobile").collapse('toggle');
		});*/

	});

})();