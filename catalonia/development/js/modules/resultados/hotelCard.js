(function() {

	$(document).ready(function() {

		$('.card__favorite span').click(function () {
			var element = '#' + $(this)[0].id;
			console.log(element);
			if($(element).hasClass('icon-heart')) {
				$(element).removeClass('icon-heart heart--emtpy');
				$(element).addClass('icon-heart_filled heart--red');
			} else {
				$(element).removeClass('icon-heart_filled heart--red');
				$(element).addClass('icon-heart heart--emtpy');
			}
		});
	});

})();