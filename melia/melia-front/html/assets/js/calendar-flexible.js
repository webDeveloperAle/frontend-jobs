(function($){

    var jsonConfig = {};
    var $calendars = $(".calendar-fechas");
    var numberOfMonths = 12;

    if ($calendars.length == 0) {
        return;
    }

    $calendars.each(function(){

        var $calendarNav = $("<div />", {'class':'calendar-nav'});
        var $calendarHeader = $("<table />", {'class':'calendar-header'});

        var $calendarTopFixed = $("<div />", {'class':'calendar-top-fixed'});
        $calendarTopFixed.append($calendarNav).append($calendarHeader);

        var $calendar = $("<div />", {'class': 'calendar-fechas-container'});

        $(this).append($calendarTopFixed).append($calendar);

        var oldGenerateHTML = $.datepicker._generateHTML;

        $.datepicker._generateHTML = function() {
            var ret = oldGenerateHTML.apply(this,arguments);

            if (ret.indexOf('calendar-flexible') == -1) {
                return ret;
            }

            ret = ret.replace(/href='#'>([0-9]{1,2})<\/a>/g, "href='#'>$1<span class='top'></span><span class='bottom'><span></span></span></a>");
            return ret;
        };

        $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
        $.datepicker._updateDatepicker = function(inst) {
            $.datepicker._updateDatepicker_original(inst);
            var afterShow = this._get(inst, 'afterShow');
            if (afterShow) {
                afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
            }
        }

        $calendar.datepicker({
            numberOfMonths: numberOfMonths,
            minDate: 0,
            maxDate: '+1y',
            firstDay: 1,
            dayNamesMin: [ 'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado' ],
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
                'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            onSelect: function(dateText, inst) {
                window.location.href = 'calendarLink.php?day='+(inst.selectedMonth+1)+'&year='+inst.selectedYear;
            },
            beforeShowDay: function ( date ) {
                var month = date.getMonth()+1;
                var day = date.getDate();
                var clases = ['calendar-flexible date-info date-'+month+'-'+day];
                var clase = getConfigValue(month, day, 'class');

                var selectable = true;

                if (clase) {
                    clases.push(clase);
                }

                if (getConfigValue(month, day, 'price')) {
                    clases.push('with-price');
                }

                if (getConfigValue(month, day, 'tooltip')) {
                    clases.push('with-tooltip');
                }

                if (clase.indexOf("no-disponibility") > -1 || getConfigValue(month, day, 'disabled-text') != '') {
                    selectable = false;
                }

                var bottomText = getConfigValue(month, day, 'disabled-text');

                if (bottomText) {
                    clases.push('with-bottom-text');
                }

                return [selectable, clases.join(' ')];
            },
            afterShow: function() {
                $(".calendar-fechas-flexibles").find(".ui-datepicker-other-month+.ui-datepicker-other-month+.ui-datepicker-other-month+.ui-datepicker-other-month+.ui-datepicker-other-month+.ui-datepicker-other-month+.ui-datepicker-other-month").closest("tr").remove();
            }
        });

        //$.datepicker._generateHTML = oldGenerateHTML;

        $calendarHeader.append($calendar.find(".ui-datepicker-calendar:eq(0)>thead").clone());

        // navigation

        var currentVisibleMonth = 0;

        $calendarNav.on('click', '.next-months a, .prev-months a', function(event) {
            event.preventDefault();

            var isPrevMonth = $(this).parent().hasClass('prev-months');

            if (isPrevMonth) {
                if (currentVisibleMonth > 1) {
                    buildNavigation(currentVisibleMonth-1);
                }
            } else {
                // nextMonth
                if (currentVisibleMonth < 12) {
                    buildNavigation(currentVisibleMonth+1);
                }
            }

            scrollToMonthNumber(currentVisibleMonth);

        }).on('click', 'a.go-to-month', function(event){
                event.preventDefault();
                buildNavigation($(this).data('month'));
                scrollToMonthNumber($(this).data('month'));
            });

        var doScroll = true;
        var fixedHeaderHeight = 210;

        function scrollToMonthNumber(monthNumber) {
            var $group = $calendar.find('.ui-datepicker-group:eq('+(monthNumber-1)+')');
            var $today = $group.find(".ui-datepicker-today");

            var $scrollTo = $today.length != 0 ? $today : $group;

            doScroll = false;
            $('html,body').animate({scrollTop: $scrollTo.offset().top-fixedHeaderHeight}, 'slow',
                function(){
                    window.setTimeout(function(){doScroll = true;}, 100);
                }
            );
        }

        var $xsTest = $("<div />", {'class': 'visible-xs'}).appendTo($("body"));
        var $tabletTest = $("<div />", {'class': 'visible-sm'}).appendTo($("body"));

        $(window).on("scroll", function(){

            if (!doScroll) {
                return;
            }

            var $groups = $calendar.find('.ui-datepicker-group');
            var groupTops = $groups.map(function(){return $(this).offset().top;}).get();
            var scrolTop = scrollY();

            for (var i = 11; i >= 0; i--) {
                if (scrolTop > (groupTops[i]-fixedHeaderHeight)) {
                    buildNavigation(i+1);
                    break;
                }
            }
        });

        function buildNavigation(activeMonth) {
            var $nav = $("<ul />");

            activeMonth = activeMonth || 1;

            if (activeMonth < 1) {
                activeMonth = 1;
            }

            var navigationRange = getNavigationRange(activeMonth);

            var rangeItems = navigationRange[1]-navigationRange[0];

            var addEmptyItems = function($ul, numberOfItems){
                for(var i = 0; i < numberOfItems; i++) {
                    $ul.append($("<li />", {'class': 'empty-item'}).html("&nbsp;"));
                }
            };

            var emptyItems = $tabletTest.is(":visible") ? 4 : 6;

            if (rangeItems < emptyItems && navigationRange[0] == 1) {
                addEmptyItems($nav, emptyItems-rangeItems);
            }

            $calendar.find(".ui-datepicker-month").each(function(key, item){
                var monthNumber = key+1;
                var year = $(this).parent().find(".ui-datepicker-year").text();
                if (monthNumber >= navigationRange[0] && monthNumber <= navigationRange[1]) {
                    var $li = $("<li />");
                    $li.append($("<a />", {'href': '#', 'class': 'go-to-month', 'data-month': monthNumber, 'text': $(item).text()}).prepend($("<span/>", {text: year})));

                    if (monthNumber == activeMonth) {
                        $li.addClass("active");
                    }

                    $nav.append($li);
                }
            });

            if (rangeItems < 4 && navigationRange[1] == 12) {
                addEmptyItems($nav, 4-rangeItems);
            }

            if (activeMonth > 1) {
                $nav.prepend($("<li />", {'class': 'prev-months'}).append($("<a />", {'href': '#'})));
            }

            if (activeMonth < 12) {
                $nav.append($("<li />", {'class': 'next-months'}).append($("<a />", {'href': '#'})));
            }

            $calendarNav.empty().append($nav);

            currentVisibleMonth = activeMonth;
        }

        function getNavigationRange(currentMonth) {

            if ($tabletTest.is(":visible")) {
                if (currentMonth == 1) {
                    return [1, 3]
                }

                if (currentMonth == 2) {
                    return [1, 4];
                }

                if (currentMonth == 11) {
                    return [9, 12];
                }

                if (currentMonth == 12) {
                    return [10, 12];
                }

                return [currentMonth-2, currentMonth+2];
            }

            if (currentMonth == 1) {
                return [1, 4]
            }

            if (currentMonth == 2) {
                return [1, 5];
            }

            if (currentMonth == 3) {
                return [1, 6];
            }

            if (currentMonth == 10) {
                return [7, 12];
            }

            if (currentMonth == 11) {
                return [8, 12];
            }

            if (currentMonth == 12) {
                return [9, 12];
            }

            return [currentMonth-3, currentMonth+3];
        }

        buildNavigation();

        $(window).resize(function(){
            buildNavigation(currentVisibleMonth);
        });
    });

    function scrollY() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    $('body').tooltip({
        selector: '.with-tooltip span',
        html: true,
        placement: 'auto bottom',
        trigger: 'click hover',
        title: function(){
            var dateInfo = getDateFromClass($(this).closest("td").attr("class"));
            return getConfigValue(dateInfo.month, dateInfo.day, 'tooltip');
        }
    });

    $(".fechas-flexibles-intro .total-nights").popover({
        content: function(){
            return $('#total-nights-content').html();
        },
        html: true,
        placement: 'auto bottom'
    });

    $.getJSON('includes/modules/ajax/json_calendar.php', function(data){
        loadCellsInfo(data);
    });

    function getDateFromClass(className) {
        var dateInfo = className.match(/date-(\d+)-(\d+)/i);
        return {month: dateInfo[1], day: dateInfo[2]};
    }

    function getConfigValue(month, day, key) {
        if (jsonConfig.hasOwnProperty(month)
            && jsonConfig[month].hasOwnProperty(day)
            && jsonConfig[month][day].hasOwnProperty(key)) {
            return jsonConfig[month][day][key];
        }

        return '';
    }

    function loadCellsInfo(calendarConfig) {

        jsonConfig = calendarConfig;

        var addCSSRule = function(rule) {
            $('head').append('<style>' + rule + '</style>');
        };

        $.each(calendarConfig, function(month, days){
            $.each(days, function(day, config){
                var className = 'date-'+month+'-'+day;

                if (config.hasOwnProperty('price')) {
                    addCSSRule('.'+className+' a>span>span:before {content: "'+config.price+'";}');
                }

                if (config.hasOwnProperty('bottom')) {
                    addCSSRule('.'+className+' a:after {content: "'+config.bottom+'";}');
                }

                if (config.hasOwnProperty('top')) {
                    addCSSRule('.'+className+' a:before {content: "'+config.top+'";}');
                }

                var bottomText = getConfigValue(month, day, 'disabled-text');

                if (bottomText) {
                    addCSSRule('.'+className+':before {content: "'+bottomText+'";}');
                }
            });
        });

        $calendars.find('.calendar-fechas-container').datepicker('refresh');
    }

    (function(){
        var $fixedElement = $('.calendar-top-fixed');

        if ($fixedElement.length == 0) {
            return;
        }

        var posFromTop = $fixedElement.offset().top-95; // 95 = header+main menu fixed

        $(window).on("scroll", function() {

            if (scrollY() > posFromTop) {
                $fixedElement.addClass("fixed");
            } else {
                $fixedElement.removeClass("fixed");
            }

        });
    })();

    (function(){
        $(document).on('scroll', function() {
            if (document.body.scrollTop !== 0) {
            $(".calendar-header th").children().css("padding", "0");
            } else{
            $(".calendar-header th").children().css("padding", "12");
            };
        });

    })();


})(jQuery);