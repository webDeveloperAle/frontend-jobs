// Dealing with events
$('[data-event]').on('click', function (ev) {
  var $this     = $(this),
      eventName = $this.data('event'),
      eventData = $this.data('data');
console.log(eventName);
  if (eventName === 'deleteAlternativeCalendar') {
    ev.stopPropagation();
  }

  $.publish(eventName, eventData);
});

$.subscribe('deleteElement', function (ev, selectors) {
  var selectors = selectors.split(',');

  for (var i = 0, j = selectors.length; i < j; i++) {
    $(selectors[i]).remove();
  }
});

$('[data-event-check]').on('click', function () {
  var $this = $(this);

  if( $this.is(':checked') ) {
    $.publish($this.data('event-check-true'), $this.data('data'));
  } else {
    $.publish($this.data('event-check-false'), $this.data('data'));
  }
});

var calendarWidget = (function () {

  var calendars = [];

  function initDaySelection(calendar) {
    var $modal = $('.select-offers-modal'),
        $modalContent = $modal.find('.modal-content'),
        $tmpl  = $modal.find('.model-modal-content');

    $modalContent.html('')

    for (var i = 0, j = calendar.daysToProcess.length; i<j; i++) {
      var day = calendar.daysToProcess[i],
          $newModal = $($tmpl.html()),
          $newModalNextBtn = $newModal.find('.day-selection-next');

      $modalContent.append($newModal);

      $newModal
        .find('input[type="radio"]')
        .attr('name','select-promo-'+i)
        .each(function (index, el) {
          $(this)
            .attr('id', 'select-promo-'+i+'-'+index)
            .parent().find('label')
            .attr('for', 'select-promo-'+i+'-'+index);
        });

      if (i === 0) {
        $newModal.removeClass('hide');
      }

      if (i < j-1) {
        $newModalNextBtn
          .text('DÍA SIGUIENTE ('+(i+1)+' DE '+j+')')
          .on('click', function () {
            $(this)
              .parents('.select-offers-content')
              .addClass('hide')
              .next('.select-offers-content')
                .removeClass('hide');
          });
      } else if (i === j-1) {
        $newModalNextBtn
          .text('CONTINUAR')
          .on('click', function () {
            $(this).parents('.modal').modal('toggle');
            $.publish('calendarChanged', calendar);
          });
      }

      $newModal.find('.day-selection-title').html(translateDate(day.date));

      $newModal.find('.select-opportunity').on('change', function () {
        var $this = $(this);
        if ($(this).is(':checked')) {
          day.opportunity = true;
          if (!$this.hasClass('promotion')) day.promotion = false;
        }
      });
      $newModal.find('.select-promotion').on('change', function () {
        var $this = $(this);
        if ($(this).is(':checked')) {
          day.promotion = true;
          if (!$this.hasClass('select-opportunity')) day.opportunity = false;
        }
      });
    }

    $modal.modal('show');
  }

  // Makes the calendar interactive.
  // Quite messy because of the Datepicker's nature.
  function setCalendarBehaviour(calendar) {
    calendar.$wrapper.datepicker({
      numberOfMonths: 1,
      minDate: 0,
      firstDay: 1,
      dayNamesMin: [ "DOMINGO", "LUNES", "MARTES", "MIÉRCOLES", "JUEVES", "VIERNES", "SÁBADO" ],
      monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio",
      "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
      onSelect: function (dateText, inst) {
        var day1 = -1, day2 = -1;

        if (calendar.prv != calendar.cur) {
          calendar.selectedDays = [];
          calendar.prv = -1;
          calendar.cur = -1;
          $.datepicker._clearDate(calendar.$wrapper);
        }

        if (!dateText) {
          calendar.prv = -1;
          calendar.cur = -1;
          calendar.$checkOut.val('');
          calendar.$checkIn.val('');
          $.publish('calendarChanged', calendar);
          return;
        }

        calendar.prv = calendar.cur;
        calendar.cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();

        if (calendar.mode === 'oneDay') {
          calendar.prv = calendar.cur;
          calendar.$checkOut.val('');
          day1 = new Date(calendar.prv);
          calendar.$checkIn.val($.datepicker.formatDate('dd/mm/yy', day1, {}));
        }

        var prvBck = Math.min(calendar.prv, calendar.cur);
        var curBck = Math.max(calendar.prv, calendar.cur);

        calendar.prv = prvBck;
        calendar.cur = curBck;

        if ( calendar.prv === -1 || calendar.prv === calendar.cur) {
          calendar.prv = calendar.cur;
          day1 = new Date(calendar.prv)
          calendar.$checkIn.val($.datepicker.formatDate('dd/mm/yy', day1, {}));
        } else {
          day1 = new Date(calendar.prv);
          day2 = new Date(calendar.cur);
          calendar.$checkIn.val($.datepicker.formatDate('dd/mm/yy', day1), {});
          calendar.$checkOut.val($.datepicker.formatDate('dd/mm/yy', day2), {});
        }

        // Show select promo/opportunity modal
        calendar.selectedDays = [];
        calendar.daysToProcess = [];
        for (var prv=calendar.prv, cur=calendar.cur; prv<=cur; prv+=86400000) {
          var date = new Date(prv),
              dateDDMMYYYY = $.datepicker.formatDate('dd/mm/yy', date, {}),
              selectedDay = $.extend({ date: date }, calendar.specialDays[dateDDMMYYYY]);

          calendar.selectedDays.push(selectedDay);

          if (calendar.specialDays[dateDDMMYYYY] && calendar.specialDays[dateDDMMYYYY].promotion && calendar.specialDays[dateDDMMYYYY].opportunity) {
            calendar.daysToProcess.push(selectedDay);
            selectedDay.promotion = false;
          }
        }

        //
        if (calendar.mode === 'oneDay') {
          if (calendar.daysToProcess.length > 0) initDaySelection(calendar);
          else $.publish('calendarChanged', calendar);
        } else {
          if (calendar.cur === calendar.prv) $.publish('calendarChanged', calendar);
          else if (calendar.daysToProcess.length > 0) initDaySelection(calendar);
          else $.publish('calendarChanged', calendar);
        }
      },
      beforeShowDay: function ( date ) {
        var className = [],
            dateTime = date.getTime(),
            formattedDate = $.datepicker.formatDate('dd/mm/yy', date),
            min = Math.min(calendar.prv, calendar.cur),
            max = Math.max(calendar.prv, calendar.cur);

        if (calendar.specialDays[formattedDate]) {
          if(calendar.specialDays[formattedDate].promotion === true) {
            className.push('promotion icon-bullet');
          }

          if(calendar.specialDays[formattedDate].opportunity === true) {
            className.push('opportunity icon-bullet');
          }
        }

        if (dateTime >= min && dateTime <= max) {
          className.push('date-range-selected');
        }

        if (dateTime === min) {
          className.push('range-first');
        } else if (dateTime === max) {
          className.push('range-last');
        }

        return [true, className.join(' ')];
      }
    });
  }

  // Returns a calendar given an key or a DOM object
  function getCalendar(id) {
    for (var i = 0, j = calendars.length; i < j; i++) {
      if (id === calendars[i].key) {
        return calendars[i]
      }
    }

    return null;
  }

  // Makes the calendar able to choose a range of dates
  function setInOutMode(id) {
    var calendar = getCalendar(id);

    calendar.mode = 'inOut';

    $.datepicker._clearDate(calendar.$wrapper);

    $.publish('calendarChanged', calendar);
  }

  // Makes the calendar being able to only chose one day
  function setOneDayMode(id) {
    var calendar = getCalendar(id);

    calendar.mode = 'oneDay';

    $.datepicker._clearDate(calendar.$wrapper);

    $.publish('calendarChanged', calendar);
  }

  // Switch to another calendar
  function switchCalendars(fromKey, toKey) {
    var from, to;

    if (fromKey === toKey) return;

    from = getCalendar(fromKey),
    to = getCalendar(toKey);

    from.$root.addClass('hide');
    to.$root.removeClass('hide');
  }

  function showCalendar(id) {
    getCalendar(id).$root.removeClass('hide');
  }

  function hideCalendar(id) {
    getCalendar(id).$root.addClass('hide');
  }

  // Removes a calendar from the DOM and from the calendars array
  function destroyCalendar(id) {
    var calendar = getCalendar(id);

    if (calendar) {
      calendar.object.remove();
      calendars.splice(calendar.key, 1);
    }
  }

  function validateCalendar(id) {
    var calendar = getCalendar(id);

    if (calendar.mode === 'oneDay') {
      if (!calendar.prv || calendar.prv === -1) return false;
    } else {
      if (calendar.prv === -1 || calendar.cur === -1) return false;
      if (!calendar.prv || !calendar.cur) return false;
      if (calendar.prv === calendar.cur) return false;
    }

    return true;
  }

  // Init
  function createCalendar(options) {
    var calendar = $.extend({}, {
          daysToProcess:  [],
          selectedDays:   [],
          mode:           'inOut',
          prv:            -1,
          cur:            -1
        }, options);

    calendars.push(calendar);
    setCalendarBehaviour(calendar);

    return calendar;
  }

  function translateDate(date) {
    if (date) {
      var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
      var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
      return days[date.getDay()]+', '+date.getDate()+' de '+months[date.getMonth()]+' de '+date.getFullYear();
    }
  }



  return {
    oneDayMode: setOneDayMode,
    inOutMode:  setInOutMode,
    switchTo:   switchCalendars,
    create:     createCalendar,
    remove:     destroyCalendar,
    get:        getCalendar,
    hide:       hideCalendar,
    translate:  translateDate,
    validate:   validateCalendar,
    getAllCalendars: function () {
      return calendars;
    }
  };

}());



(function ($){

  $('.mice-calendar').each(function () {
    var $this     = $(this),
        calendar  = calendarWidget.create({
          key:          $this.attr('id'),
          specialDays:  meliaServices.getSpecialDays(),
          $root:        $this,
          $wrapper:     $this.find('.mice-calendar-wrapper'),
          $checkIn:     $this.find('.mice-calendar-check-in'),
          $checkOut:    $this.find('.mice-calendar-check-out')
        });

    $this.find('.mice-calendar-one-day').on('change', function () {
      if (this.checked) { calendarWidget.oneDayMode(calendar.key); }
    });

    $this.find('.mice-calendar-in-out').on('change', function () {
      if (this.checked) { calendarWidget.inOutMode(calendar.key); }
    });

    $this.delegate('.ui-datepicker-calendar td', 'mouseenter', function () {
      var $that = $(this),
          $days = $that.parents('table').find('td');

      $days.removeClass('hover-active');

      if (calendar.prv > -1 && calendar.cur > -1 && calendar.prv != calendar.cur) {
        return
      }
      
      var date = new Date($that.data('year'), $that.data('month'), $that.text()),
          dateTime = date.getTime(),
          i = $days.index($that),
          $next;

      if (calendar.mode != 'oneDay' && calendar.prv != -1) {
        do {
          if (calendar.prv < dateTime) {
            $next = $days.eq(--i);
          } else if (calendar.prv > dateTime) {
            $next = $days.eq(++i);
          }

          if (!$next
              || $next.hasClass('range-first')
              || $next.hasClass('range-last')
              || i < 0
              || i > $days.length) {
            $next = null;
          } else {
            $next.addClass('hover-active');
          }
        } while ($next)
      }
    });

  });

  
  // Control scrolling position for aside options
  $.subscribe('calendarUpdated', function () {
    var $window = $(window),
        offset = 0;

    $window.unbind('scroll');

    $('[data-stick-to]').each(function (i) {
      if (i>0) {
        var $self = $(this),
            selfTop = $self.offset().top-60-$self.css('margin-top').replace('px',''),
            $calendar = calendarWidget.get($self.data('stick-to')).$root.parent(),
            calendarTop = $calendar.offset().top+$calendar.outerHeight()-$self.innerHeight()-60,
            previousOffset = offset;

        $window.scroll(function () {
          var scrollTop = $window.scrollTop();

          if (scrollTop > selfTop+previousOffset && scrollTop < calendarTop) {
            $self.css('margin-top', (scrollTop-selfTop-previousOffset)+'px');
          } else if (scrollTop > calendarTop) {
            $self.css('margin-top', calendarTop-selfTop-previousOffset+'px');
          } else if (scrollTop < selfTop || scrollTop < selfTop+previousOffset) {
            $self.css('margin-top', '0px');
          }
        });

        offset = $calendar.offset().top+$calendar.outerHeight()-$self.innerHeight() - $self.offset().top;
        offset = offset < 0 ? 0 : offset;
      }
    });
  });

  $.publish('calendarUpdated');



  // Validate that all calendars dates are correctly setted
  function validateAllCalendars() {
    var allValid = true;
    
    $('.mice-calendar-mandatory').each(function () {
      allValid = allValid && calendarWidget.validate($(this).attr('id'));
    });
    $('.mice-calendar-alt').not('.hide').each(function () {
      allValid = allValid && calendarWidget.validate($(this).attr('id'));
    });

    if (allValid) {
      $('.checkout-form button').removeAttr('disabled');
    } else {
      $('.checkout-form button').attr('disabled', true);
    }
  }



  // Dealing with calendar events
  $.subscribe('calendarChanged', function (ev, calendar) {
    var resume    = $('#'+calendar.key+'_resume'),
        checkIn   = calendar.prv > -1 ? new Date(calendar.prv) : '',
        checkOut  = calendar.cur > -1 ? new Date(calendar.cur) : '',
        opportunities = promotions = 0;

    if (checkIn) {
      resume.find('.calendar-resume-intro').addClass('hide');
      resume.find('.calendar-resume-check-in').removeClass('hide');
    } else {
      resume.find('.calendar-resume-intro').removeClass('hide');
      resume.find('.calendar-resume-check-in').addClass('hide');
    }

    if (calendar.selectedDays.length > 1) {
      resume.find('.calendar-resume-check-out').removeClass('hide');
      resume.find('.calendar-resume-total').removeClass('hide');
      $('#'+calendar.key+'_alt_resume').removeClass('hide');
    } else if (calendar.selectedDays.length <= 1) {
      resume.find('.calendar-resume-check-out').addClass('hide');
      resume.find('.calendar-resume-total').addClass('hide');
      if (calendar.mode === 'oneDay' && calendar.selectedDays.length === 1) $('#'+calendar.key+'_alt_resume').removeClass('hide');
    }

    resume.find('.calendar-resume-check-in-days').html(calendarWidget.translate(checkIn) || '');
    resume.find('.calendar-resume-check-out-days').html(calendarWidget.translate(checkOut) || '');
    resume.find('.calendar-resume-total-days').html(Math.round(Math.abs(checkIn-checkOut) / 8.64e7)+1);

    for (var i=0, j=calendar.selectedDays.length; i<j; i++) {
      if (calendar.selectedDays[i].promotion) promotions++;
      if (calendar.selectedDays[i].opportunity) opportunities++;
    }

    if (promotions) {
      resume.find('.calendar-resume-promotion').removeClass('hide');
      resume.find('.calendar-resume-promotion-days').html(promotions);
    } else {
      resume.find('.calendar-resume-promotion').addClass('hide');
      resume.find('.calendar-resume-promotion-days').html('');
    }

    if (opportunities) {
      resume.find('.calendar-resume-opportunity').removeClass('hide');
      resume.find('.calendar-resume-opportunity-days').html(opportunities);
    } else {
      resume.find('.calendar-resume-opportunity').addClass('hide');
      resume.find('.calendar-resume-opportunity-days').html('');
    }

    validateAllCalendars();

    $.publish('calendarUpdated');
  });


  $.subscribe('deleteHotel', function (ev, id) {
    var calendar = calendarWidget.get('calendar_'+id),
        name = calendar.$root.parent().find('.hotel-name').text(),
        stars = calendar.$root.parent().find('.stars').attr('class'),
        $modal = $('#delete-hotel-confirmation');
    
    $modal.find('.delete-hotel-name').html(name);
    $modal.find('.delete-hotel-stars').addClass(stars);
    $modal.modal('show');

    $modal.find('.delete-hotel-no').unbind('click').on('click', function () {
      $modal.modal('hide');
    });

    $modal.find('.delete-hotel-yes').unbind('click').on('click', function () {
      $('#'+id+',#resume_'+id).remove();

      var resumesLeft = $('.resume_hotel');
      if (resumesLeft.length <= 1) {
        resumesLeft.find('.icon-close').addClass('hide');
      }

      $modal.modal('hide');

      validateAllCalendars();
    });

    $.publish('calendarUpdated');
  });


  $.subscribe('addAlternativeCalendar', function (ev, id) {
    var $resume = $('#'+id+'_alt_resume');

    $resume.find('.alternative-calendar').removeClass('hide');
    $resume.find('.add-alternative-calendar').addClass('hide');
    
    $.publish('selectCalendar', id+'_alt');

    validateAllCalendars();

    $.publish('calendarUpdated');
  });


  var dirtyDelete = false, noScroll = false;
  $.subscribe('deleteAlternativeCalendar', function (ev, id) {
    var $resume = $('#'+id+'_alt_resume');

    $.datepicker._clearDate(calendarWidget.get(id+'_alt').$wrapper);
    calendarWidget.switchTo(id+'_alt', id);
    $resume.find('.alternative-calendar').addClass('hide');
    $resume.find('.add-alternative-calendar').removeClass('hide');

    $.publish('calendarChanged', calendarWidget.get(id+'_alt'));

    if (!dirtyDelete) {
      noScroll = true;
      $.publish('selectCalendar', id);
    }
    else dirtyDelete = false;

    validateAllCalendars();

    $.publish('calendarUpdated');
  });

  var firstLoad = true;
  $.subscribe('selectCalendar', function (ev, id) {
    var from = to = id;

    if (id.indexOf('_alt') > -1) {
      from = id.split('_alt')[0];
    } else {
      from = id+'_alt';
    }

    calendarWidget.switchTo(from, to);

    lastSelected = id;

    if (!firstLoad && !noScroll) {
      $('html, body').animate({
          scrollTop: calendarWidget.get(to).$root.parent().offset().top - 60
      }, 600);
    }
    firstLoad = false;
    noScroll = false;

    $('#'+from+'_resume').removeClass('grey').find('.alternative-calendar').removeClass('grey');
    $('#'+to+'_resume').addClass('grey').find('.alternative-calendar').addClass('grey');

    if (id.indexOf('_alt') === -1 && !calendarWidget.validate(id+'_alt')) {
      dirtyDelete = true;
      $.publish('deleteAlternativeCalendar', id);
    }
  });
  firstLoad = false;

  $.subscribe('addReunionPack', function (ev, id) {
    $('#resume_'+id).find('.pack-option').removeClass('hide');
    $.publish('calendarUpdated');
  });


  $.subscribe('removeReunionPack', function (ev, id) {
    $('#resume_'+id).find('.pack-option').addClass('hide');
    $('#'+id).find('.reunion-pack input').prop('checked', false);
    $.publish('calendarUpdated');
  });



  function updateTodayText() {
    $('.calendar-wrapper').each(function () {
      var $this = $(this),
          todayText = $this.data('today-text');
      $this.find('.ui-datepicker-today .ui-state-default').attr('data-today-text', todayText);
    });
  }

  $(document).on('click', function () {
    setTimeout(updateTodayText, 100);
  });
  updateTodayText();

})(jQuery);