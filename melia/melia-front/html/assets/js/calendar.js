/* global fn */
var hideCalendar;

(function($){
    var $inputWrapper, $reservationCalendar;

    var $calendarModule = $('#search-calendar'),
        $calendarTarget = $calendarModule.find(".calendar-wrapper"),
        $calendarCont = $('.calendar-container');

    hideCalendar = function(speed) {
        $calendarModule.fadeOut(speed);
        $calendarCont.fadeOut(speed);
        $calendarTarget.datepicker("hide");
        $('.form-group.reservation-calendar').removeClass('focus-checkin focus-checkout');
    };

    function getCurrentRange(type) {
        if (!$reservationCalendar || $reservationCalendar.length == 0) {
            return 0;
        }
        return $reservationCalendar.data("current-range-"+type) || 0;
    }

    $calendarTarget.datepicker({
        numberOfMonths: 2,
        minDate: 0,
        firstDay: 1,
        dayNamesMin: [ "D", "L", "M", "M", "J", "V", "S" ],
        monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio",
            "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
        beforeShowDay: function ( date ) {
            var clases = [];

            if(date.getTime() == getCurrentRange('check-in')) {
                clases.push("date-range-selected range-first");
            } else if(date.getTime() == getCurrentRange('check-out')) {
                clases.push("date-range-selected range-last");
            } else if (date.getTime() >= getCurrentRange('check-in') && date.getTime() <= getCurrentRange('check-out')) {
                clases.push("date-range-selected");
            }

            return [true, clases.join(" ")];
        },
        onSelect: function ( dateText, inst ) {

            var selectedDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

            $reservationCalendar.data("current-range-"+$inputWrapper.data("input-type"), selectedDate.getTime());

            if (getCurrentRange("check-in") > getCurrentRange("check-out")) {
                $reservationCalendar.data("current-range-check-out", 0);
            }

            setCalendarHeaderText($inputWrapper.data("input-type"), selectedDate);

            $inputWrapper.find("input").val($.datepicker.formatDate( 'dd-mm-yy', selectedDate, {} ));

            // ahora tenemos que mostrar el datepicker de fecha fin o abrir el selector de habitaciones
            if ($inputWrapper.data("input-type") == "check-in") {
                getTheOtherInput($inputWrapper).trigger("click");
            } else if ($reservationCalendar.data("calendar-ocupation") !== false) {
                $("#ocupationshow").trigger("click");
            }

            // Cerrar calendario en rewards reservas
            if($(".rw-reserva-modificar-reserva #"+$inputWrapper.data("inpyt-type") == "check-out")){ // WHAT??
               if(getCurrentRange("check-out") != 0){
                    hideCalendar(200);
                }
            }

        }
    });

    function checkOffscreen($el) {

        if ($el.length == 0) {
            return;
        }

        $el.removeClass('right-edge').removeClass('left-edge');

        var windowWidth = $(window).width();
        var left = $el.offset().left;
        var width = $el.outerWidth();

        if(windowWidth < (left + width)){
            $el.addClass('right-edge');
        }

        if(left < 0){
            $el.addClass('left-edge');
        }
    }

    /* abrir y configurar el calendar actual */
    $(".reservation-calendar .check-in, .reservation-calendar .check-out").click(function () {
        var checkInOrCheckOut = $(this).hasClass('check-in') ? 'check-in' : 'check-out';
        $(this).data("input-type", checkInOrCheckOut);
        $inputWrapper = $(this);
        $reservationCalendar = $inputWrapper.closest('.reservation-calendar');

        $(".calendar-container").removeClass("active");

        var $calendarContainer = $reservationCalendar.find(".calendar-container");

        $calendarContainer.addClass("active");

        if (!$.contains($reservationCalendar[0], $calendarModule)) {
            $calendarContainer.append($calendarModule);
        }

        $calendarModule.show();
        checkOffscreen($calendarContainer);
        $calendarTarget.datepicker('refresh');

        if (!$(this).attr("data-text-initial")) {
            $(this).attr("data-text-initial", $(this).attr("data-text"));
        }

        /* dependiendo que datepicker estoy abriendo tengo que setear la fecha minima o maxima */

        var openCheckInCalendar = function() {
            // ninguna fecha minima
            $calendarTarget.datepicker("option", 'minDate', 0);
            $calendarCont.fadeOut(200, function () {
                $calendarTarget.addClass("is-checkin-date").removeClass("is-checkout-date");
                $calendarTarget.parent().addClass("is-checkin-date").removeClass("is-checkout-date");
                $(this).fadeIn(200);
            });
            $('.form-group.reservation-calendar').addClass('focus-checkin').removeClass('focus-checkout');
        };

        var openCheckOutCalendar = function(){
            if(!$calendarCont.is(':visible')) {
                $calendarCont.show()
            }
            $calendarTarget.addClass("is-checkout-date").removeClass("is-checkin-date");
            $calendarTarget.parent().addClass("is-checkout-date").removeClass("is-checkin-date");
            $('.form-group.reservation-calendar').addClass('focus-checkout').removeClass('focus-checkin');
        };

        if ($inputWrapper.data("input-type") == "check-in") {
            openCheckInCalendar();
        } else {
            // si no hay fecha minima, abrimos el calendar de check in
            if (!getCurrentRange('check-in')) {
                $inputWrapper = getTheOtherInput($inputWrapper);
                openCheckInCalendar();
                return;
            }

            // seteamos la fecha minima
            $calendarTarget.datepicker("option", 'minDate', new Date(getCurrentRange('check-in')));

            if ($responsiveTest.is(":visible")) {
                // en mobile sin ningun efecto
                openCheckOutCalendar();
            } else {
                $calendarCont.fadeOut(200, function () {
                    openCheckOutCalendar();
                    $(this).fadeIn();
                });
            }

        }

    });

    function getTheOtherInput($input) {
        if($input.hasClass("check-in")) {
            return $input.closest(".reservation-calendar").find(".check-out");
        }

        return $input.closest(".reservation-calendar").find(".check-in");
    }

    /* efecto hover */
    $calendarTarget.on("mouseleave", "table.ui-datepicker-calendar", function() {
        /* al salir de la tabla dejamos todo como estaba originalmente */
        hoverEffect.clear();
        setCalendarHeaderText('check-in', 'reset');
        setCalendarHeaderText('check-out', 'reset');
        resetNumberOfNightsFromHover();
    });

    var hoverEffect = {
        tableSelector: 'table.ui-datepicker-calendar',
        selectAllNext: function($actual) {
            /* selecciona desde el elemento actual todos los demas hasta el .range-last */
            var $last = $(this.tableSelector+" td.range-last");
            var $allTds = $(this.tableSelector+" td");

            var actualPos = $allTds.index($actual);
            var lastPos = $allTds.index($last);

            var from, to;

            if (actualPos <= lastPos) {
                // selecciono todo desde actual pos hasta last pos
                from = actualPos;
                to = lastPos;
            } else {
                from = to = actualPos;
            }

            this.selectHover($allTds, from, to);
        },
        selectAllPrev: function($actual) {
            /* selecciona desde el elemento actual todos los demas hasta el .range-first */
            var $first = $(this.tableSelector+" td.range-first");
            var $allTds = $(this.tableSelector+" td");

            var actualPos = $allTds.index($actual);
            var firstPos = $allTds.index($first);

            if (actualPos < firstPos) {
                return;
            }

            this.selectHover($allTds, firstPos, actualPos);
        },
        selectHover: function($elements, from, to) {
            var n = 0;
            $elements.each(function(){

                if (n >= from && n <= to) {
                    $(this).removeClass("hover-inactive").addClass("hover-active");

                    if (n === from) {
                        $(this).addClass("hover-first");
                        setHoverCalendarHeaderText($(this), 'check-in');
                    } else {
                        $(this).removeClass("hover-first");
                    }

                    if (n === to) {
                        $(this).addClass("hover-last");
                        setHoverCalendarHeaderText($(this), 'check-out');
                    } else {
                        $(this).removeClass("hover-last");
                    }
                } else {
                    $(this).removeClass("hover-active hover-first hover-last").addClass("hover-inactive");
                }

                n++;
            });
        },
        clear: function() {
            $(this.tableSelector+" td").removeClass("hover-active hover-inactive hover-first hover-last");
        }
    };

    $calendarTarget.on("mouseenter", "td:not(.ui-datepicker-unselectable)", function() {
        if (getCurrentRange("check-in") == 0 && getCurrentRange("check-out") == 0) {
            // si no tengo ambas fechas el hover no hace nada

            setHoverCalendarHeaderText($(this), $inputWrapper.data("input-type"));

            return;
        }
        if ($inputWrapper.data("input-type") == "check-in") {
            hoverEffect.selectAllNext($(this));
        } else {
            hoverEffect.selectAllPrev($(this));
        }

        setNumberOfNightsFromHover();
    });

    /* ocultar el datepicker al hacer click afuera */
    $(document).click(function(event) {
        var $target = $(event.target);
        if ($target.hasClass("field-calendar")
            || $target.hasClass("ui-icon")
            || $target.hasClass("ui-datepicker-next")
            || $target.hasClass("ui-datepicker-prev")) {
            return;
        }

        if ($target.closest('.check-in, .check-out').length != 0) {
            // no hacemos nada
        } else if($calendarTarget.find($target).length == 0) {
            if($target.attr('id') !== 'flexible' && $target.attr('for') !== 'flexible') {
                hideCalendar(200);
            }
        }
    });

    /* checkbox de fechas flexibles */
    // $calendarModule.find(".search-calendar-footer input").click(function(event){
    //     event.preventDefault();
    //     $.datepicker._clearDate($calendarTarget);
    //     $reservationCalendar.data("current-range-check-in", 0);
    //     $reservationCalendar.data("current-range-check-out", 0);
    //     $reservationCalendar.find(".check-in input, .check-out input").val('');
    //     setCalendarHeaderText('check-in', 0);
    //     setCalendarHeaderText('check-out', 0);
    //     $calendarTarget.removeClass("is-checkin-date").removeClass("is-checkout-date");
    // });

    $("#check-in #check-out").attr("tabindex",-1).focus(); // WHAT?

    /* functions */
    function setCalendarHeaderText(checkInOrCheckOut, date) {

        if (date === "reset") {
            date = getCurrentRange(checkInOrCheckOut);
        }

        if (typeof(date) == "number") {
            date = !date ? null : new Date(date);
        }

        var $formGroup = $reservationCalendar.find("."+checkInOrCheckOut).removeClass("has-date");

        if (!$formGroup.attr("data-text-initial")) {
            $formGroup.attr("data-text-initial", $formGroup.attr("data-text"));
        }

        var $elementTitle = $calendarModule.find("."+checkInOrCheckOut+"-title");
        var $elementDate = $calendarModule.find("."+checkInOrCheckOut+"-date");

        if (!date) {
            $elementTitle.removeClass("active");
            $elementDate.removeClass("active").empty();
            $formGroup.attr("data-text", $formGroup.attr("data-text-initial"));
            return;
        }

        $elementTitle.addClass("active");

        var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
        var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        var text = days[date.getDay()]+' '+date.getDate()+' de '+months[date.getMonth()]+' de '+date.getFullYear();

        var dataText = (date.getDate() < 10 ? '0'+date.getDate() : date.getDate())
            +"-"+(date.getMonth() < 9 ? '0'+(date.getMonth()+1) : date.getMonth()+1)
            +"-"+date.getFullYear()+' '+days[date.getDay()];

        $formGroup.attr("data-text", dataText).addClass("has-date");

        $elementDate.text(text);
    }

    function setHoverCalendarHeaderText($td, checkInOrCheckOut) {
        var selectedDate = new Date($td.data("year"), $td.data("month"), $td.text());
        setCalendarHeaderText(checkInOrCheckOut, selectedDate);
    }

    function setNumberOfNightsFromHover() {
        var startDate = $(".hover-active.hover-first");
        if (startDate.length) {
            startDate = new Date(startDate.data("year"), startDate.data("month"), startDate.text());
        } else {
            startDate = new Date(getCurrentRange("check-in"));
        }

        var endDate = $(".hover-active.hover-last");
        endDate = new Date(endDate.data("year"), endDate.data("month"), endDate.text());

        $calendarModule.find(".total-nights").text(getNumberOfNights(startDate, endDate));
    }

    function resetNumberOfNightsFromHover() {
        if (getCurrentRange("check-in") == 0 || getCurrentRange("check-out") == 0) {
            $calendarModule.find(".total-nights").text('');
            return;
        }

        var startDate = new Date(getCurrentRange("check-in"));
        var endDate = new Date(getCurrentRange("check-out"));

        $calendarModule.find(".total-nights").text(getNumberOfNights(startDate, endDate));
    }

    function getNumberOfNights(date1, date2) {
        if (!date1 || !date2) return '';
        var difference = Math.abs(date1.getTime() - date2.getTime())
        return Math.round(difference/(60*60*24*1000));
    }

    /* responsive */
    var $responsiveTest = $("<div />", {'class': 'visible-xs'}).appendTo($("body"));

    function setDatepickerNumberOfMonths() {
        if ($responsiveTest.is(":visible")) {
            $calendarTarget.datepicker("option", "numberOfMonths", 1);
        } else {
            $calendarTarget.datepicker("option", "numberOfMonths", 2);
        }
    }
    setDatepickerNumberOfMonths();
    $(window).resize(setDatepickerNumberOfMonths);

    function updateTodayText() {
    $('.calendar-wrapper').each(function () {
      var $this = $(this),
          todayText = $this.data('today-text');
      $this.find('.ui-datepicker-today .ui-state-default').attr('data-today-text', todayText);
    });
  }

  $(document).on('click', function () {
    setTimeout(updateTodayText, 100);
  });
  updateTodayText();

})(jQuery);