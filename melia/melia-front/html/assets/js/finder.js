(function($){

    var destinationController = {
        init: function (el) {
            destinationController.setup(el);
            destinationController.placeHolderShow(el);
        },

        setup: function (el) {

            if(isIE(9, 'lte')) $(el).val($(el).attr("placeholder-text"));

            $(el).on('focus', destinationController.show);

            $(el).on('blur', destinationController.hide);

            $(el).on('click', destinationController.onClick);

            $(el).on('change past keyup', destinationController.compare)

            destinationController.enterController(el);

            $('.last-searches [data-value]')
                .on('click', function () {
                    destinationController.setValue(this, el);
                    destinationController.changeFocus(el);
                    window.setTimeout(function(){ $("#check-in").trigger("click");}, 500);
                })
                .on('mouseenter', function () {
                    if( $(el).val().length == 0 ) {
                        destinationController.placeHolderHide(el);
                        destinationController.setText(this, '.autosuggest-div');
                    }
                })
        },

        setValue: function (el, target) {
            $(target).val( $(el).data('value') );
        },

        setText: function (el, target) {
            $(target).text( $(el).data('value') );
        },

        onClick: function () {
            $(this).select();
        },

        show: function () {
            $(this).siblings('.autosuggest-div').show();
            $($(this).data('show')).fadeIn(200);
        },

        hide: function () {
            var el = this;
            $(el).siblings('.autosuggest-div').hide().empty();
            $($(el).data('show')).delay(10).queue(function (next) {
                $(this).fadeOut(200);
                if( $(el).val().length == 0 ) {
                    destinationController.placeHolderShow(el);
                }
                next();
            });

        },

        changeFocus: function (el) {
            $($(el).data('next-focus')).focus();
        },

        enterController: function (el) {
            $(window).keydown( function (event) {
                if( (event.keyCode == 13) && $(el).is(':focus') ) {
                    event.preventDefault();
                    if ( $('.autosuggest-div').text().length > 0 ) {
                        $(el).val($('.autosuggest-div').text());
                        destinationController.changeFocus(el);
                    } else {

                    }
                    return false; 
                }
            });
        },

        placeHolderShow: function (el) {
            $(el).attr('placeholder', $(el).attr('placeholder-text'));
        },

        placeHolderHide: function (el) {
            $(el).attr('placeholder', '');
        },

        compare: function () {
            var suggest = $('.autosuggest-div').text().toUpperCase();
            var input = $('#destination').val().toUpperCase();
            suggest = suggest.substring(0, input.length);
            if (suggest != input) $('.autosuggest-div').empty();
        }

    }

    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> li:not(.ui-category)" );
        },

        _renderMenu: function( ul, items ) {
            var that = this,
            ulCategory,
            liCategory,
            currentCategory = "",
            wordRegex = this.term.split(' ').join('|');

            $.each( items, function( index, item ) {
                var li;
                if ( item.category_type != currentCategory ) {
                    ulCategory = $("<ul />", {'class': 'ul-category', 'data-text':item.category_label});
                    ul.append( $("<li />", { 'aria-label': ' ', "class": "ui-category " + item.category_type }).append(ulCategory) );
                    currentCategory = item.category_type;
                }

                li = that._renderItemData( ulCategory, item );
                li.html(li.text().replace(new RegExp("(" + wordRegex + ")", "gi"), '<b>$1</b>'));

                if ( item.category ) {
                    li.attr( "aria-label", item.category + " : " + item.label );
                }
            });
        }
    });

$( "#destination" )
.catcomplete({
    delay: 0,
    source: function () {
        if (!!$('#destination').closest('.mice').length) return miceSearch;
        else return citiesAndHotels;
    }(),
    appendTo: '#predictor .list',
    minLength: 3,
    focus: function (event, ui) {
        event.preventDefault();

        if (!ui.item) return;

        var value = $(this).val().toUpperCase();
        var item = ui.item.label.toUpperCase();
        if ( item.substring(0, value.length) === value ) {
            $(this).siblings('.autosuggest-div').text(ui.item.label);
        } else {
            $(this).siblings('.autosuggest-div').empty();
        }  
    },
    response: function(event, ui) {
        var noresults = $(this).data('noresults-message');
        if (!ui.content.length) {
            var noResult = { 
                value: "noResults", 
                label: noresults
            };
            ui.content.push(noResult);                    
        } else {
            $("#message").empty();
        }
    },
    select: function(event, ui) {
        this.value = (ui.item ? ui.item.name : '');
        if( ui.item.value != 'noResults') {
            destinationController.changeFocus(this);
            window.setTimeout(function(){ $("#check-in").trigger("click");}, 500);
        }
    }
})

.blur(function () {
    $('#destination').catcomplete('close');
    $('#predictor').fadeOut(200);
})

.on( "catcompleteopen", function( event, ui ) {
    $('.last-searches').hide();
    $('#predictor').show();
    hideCalendar(1);
})

.on( "catcompleteclose", function( event, ui ) {
    $(this).siblings('.autosuggest-div').empty();
    //$('.last-searches').show();
    $('#predictor').hide();
});

destinationController.init('#destination');
})(jQuery);


(function ($) {

    var ocupationController = {

        showed: false,

        init: function (context) {
            ocupationController.setup(context);
            ocupationController.preventLinks(context);
        },

        setup: function (context) {
            ocupationController.showValues();
            $('#ocupationshow').on('click', ocupationController.showSelection);
            $('#mostpopular li').on('click', ocupationController.valueMostPopular);
            $('#morebutton').on('click', ocupationController.buttonMore);
            $('#addroom').on('click', ocupationController.addRoom);
            $(context).on('click', '.removeroom', ocupationController.removeRoom);
            $(context).on('change', 'select', ocupationController.refreshOcupation);
            $(context).on('change', '.children-select', ocupationController.addingChildren);
            $('#ocupationclose').click(ocupationController.hideSelection);
        },

        showValues: function () {
            $('#ocupationshow .rooms span').text($('#totalrooms').val());
            $('#ocupationshow .adults span').text($('#totaladults').val());
            $('#ocupationshow .children span').text($('#totalchildren').val());
        },

        valueMostPopular: function () {
            $('#totalrooms').val($(this).data('rooms'));
            $('#totaladults').val($(this).data('adults'));
            $('#totalchildren').val($(this).data('children'));
            ocupationController.showValues();
            ocupationController.hideSelection();
            ocupationController.resetRooms();
        },

        refreshRoomsValue: function () {
            $('#totalrooms').val(ocupationController.roomsCounter);
            ocupationController.showValues();
        },

        refreshOcupation: function () {
            var rm = ocupationController.roomsCounter;
            var adults = 0;
            var children = 0;
            for ( i=1 ; i<=rm ; i++ ) {
                adults += parseInt($('#room' + i + '-adults').val());
                children += parseInt($('#room' + i + '-children').val());
            }
            $('#totaladults').val(adults);
            $('#totalchildren').val(children);
            ocupationController.showValues();
        },

        buttonMore: function () {
            $('#mostpopular').fadeOut(100, function () {
                $('#moreoptions').fadeIn(100).css('display', 'inline-block');
                ocupationController.refreshRoomsValue();
                ocupationController.refreshOcupation();
            });
        },

        roomsCounter: 1,

        addRoom: function () {

            ocupationController.roomsCounter++;
            var i = ocupationController.roomsCounter;
            var room = $('#room1').html();

            var legend = $('#room1 .room-tittle').html().replace('1', i);
            var labelAdults = $('label[for="room1-adults"]').html().replace('1', i);
            var labelChildren = $('label[for="room1-children"]').html().replace('1', i);
            var labelAgeChild1 = $('label[for="room1-children1"]').html().replace('1', i);
            var labelAgeChild2 = $('label[for="room1-children2"]').html().replace('1', i);
            var labelAgeChild3 = $('label[for="room1-children3"]').html().replace('1', i);
            var labelAgeChild4 = $('label[for="room1-children4"]').html().replace('1', i);

            room = room.replace(new RegExp('room1', 'g') , 'room' + i );

            var container = '<fieldset id="room' + i +'" class="aroom" data-room="' + i + '"></fieldset>';
            $('#roomsadded').append(container);
            $('#room' + i).append(room);
            $('#roomsadded').data('rooms', i);

            $('#room' + i + ' .room-tittle').html(legend);
            $('label[for="room' + i + '-adults"]').html(labelAdults);
            $('label[for="room' + i + '-children"]').html(labelChildren);
            $('label[for="room' + i + '-children1"]').html(labelAgeChild1);
            $('label[for="room' + i + '-children2"]').html(labelAgeChild2);
            $('label[for="room' + i + '-children3"]').html(labelAgeChild3);
            $('label[for="room' + i + '-children4"]').html(labelAgeChild4);
            $('#room' + i + '-age').addClass('hide');
            ocupationController.addRoomButton();
            ocupationController.refreshRoomsValue();
            ocupationController.refreshOcupation();
        },

        removeRoom: function () {
            var i = ocupationController.roomsCounter;
            var rn = $(this).closest('.aroom').data('room');

            if ( i > 1 && rn != 1 ) {
                $(this).closest('.aroom').remove();

                if ( i != rn ) {
                    ocupationController.renameRooms(rn);
                }
                ocupationController.roomsCounter--;

                ocupationController.addRoomButton();
                ocupationController.refreshRoomsValue();
                ocupationController.refreshOcupation();
            }
        },

        renameRooms: function (rn) {
            do {

                var adults = $( '#room' + (rn+1) + '-adults').val();
                var children = $( '#room' + (rn+1) + '-children').val();

                for (i=1; i <= children; i++) {
                    if( i == 1 ) {
                        var childrenAges = new Array();
                    }
                    childrenAges.push( $('#room' + (rn+1) + '-children' + i + ' select').val() );
                }

                var room = $('#room' + (rn+1)).html();
                var legend = $('#room'+ (rn+1) + ' .room-tittle').html().replace( (rn+1), rn );
                var labelAdults = $('label[for="room' + (rn+1) + '-adults"]').html().replace( (rn+1), rn) ;
                var labelChildren = $('label[for="room' + (rn+1) + '-children"]').html().replace( (rn+1), rn) ;
                var labelAgeChild1 = $('label[for="room' + (rn+1) + '-children1"]').html().replace( (rn+1), rn );
                var labelAgeChild2 = $('label[for="room' + (rn+1) + '-children2"]').html().replace( (rn+1), rn );
                var labelAgeChild3 = $('label[for="room' + (rn+1) + '-children3"]').html().replace( (rn+1), rn );
                var labelAgeChild4 = $('label[for="room' + (rn+1) + '-children4"]').html().replace( (rn+1), rn );

                room = room.replace( new RegExp( 'room' + (rn+1), 'g') , 'room' + rn );

                $( '#room' + (rn+1) ).data('room', rn).attr('id', 'room' + rn).html(room);

                $('#room' + rn + ' .room-tittle').html(legend);
                $('label[for="room' + rn + '-adults"]').html(labelAdults);
                $('label[for="room' + rn + '-children"]').html(labelChildren);
                $('label[for="room' + rn + '-children1"]').html(labelAgeChild1);
                $('label[for="room' + rn + '-children2"]').html(labelAgeChild2);
                $('label[for="room' + rn + '-children3"]').html(labelAgeChild3);
                $('label[for="room' + rn + '-children4"]').html(labelAgeChild4);



                if( children > 0 ) {
                    for ( i=0; i<childrenAges.length; i++ ) {
                        $('#room' + rn + '-children' + (i+1) + ' select').val(childrenAges[i]);
                    }
                }

                $( '#room' + rn + '-adults').val(adults);
                $( '#room' + rn + '-children').val(children);

                rn++;

            } while (rn < ocupationController.roomsCounter);
        },

        addingChildren: function () {
            var children = parseInt($(this).val());
            $(this).closest('fieldset.aroom').data('children', children);
            var rn = $(this).closest('.aroom').data('room');

            for( i = 1; i <= 4; i++ ) {
                if ( i <= children ) {
                    $('#room' + rn + '-age').removeClass('hide');
                    $('#room' + rn + '-children' + i).removeClass('hide');    
                }

                if ( i > children ) {
                    $('#room' + rn + '-children' + i).addClass('hide');
                    $('#room' + rn + '-children' + i + ' select').val('');
                }

                if ( children == 0 ) {
                    $('#room' + rn + '-age').addClass('hide');
                }
            }
        },

        addRoomButton: function () {
            if( ocupationController.roomsCounter >= 5 ) {
                $('#addroom').addClass('hide');
            } else {
                $('#addroom').removeClass('hide');
            }
        },

        resetRooms: function () {
            var rooms = ocupationController.roomsCounter;
            for( i=2; i<=rooms; i++ ) {
                $('fieldset#room' + i).remove();
                ocupationController.roomsCounter = 1;
            }
        },

        showSelection: function () {
            $('#ocupation').addClass('focus');
            $('#ocupationselection').delay(10).queue(function(next) {
                $(this).fadeIn(200);
                ocupationController.showed = true;
                next();    
            });

        },

        hideSelection: function () {
            $('#ocupation').removeClass('focus');
            $('#ocupationselection').delay(10).queue(function(next) {
                $(this).fadeOut(200);
                ocupationController.showed = false;
                next();    
            });
        },

        preventLinks: function (context) {
            var links = context + ' a';
            $(links).click(function (e) {
                e.preventDefault();
            });
        }

    }


    ocupationController.init('#ocupation');

    $(document).click( function(e) {
        if( !$('#ocupationselection').is(e.target) && !$(e.target).closest('#ocupationselection').length && ocupationController.showed == true && !$(e.target).closest('.removeroom').length ) {
            ocupationController.hideSelection();
        }
    });
})(jQuery);

(function($) {

    couponController = {

        init: function () {
            $('#coupon-trigger').on('click', function (e) {
                e.preventDefault();
                if( $('#code-number').val() === '' ) {
                    couponController.toggle.call(this);
                }
            });

            $('#coupon-form').on('submit', couponController.submit);

            $('#coupon .reset').on('click', function (event) {
                event.preventDefault();
                couponController['delete'].call(this);
            });
            $(document).on('click', '#coupon-remove', function (event) {
                event.preventDefault();
                couponController['delete'].call(this);
            });
        },

        toggle: function () {
            specialRatesController.hide();
            $('#coupon').slideToggle();
            var icon = $(this).children('i');
            icon.hasClass('icon-arrow-up')
                ? icon.removeClass('icon-arrow-up').addClass('icon-arrow-down')
                : icon.removeClass('icon-arrow-down').addClass('icon-arrow-up');
        },

        hide: function (op) {
            $('#coupon').slideUp();
            var icon = $('#coupon-trigger').children('i');
            if (icon.hasClass('icon-arrow-up')) icon.removeClass('icon-arrow-up').addClass('icon-arrow-down');
            if (op == 'empty') $('#code-number').val('');
        },

        submit: function () {
            var coupon = $('#code-number').val();
            var text = $('#coupon-trigger').data('with-coupon-text');
            var icon = '<a href="#" id="coupon-remove" class="icon-close"></a>';
            if (coupon !== '') {
                $('#coupon-trigger').children('i').removeClass('icon-arrow-up').addClass('icon-arrow-down');
                $('#coupon-trigger')
                    .data('default-text', $('#coupon-trigger').html())
                    .html('<span class="hidden-xs">' + text + '</span> <span>' + coupon + '</span>')
                    .after(icon);
                $('#coupon-reciever').val(coupon);
                couponController.hide();
                $('#coupon').removeClass('has-error');
                error_control_delete();
            } else {
                $('#coupon').addClass('has-error');
                error_control( $('#coupon.has-error') );
            }
            return false;
            
        },

        'delete': function () {
            if($(this).hasClass('icon-close')) {
                $(this).remove();
            }
            $('#coupon-trigger').html($('#coupon-trigger').data('default-text'));
            $('#coupon-reciever, #code-number').val('');
            couponController.hide();
        }

    }

    couponController.init();


    specialRatesController = {

        init: function () {
            $('#specialrates-trigger').on('click', function (e) {
                e.preventDefault();
                if( !$(this).hasClass('with-value') )
                    specialRatesController.toggle.call(this);
            });
            $('#specialrates a').each(function() {
                $(this).on('click', specialRatesController.submit);
            });
            $('#coupon [type="reset"]').on('click', specialRatesController['delete']);
            $(document).on('click', '#specialrates-remove', function (e) {
                e.preventDefault();
                specialRatesController['delete'].call(this);
            });
        },

        toggle: function () {
            couponController.hide('empty');
            $('#specialrates').slideToggle();
            var icon = $(this).children('i');
            icon.hasClass('icon-arrow-up')
                ? icon.removeClass('icon-arrow-up').addClass('icon-arrow-down')
                : icon.removeClass('icon-arrow-down').addClass('icon-arrow-up');
        },

        hide: function () {
            $('#specialrates').slideUp();  
            var icon = $('#specialrates-trigger').children('i');
            if (icon.hasClass('icon-arrow-up')) icon.removeClass('icon-arrow-up').addClass('icon-arrow-down');
        },

        submit: function () {
            var option = $(this).data('value');
            var text = $(this).text();
            var icon = '<a href="#" id="specialrates-remove" class="icon-close"></a>';
            $('#specialrates-trigger')
            .data('default-text', $('#specialrates-trigger').html())
            .html('<span>' + text + '</span>')
            .after(icon)
            .addClass('with-value');
            $('#specialrates-reciever').val(option);
            specialRatesController.hide();
        },

        'delete': function () {
            $('#specialrates-trigger')
            .html( $('#specialrates-trigger').data('default-text') )
            .removeClass('with-value');
            $(this).remove();
            specialRatesController.hide();
        }

    };

    specialRatesController.init();

})(jQuery);

(function($) {

    var searcher_position = function ($cont, $searcher) {

        if (!$cont.length) {
            return;
        }

        var callback = function() {
            var search_pos = $cont.position().top;
            var window_pos = function () {
                return $(window).scrollTop();
            };
            var calendar_h = 430;
            var calendar_pos = search_pos - calendar_h - window_pos();
            if( calendar_pos < 0 || window.navigator.userAgent.indexOf("iPad") > -1 ) {
                $searcher.addClass('show-below');
                $('#autocomplete').delay(10).queue(function(next){
                    $(this).removeClass('fix');
                    next();
                });
            } else {
                $searcher.removeClass('show-below');
                $('#autocomplete').delay(10).queue(function(next){
                    $(this).addClass('fix');
                    next();
                });
            }
        };

        callback();

        $(document).scroll(callback);
    };

    searcher_position($('.cont-search'), $('section.search'));
    $('.search-container-extra').each(function(){
        searcher_position($(this), $(this));
    });


    if(isIE(8, 'lte')) {
        $('#destination, #check-in, #check-out, #ocupation').on('click', function () {
            $('html,body').animate({scrollTop: $(this).offset().top - 50},'slow');
        })
    }

})(jQuery);