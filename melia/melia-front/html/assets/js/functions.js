//Array.isArray()
if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}


function millarSeparator(integer) {
  var length = integer.toString().length;
  var separators = Math.ceil( (length / 3 ) ) -1; // '-1' para ajuste de separadores

  var separatedInteger = '';
  for(var i = 0; i < separators; i++) {
    var end = length - (i * 3);
    var begin = end - 3;
    var substring = integer.toString().substring( begin, end);
    separatedInteger = '.' + substring + separatedInteger;
  }
  separatedInteger = integer.toString().substring(0, begin) + separatedInteger;

  return separatedInteger;
}
var superI = 0;
function pluralize() {
  $('[data-function="pluralize"]').each(function () {
    var n = Number($(this).find('[data-counter]').html());
    if (n === 1) {
      $(this).find('[data-plural="false"]').removeClass('not-showed');
      $(this).find('[data-plural="true"]').addClass('not-showed');
    } else {
      $(this).find('[data-plural="true"]').removeClass('not-showed');
      $(this).find('[data-plural="false"]').addClass('not-showed');
    }
  });
};

$('.hotels-counter').bind('contentchange', pluralize);


(function () {

  function DisablePinchZoom() {
    var viewport = $('meta[name=viewport]').attr('content');
    var temp_viewport = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no';

    $('select, input')
      .mousedown(function () {
        $('meta[name=viewport]').attr('content', temp_viewport);
      })
      .blur(function () {
        $('meta[name=viewport]').attr('content', viewport);
      });
  };

  DisablePinchZoom();

})();


function isIE(version, comparison) {
  var cc      = 'IE',
      b       = document.createElement('B'),
      docElem = document.documentElement,
      isIE;
      
  if(version){
    cc += ' ' + version;
    if(comparison){ cc = comparison + ' ' + cc; }
  }
  
  b.innerHTML = '<!--[if '+ cc +']><b id="iecctest"></b><![endif]-->';
  docElem.appendChild(b);
  isIE = !!document.getElementById('iecctest');
  docElem.removeChild(b);
  return isIE;
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function scrollToAnchor(aid, fixedElements) {
  var fixedHeight = 0;
  for(i=0;i<fixedElements.length;i++) {
    fixedHeight += $(fixedElements[i]).outerHeight();
  }
  var aTag = $("[name='"+ aid +"']");
  $('html,body').animate({scrollTop: aTag.offset().top - fixedHeight},'slow');
}

function winHeightFree(fixedElements) {
  var fixedHeight = 0;
  for(i=0; i<fixedElements.length; i++) {
    fixedHeight += $(fixedElements[i]).outerHeight();
  }
  return $(window).height() - fixedHeight;
}

// function getJson(callback, url) {
//   $.ajax({
//      url: url,
//      dataType: 'json',
//      success: function(data) {
//       callback(data);
//      }
//   });
// }

function separateInLines(text, lines) {
  var length = text.length;
  var factor = text.length/lines;
  var breakpoints = [];
  for (var i = 1; i < lines; i++) {
    breakpoints.push(Math.round(factor*i));
  }

  var spaceIndexes = [], i = -1;
  while ((i = text.indexOf(' ', i+1)) != -1) {
    spaceIndexes.push(i);
  }

  for (i = 0; i < breakpoints.length; i++) {
    var breakpoint = breakpoints[i];
    var distance = Math.abs(spaceIndexes[0] - breakpoint);
    var idx = 0;
    for (var c = 1; c < spaceIndexes.length; c++) {
      var cdistance = Math.abs(spaceIndexes[c] - breakpoint);
      if(cdistance < distance) {
        idx = c;
        distance = cdistance;
      }
    }
    breakpoints[i] = spaceIndexes[idx];
  }

  var textSlices = [];
  var i = 0;
  do {
    var ini = (i === 0) ? 0 : breakpoints[i-1]+1;
    var end = (i === breakpoints.length) ? text.length : breakpoints[i];
    textSlices.push(text.substring(ini, end));
    i++;
  } while (i <= breakpoints.length);
  text = '';
  for (var i=0; i<textSlices.length; i++) {
    text += textSlices[i];
    if (i+1 < textSlices.length) {
      text += '<br>';
    }
  }
  return text;

}


$('input, textarea').placeholder();

//Function to make placeholder work on Ie9
(function() {


  var placeholder_submenu_ie = function() {
    $('.submenu input').each(function () {
      var placeholder = $(this).attr('placeholder');
      if (!!placeholder) {
        var ie_placeholder = '<span class="ie-placeholder">' + placeholder + '</span>';
        $(this).closest('.form-group').addClass('with-placeholder');
        $(this).after(ie_placeholder);
      };
      $(this).on('focus', function () {
        $(this).siblings('.ie-placeholder').hide();
      });
      
      $(this).on('blur', function () {

        if(this.value === '') {
          $(this).siblings('.ie-placeholder').show();  
        }
      });
    });
  }

     if( isIE(9, 'lte') ) { 
      placeholder_submenu_ie();
    }
  
})();



(function () {
  if( isIE(8, 'lte') ) {
    $('[data-image-src]').each(function () {
      var img = $(this).data('image-src');
      $(this).css({
        'filter': "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + img + "', sizingMethod='scale');",
        '-ms-filter': "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + img + "', sizingMethod='scale');"
      });
    });
    
    
  };
})();

// IndexOf Prototype for IE8
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
    "use strict";
    if (this == null) {
      throw new TypeError();
    }
    var t = Object(this);
    var len = t.length >>> 0;
    if (len === 0) {
      return -1;
    }
    var n = 0;
    if (arguments.length > 0) {
      n = Number(arguments[1]);
      if (n != n) { // shortcut for verifying if it's NaN
        n = 0;
      } else if (n != 0 && n != Infinity && n != -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
    for (; k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
  return -1;
  }
}

//Main menú
$(function(){

  $('#menu-toggle').click(function() {
    if(!$('body').hasClass('menu-opened')) {
      $('body').addClass('menu-opened').delay(500).queue(function (next) {
        $(this).addClass('ready');
        next();
      });
      $('#menu').fadeIn();
      if ( $('header.navbar').hasClass('affix') ) {
        $('body').data('header', 'affix');
      }
      if ( $('header.navbar').hasClass('affix-top') ) {
        $('body').data('header', 'affix-top');
      }
    } else {
      $('body').removeClass('menu-opened ready');
      $('#menu').fadeOut();
      if ( $('body').data('header') === 'affix' ) {
        $('header.navbar').delay(100).queue(function (next) {
          $(this).removeClass('affix-top').addClass('affix');
          next();
        });
      }
      if ( $('body').data('header') === 'affix-top' ) {
        $(this).addClass('affix-top').removeClass('affix');
      }
    }
  });

  $(document)
  .on('click', '#menu-lang-trigger', function () {
    $('#menu').addClass('menu-lang');
  })
  .on('click', '.languages h3 a', function () {
    $('#menu').removeClass('menu-lang');
  });

  $(document)
  .on('focus', '.custom-select select', function () {
    $(this).parent().addClass('focus');
  })
  .on('blur', '.custom-select select', function () {
    $(this).parent().removeClass('focus');
  });

  var multiLevelMenuController = {

    init: function (link) {
      multiLevelMenuController.setup(link);
    },

    setup: function (link) {
      $(link).each(function () {
        $(this).on('click', function (e) {
          if( $(this).attr('data-target') ) {
            e.preventDefault();
          }
          multiLevelMenuController.onClick.call(this);
        });
      });
    },

    onClick: function () {
      if( $(this).closest('.first-level').length ) {
        multiLevelMenuController.toSecondLevel.call(this);
      } else {
        if( $(this).hasClass('back') ) {
          multiLevelMenuController.toFirstLevel.call(this);
        }
      }
    },

    toSecondLevel: function () {
      var target = $(this).data('target');
      $(target).addClass('show');
      $(this).closest('#wrapper-inner').find('.second-level').scrollTop(0);
      $(this).closest('#wrapper-inner').addClass('to-second-level');
    },

    toFirstLevel: function () {
      $(this).closest('#wrapper-inner').find('.first-level').scrollTop(0);
      $(this).closest('#wrapper-inner')
      .removeClass('to-second-level')
      .addClass('to-first-level');
      $('.second-level').delay(200).queue(function(next){
        $(this).removeClass('show');
        next();
      })
    }

  };

  multiLevelMenuController.init('#menu-wrapper a');
});

// SliderToggleler
(function () {
    var slideToggleController = {
        init: function (elements) {
            slideToggleController.setup(elements);
        },

        setup: function (elements) {
            $(elements).each(function () {
                $(this).on('click', slideToggleController.onClick);
            });
        },

        onClick: function () {
            slideToggleController.toggleTarget.call(this);

          if( $(this).attr('data-toggle-text') ) {
              slideToggleController.toggleText.call(this);
            }

            if ( $(this).attr('data-toggle-class') ) {
              slideToggleController.toggleClass.call(this);
            }
        },

        toggleTarget: function () {
            var target = $(this).data('toggle-target');
            $(target).slideToggle();
        },

        toggleText: function () {
            var toggleText = $(this).data('toggle-text');
            var defaultText = $(this).html();
            $(this)
                .data('toggle-text', defaultText)
                .html(toggleText);
        },

        toggleClass: function () {
          var toggleClass = $(this).data('toggle-class')[0];
          var defaultClass = $(this).data('toggle-class')[1];
          $(this)
            .toggleClass(toggleClass)
            .toggleClass(defaultClass);
        }
    };

    slideToggleController.init('[data-toggle="slideToggle"]');

})();


//SlideToogle for submenús
(function(){

  var submenuToggleController = {
    init: function (elements) {
      submenuToggleController.setup(elements);
    },

    setup: function (elements) {
      $(elements).on('click', function() {
        if( $(this).attr('aria-expanded') == 'false' ) {
            submenuToggleController.hideOthers.call(this);          
        }
        if(!$(this).hasClass('navbar-brand') || !$('body').hasClass('menu-opened')) {
          if( isIE(8, 'lte') ) {
            submenuToggleController.onClick.call(this);
          } else {
            if (Modernizr.mq('(min-width: 992px)') || $(this).data('target') !== '#brands-list') {
                submenuToggleController.onClick.call(this);
            }
          }
        }
      });
    },

    hideOthers: function () {
      $('.submenu').slideUp().removeClass('open');
      $('header').removeClass('submenu-opened');
      $('.navbar [data-target]').attr('aria-expanded', 'false').removeClass('active');
    },

    onClick: function () {
      if( $(this).attr('aria-expanded') == 'false' ) {
        $('header, body').addClass('submenu-opened'); 
        $(this).addClass('active');
      } else {
        $('header, body').removeClass('submenu-opened');
        $(this).delay(300).queue(function (next) {
          $(this).removeClass('active');
          next();
        });
      }

      submenuToggleController.toggleTarget.call(this);
      if($(this).attr('aria-expanded')) {
        submenuToggleController.ariaToggle.call(this);
      }
    },

    toggleTarget: function() {
      var target = $($(this).data('target'));
      if ($(this).attr('aria-expanded') == 'false') {
        $(target).delay(150).slideDown(300);
      } else {
        $(target).slideUp(300);
      }
      $(target).toggleClass('open');
    },

    ariaToggle: function() {
      if ($(this).attr('aria-expanded') == 'true') {
        $(this).attr('aria-expanded', 'false');
      } else {
        $(this).attr('aria-expanded', 'true');
      }
    }
  };
  submenuToggleController.init('.navbar [data-target], [data-target].submenu-toggle');

  $(window).on('resize', function () {
    submenuToggleController.hideOthers();
  });

  $(document).click( function(e) {
    if( !$(e.target).closest('.submenu').length && !$('.navbar [data-target], .navbar [data-target] *, .submenu a, .submenu-toggle').is(e.target) ) {
      $('.submenu').slideUp(300);
      $('.submenu-toggle').removeClass('active').attr('aria-expanded', 'false');
      $('.navbar a.active').delay(300).queue(function(next){
        $(this).removeClass('active').attr('aria-expanded', 'false');
        next();	
      });
    }
  });

})();



(function () {

  var linkTargetController = {
    init: function (triggers) {
      $(triggers).each(function () {
        linkTargetController.setup.call(this);
      });
    },

    setup: function () {
      $(this).on('click', function () {
        linkTargetController.onClick.call(this);
      });
    },

    onClick: function () {
      var toShow = $(this).data('to-show');
      var toHide = $(this).closest('form');
      linkTargetController.contentToggle(toHide, toShow);
    },

    contentToggle: function (toHide, toShow) {
      $(toHide).fadeOut(300, function () {
        $(toShow).fadeIn(300);
        if ($(this).hasClass('active')) {
          $(this).removeClass('active');
          $(toShow).addClass('active');
        }
      });
    }
  };

  linkTargetController.init('.login-interaction');

})();


(function() {
  $('#newsletter-form').on('submit', function () {
    var url = $(this).data('modal-url');
    var url_succeed = 'includes/modules/ajax/modal-newsletter-succeed.html';
    var url_error = 'includes/modules/ajax/modal-newsletter-error.html';
    var second_modal = (Math.random()*10 > 5) ? url_succeed : url_error;
    console.log(second_modal);
    var email = $(this).find('input[name="email"]').val();
    BootstrapDialog.show({
      title: '',
      message: $('<div></div>').load(url),
      onshown: function () {
        $("#newsletter-form-modal input[name='email']").val(email);
        modalOnSubmit('#newsletter-form-modal', second_modal);
      }
    });
    return false;
  });

  var modalOnSubmit = function (form, url) {
    $(form).on('submit', function (e) {
      $('.modal').modal('hide');
      BootstrapDialog.show({
        title: '',
        message: $('<div></div>').load(url)
      });
      return false
    });
  };

  var closeButton = function (el) {
    $(document).on('click', el, function () {
      $('.modal').modal('hide');
    })
  }

  closeButton('#close-modal');

  $('.btn-modal').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('modal-url');
    var cssClass = $(this).data('type');
    BootstrapDialog.show({
      title: '',
      message: $('<div></div>').load(url),
      cssClass: cssClass
    });
  });

})();



// Carousel
(function(){
  var SwipeCarrousel = {
    init: function (elements) {
      $(elements).each(function() {

        SwipeCarrousel.addSwipeSupport.call(this);
      });
    },

    addSwipeSupport: function () {
      $(this)
      .swiperight(function(){ $(this).carousel('prev') })
      .swipeleft (function(){ $(this).carousel('next') });
    }
  };
  SwipeCarrousel.init($('.carousel'));

  var PauseCarousel = {
    init: function (elements) {
      $(elements).each(function() {
        $(this).carousel({
          pause: true,
          interval: false      
        })
      })
    }
  }
  PauseCarousel.init($('.carousel'));
})();


// Coockies
$("#clickme").click(function(){
  $("#cookies").hide();
});

$("#show").click(function(){
  $("p").show();
});


//We call you
$('#phone-free .country select').on('change', function () {
  $('#phone-free .phone .prefix').text($(this).val());
});


//Menu hotel
(function(){
  var items = $('.hotel-menu li').length;
  $('.hotel-menu ul').addClass('items-' + items);
})();



$('.img-container').imagefill();
$(window).resize(function() {
  $('.img-container').imagefill();
});
$('.tab-pane, .carousel .item').bind('onbeforeaddClass', function (e) {
  if (!$(this).hasClass('active')) {
    $(this).find('.img-container').css( 'opacity', '0')
      .delay(150)
      .queue(function (next) {
        $(this).css('opacity', '1');
        next();
      });
  }
  
  $('.img-container').imagefill();
});

//Middle content for supporting ie8
$(function() {

  var middleContent = function() {
    $('.middle-content').each(function() {
      $(this).css('margin-top', - $(this).outerHeight() / 2);
    });
  };
  middleContent();
  $(window).resize(function() {
    middleContent();
  });
  $.hook('addClass');
  $('.tab-pane, .carousel .item').bind('onafteraddClass', function () {
    middleContent();
  });
});



$('.more-container').responsiveMore({
  moreText: 'View more',
  lessText: 'View less',
  moreIconClass: 'icon-arrow-down',
  lessIconClass: 'icon-arrow-up',
  mediaQueries: 'bootstrap'
});


// MODAL SHARE FRIEND
(function(){

    var $modal = $("#modal-share-friend");
    var $modalContent = $modal.find(".modal-content");

    $modal.find("form").submit(function(event){
        event.preventDefault();

        // validar...

        var email = $(this).find("input[name='email-to']").val();

        $modal.find(".email-to-placeholder").text(email);

        $modalContent.toggleClass("hide");
    });

    // reseteamos todo
    $modal.on("hidden.bs.modal", function(event){
        $modalContent.removeClass("hide");
        $modalContent.filter(".modal-content-success").addClass("hide");
    });

})();

// Disable all links with an empty href attr
$('[href=""]').attr('href', 'javascript:void(0)');

// Destination grid - Hidden last if alone on tablet
var hideLast = function () {
  $('.destination-grid').each(function() {
    $(this).find('.grid-col').last().show();
    var pos = [];
    $(this).find('.grid-col').each(function() {
      pos.push($(this).position().top);
    })
    var last = pos[pos.length-1];
    var last_bef = pos[pos.length-2];
    
    if (last !== last_bef && isIE(8, 'gte')) {
      $(this).find('.grid-col').last().hide();
    }
  });
};
hideLast();
$(window).resize(hideLast);


// Filter controller 


(function() {

  $('.filters-trigger').on('click', function () {
    var target = $($(this).data('target'));
    if ( $(this).hasClass('active') ) {
      $(this).removeClass('active');
      var itemsChecked = $('.all-filters input[type="checkbox"]:checked').length;
      if (itemsChecked !== 0) {
        $(this).find('.text').append('<span id="items-checked"> (' + itemsChecked + ')</span>');
      }
      target.slideUp();
    } else {
      $(this).addClass('active');
      $(this).find('#items-checked').remove();
      target.slideDown();
    }
  });



$(function() {
    var slider = {
      init: function (el) {
        $(el).each(function () {
          slider.setup(this);
        });
        $('#reset').on('click', function () {
          slider.reset('.super-slider');
        });
      },

      setup: function (el) {
        var range = $(el).data('range');
        slider.render.slider(el, range);
        if (range === true) slider.render.values(el, slider.values.maxVal(el), slider.values.minVal(el));
        else slider.render.values(el, slider.values.maxVal(el));
        slider.slide(el, range);
      },

      values:{
        minVal: function (el) { return $(el).data('min-val'); },
        maxVal: function (el){ return $(el).data('max-val'); },
      },

      units: function(el) { 
        var data = $(el).closest('.filter-cont').find('.amount').data('units');
        if (!!data) {
          return data;  
        } else {
          return '';
        }
      },
        
      unitsPos: function(el) { 
        var data = $(el).closest('.filter-cont').find('.amount').data('units-pos');
        if (!!data) {
          return  data;  
        } else {
          return '';
        }
      },

      step: function(el) { return $(el).data('step'); },
       
      render:{
        slider: function (el, range) {
          $(el).slider({
            range: function () {
              if (range === true) return true;
              else return 'min';
            }(),
            min: function () {
              return slider.values.minVal(el);
            }(),
            max: function () {
              return slider.values.maxVal(el);
            }(),
            value: function () {
              if (range === true) {
                return 0;
              } else {
                return slider.values.maxVal(el);
              }
            }(),
            values: function () {
              if (range === true) {
                var values = new Array();
                values.push(slider.values.minVal(el));
                values.push(slider.values.maxVal(el));
                return values;
              } else {
                return null;
              }
            }(),
            step: function () {
              return slider.step(el);
            }(),
            animate: true,
          });
        },
        values: function (el, maxRange, minRange) {
          if (slider.unitsPos(el) === 'before') {
            var html = '';
            html += (minRange !== undefined) ? "<span class='slider-units'>" + slider.units(el) + "</span><span id='minPrice'>" + minRange + "</span> - " : "";
            html += "<span class='slider-units'>"+ slider.units(el) + "</span><span id='maxPrice'>" + maxRange + "</span>";
            $(el).closest('.filter-cont').find('.amount').html(html);

          } else {
            var html = '';
            html += (minRange !== undefined) ? "<span id='minPrice'>" + minRange + "</span><span class='slider-units'>" + slider.units(el) + "</span> - " : "";
            html += "<span id='maxPrice'>" + maxRange + "</span><span class='slider-units'>" + slider.units(el) + "</span>";
            $(el).closest('.filter-cont').find('.amount').html(html);
          }
        },
      },

      reset: function (el) {
        $(el).each(function () {
          var range = $(this).data('range'); 
          if (range === true) {
            $(this).slider('values', [slider.values.minVal(this), slider.values.maxVal(this)] );
            slider.render.values(this, slider.values.maxVal(this), slider.values.minVal(this) );
          } else {
            $(this).slider('value', slider.values.maxVal(this) );
            slider.render.values(this, slider.values.maxVal(this) );
          }
        });        
      },

      slide: function (el, range) {
        $(el).on('slide', function (event, ui) {
          if (range === true) slider.render.values(this, ui.values[1], ui.values[0]);
          else slider.render.values(this, ui.value);
        });
      },
    };

    slider.init('.super-slider', 'render');


});


  $('form.all-filters').on('submit', function () {
    var fixedEl = ['header', 'section.search'];
    scrollToAnchor('anchor', fixedEl);
    return false;
  })


  var PointsSlider = {

    init: function (slider) {
      $slider = $(slider);
      if($slider.length) {
        PointsSlider.setup($slider);
      }
    },

    values: {
      min: function () {
        return $('#slider-select-points').data('min-value');
      }(),
      max: function () {
        return $('#slider-select-points').data('max-value');
      }(),
      units: function () {
        return $('#slider-select-points').data('units');
      }()
    },

    setup: function ($slider) {
      $slider.slider({
        min: PointsSlider.values.min,
        max: PointsSlider.values.max,
        range: "min",
        step: 100,
        animate: true,
        slide: function (event, ui) {
          PointsSlider.render.value(ui.value, PointsSlider.values.units);
        }
      });
      PointsSlider.render.minMax(PointsSlider.values.min, PointsSlider.values.max, PointsSlider.values.units);
      PointsSlider.render.value($slider.slider('value'), PointsSlider.values.units);
      PointsSlider.change.init($slider);
      if(!!$slider.data('tooltip')) {
        PointsSlider.tooltip.init($('#slider-select-points .ui-slider-handle'), $slider);
      }
    },

    render: {
      value: function (value, units) {
        $('#points-selected, #points-selected-2').text(millarSeparator(value) + units);
      },
      minMax: function (min, max, units) {
        $('#min-points').text(millarSeparator(min) + units);
        $('#max-points').text(millarSeparator(max) + units);
      }
    },

    change: {
      init: function ($slider) {
        var intStart, intIn;
        $('.trigger-change').on('mousedown', function (event) {
          event.preventDefault();
          if(event.which === 1) {

            var $trigger = $(this);
            var firstEnter = true;

            if ($trigger.attr('id') === 'substract') {
              PointsSlider.change.substract($slider, $trigger);

              intStart = setInterval(function () {
                PointsSlider.change.substract($slider, $trigger);
                if (firstEnter == true) {
                  intIn = setInterval(function () {
                    PointsSlider.change.substract($slider, $trigger);
                  }, 20);
                };

                firstEnter = false;

              }, 500);

            } else if ($trigger.attr('id') === 'add') {
              PointsSlider.change.add($slider, $trigger);

              intStart = setInterval(function () {
                PointsSlider.change.add($slider, $trigger);
                if (firstEnter == true) {
                  intIn = setInterval(function () {
                    PointsSlider.change.add($slider, $trigger);
                  }, 20);
                };
                
                firstEnter = false;

              }, 500);
            };
          };
        });

        $(document).on('mouseup', function () {
            clearInterval(intStart);
            clearInterval(intIn);
        });

      },
      substract: function ($slider, $trigger) {
        var qt = $trigger.data('quantity');
        var val = $slider.slider('value');
        $slider.slider('option', 'value', val-qt);
        PointsSlider.render.value($slider.slider('value'), PointsSlider.values.units);
      },
      add: function ($slider, $trigger) {
        var qt = $trigger.data('quantity');
        var val = $slider.slider('value');
        $slider.slider('option', 'value', val+qt);
        PointsSlider.render.value($slider.slider('value'), PointsSlider.values.units);
      }
    },

    tooltip: {
      init: function ($trigger, $slider) {
        PointsSlider.tooltip.content($slider);
        PointsSlider.tooltip.position($trigger);
        PointsSlider.tooltip.destroy($trigger);
        PointsSlider.tooltip.destroy($('.trigger-change, #slider-select-points'));
      },

      content: function ($slider) {
        var cont = '<span id="tooltip-slider" class="slider-tooltip invisible"><i class="icon-info"></i><span class="text">';
        cont += separateInLines($slider.data('tooltip'), 2);
        cont += '</span><span class="triangle"></span></span>';
        $slider.append(cont);
      },

      position: function ($trigger) {
        setTimeout(function() {

          var trigger = {
            width: $trigger.outerWidth(),
            height: $trigger.outerHeight(),
            left: $trigger.position().left,
            top: $trigger.position().top
          };
          var tooltip = {
            width: $('#tooltip-slider').outerWidth(),
            offset: {
              top: -parseInt($('#tooltip-slider .triangle').css('top'))
            },
            position: {}
          };
          tooltip.position.left = trigger.left - tooltip.width/2;
          tooltip.position.top = trigger.top + trigger.height + tooltip.offset.top;
          $('#tooltip-slider').css(tooltip.position).addClass('fadeIn');

        }, 1000);
        
      },

      destroy: function ($trigger) {
        $trigger.on('mousedown', function () {
          $('#tooltip-slider').fadeOut();
        });
      }

    }
  }

  PointsSlider.init('#slider-select-points');  

})();

(function() {
  $('a[data-method="show-modal"]').on('click', function () {
    $($(this).data('target')).addClass('show').show();
    $('body').addClass('modal-open');
  });
  $('a[data-method="hide-modal"]').on('click', function () {
    $($(this).data('target')).removeClass('hide').hide();
    $('body').removeClass('modal-open');
  });
})();


(function () {

  var hotelAside = {

    init: function (el, context) {
      var aside = $(el);
      var trigger = $(context).find('[data-trigger]');
      hotelAside.onClick(el, context, trigger);
    },

    onClick: function (el, context, trigger) {
      $(trigger).on('click', function () {
        $(trigger).removeClass('opened');
        $('article.hotel').removeClass('opened');
        $(this).addClass('opened');
        $(this).closest('article.hotel').addClass('opened');
        var hotel_id = $(this).data('trigger');
        // getJson( function (data) {
        //   hotelAside.putContent(el, data, hotel_id);
        //   hotelAside.setHeight(el);
        //   hotelAside.setContentHeight(el + ' .tab-content', el, [el + ' header', el + ' .nav-tabs']);
        //   $(window).resize(function () {
        //     $(el).removeData('initial-pos');
        //     hotelAside.setHeight(el);
        //     hotelAside.setContentHeight(el + ' .tab-content', el, [el + ' header', el + ' .nav-tabs']);
        //   });
        // }, 'assets/js/hotel-aside.json');
        hotelAside.setHeight(el);
        hotelAside.setContentHeight(el + ' .tab-content', el, [el + ' header', el + ' .nav-tabs']);
        $(window).resize(function () {
          $(el).removeData('initial-pos');
          hotelAside.setHeight(el);
          hotelAside.setContentHeight(el + ' .tab-content', el, [el + ' header', el + ' .nav-tabs']);
        });
        if(!$(context).hasClass('hotel-aside-opened')) {
          hotelAside.show(context);
        }
      });
      $(el).find('a.close').on('click', function () {
        hotelAside.hide(context, trigger);
      });
    },

    show: function (context) {
      $('body').addClass('hotel-aside-opened');
      $(context).addClass('hotel-aside-opened');
      $(context).removeClass('closed finished');
    },

    hide: function (context, trigger) {
      $('article.hotel').removeClass('opened');
      $(context).addClass('closing');
      $('body').removeClass('hotel-aside-opened');
      setTimeout(function() {
        setTimeout(function() {
          $(context).addClass('finished');
        }, 600);
          $(context).removeClass('hotel-aside-opened closing').addClass('closed');
      }, 300);
      
    },

    putContent: function (el, data, hotel) {
      var hotel = data[hotel];
      $(el + ' .logo').html('<img src="' + hotel.logo + '">');
      $(el + ' .h1').html(hotel.name);

      
      for(i = 0; i < hotel.description.keypoints.length; i++) {
        if (i == 0) {
          var keypoints = '';
        }
        keypoints += '<li>' + hotel.description.keypoints[i] + '</li>';
      }
      $(el + ' .description').html(keypoints);

      
      for(i = 0; i < hotel.description.more.length; i++) {
        if (i == 0) {
          var description_more = '';
        }
        '<p>' + hotel.description.more[i] + '</p>'
      }
      $(el + ' .description-more').html(description_more);

      for(i = 0; i < hotel.services.length; i++) {
        if (i == 0) {
          var services = '';
        }
        services += '<li>' + hotel.services[i] + '</li>';
      }
      $(el + ' .services').html(services);

      $(el + ' .rooms-header').html(hotel.roomsHeader);

      var rooms = '';
      for(i = 0; i < hotel.rooms.length; i++) {
        rooms += '<li>' + hotel.rooms[i] + '</li>';
      }
      $(el + ' .rooms').html(rooms);
    },

    setHeight: function (el) {
      var fixedEl = ['header', 'section.search'];
      var freeHeight = $(window).height() - 102;
      $(el).height(freeHeight - 60);
      hotelAside.setPosition(el, fixedEl);
      $(window).scroll(function () {
        hotelAside.setPosition(el, fixedEl);
      })
    },

    setPosition: function (el) {
      if ($('section.search').length) {
        var fixedEl = ['header', 'section.search'];
      } else if ($('section.reservation-search').length) {
        var fixedEl = ['header', 'section.reservation-search'];
      }
      var scroll_pos = {
        top: function () {
          return $(window).scrollTop();
        },
        bottom: function () {
          return $(window).scrollTop() + $(window).height();
        }
      };
      var top_pos = function () {
        for (i=0; i<fixedEl.length; i++) {
          if (i == 0) {
            var fixedEl_h = 0;
          }
          fixedEl_h += $(fixedEl[i]).outerHeight();
        }
        return $('#hotel-aside').offset().top - fixedEl_h - 20;
      };
      var bottom_max_pos = function () {
        return $('section.hotels-section').offset().top + $('section.hotels-section').outerHeight();
      }

      if (scroll_pos.bottom() < bottom_max_pos()) {
        $(el).removeClass('fixed-bottom')
        if (scroll_pos.top() >= top_pos()) {
          if (!$(el).data('initial-pos')) {
            $(el).data('initial-pos', top_pos());
          }
          $(el).addClass('fixed');
        }
        if(scroll_pos.top() < $(el).data('initial-pos')) {
          $(el).removeClass('fixed');
        }
      } else {
        $(el).removeClass('fixed').addClass('fixed-bottom');
      }
    },

    setContentHeight: function (target, content, elements) {
      var contentHeight = $(content).height();
      for(i=0; i<elements.length; i++) {
        if (i==0) {
          var elementsHeight = 0;
        }  
        elementsHeight += $(elements[i]).outerHeight();
      }
      var targetHeight = contentHeight - elementsHeight - ( $(target).outerHeight(true) - $(target).outerHeight() );
      $(target).height(targetHeight);
    }
  };
  
  hotelAside.init('#hotel-aside', '.hotels-section');

})();


// Reservation functions

(function () {

  // Fixed bars
  $.hook('addClass');
  $('header').bind('onafteraddClass', function () {
    if ($(this).hasClass('affix')) {
      $('.reservation-search, #hotels-selected').addClass('header-small');
    } else if ($(this).hasClass('affix-top')) {
      $('.reservation-search, #hotels-selected').removeClass('header-small');
    }
  });

  // Searcher interaction
  $('#searched, #new-search').click(function () {
    if($(this).hasClass('mice')) return false;
    $('main').removeClass('showing-searched').addClass('showing-search');
    if(!!$(this).data('hotel')) {
      $('#destination').val($(this).data('hotel'));
    }
    setTimeout(function() {
      $('.reservation-search').css('overflow', 'visible');
    }, 200);
  });
  $('#close-search').click(function () {
    $('.reservation-search').css('overflow', 'hidden');
    $('main').addClass('showing-searched').removeClass('showing-search');
  })

})();



(function () {

  $('a.mantain-position').click(function () {
    var scroll_pos = $(window).scrollTop();
    var url = $(this).attr('href');
    var strLink = url+'?y='+scroll_pos;
    $(this).attr('href', strLink);
  });

  var scroll_pos = getURLParameter('y');

  if(!!scroll_pos) {
    $(window).scrollTop(scroll_pos);
  }

})();


(function () {

  $('a[data-scroll-to]').on('click', function () {
    var fixedEl = ['header', 'section.search'];
    scrollToAnchor($(this).data('scroll-to'), fixedEl);
    return false;
  })

})();

// Checkout
var radioBtn;
(function () {

  var aside = {

    collapsedHeight: function (el) {
      return el.find('.h1').outerHeight() - 1;
    },

    init: function (el, trigger) {
      aside.collapse(el, trigger);
      aside.setup(el, trigger);
    },

    setup: function (el, trigger) {
      $(trigger).on('click', function () {
        if($(el).hasClass('closed')) {
          aside.expand(el, this);
        } else {
          aside.collapse(el, this);
        }
      });
    },

    collapse: function (el, trigger) {
      if (!el.hasClass('search-module-square')) el.css('max-height', aside.collapsedHeight(el));
      el
        .css('overflow', 'hidden')
        .addClass('closed')
        .removeClass('opened');

      $(trigger).children('i')
        .addClass('icon-add2')
        .removeClass('icon-close');
    },

    expand: function (el, trigger) {
      el
        .removeClass('closed')
        .addClass('opened')
        .delay(100).queue(function (next) {
          $(this).css('overflow', 'visible');
          next();
        });

      $(trigger).children('i')
        .addClass('icon-close')
        .removeClass('icon-add2');
    }

  };
  aside.init($('.checkout-aside'), $('#colapse-header'));
  aside.init($('.gestion-reserva'), $('.colapse-header'));
  aside.init($('.search-module-square'), $('.colapsed-header'));

  radioBtn = {
    init: function (context) {
      context.each(function () {
        radioBtn.setup(this);
        radioBtn.change(this);
      });
    },
    setup: function (context) {
      var activeProp = radioBtn.activeBtn(context, $(context).find('.btn.active'));
      radioBtn.setBackground(context, activeProp);  
    },
    activeBtn: function (context, element) {
      return activeProp = {
        position: element.position(),
        width: element.outerWidth(),
        which: function () {
          var element = $(context).find('label');
          var lenght = element.length;
          for (var i=0;i<lenght;i++) {
            if(element.eq(i).hasClass('active')) {
              switch(i) {
                case 0:
                  return 'first';
                  break;
                case lenght-1:
                  return 'last';
                  break;
              }
            }
          }
        }
      }
    },
    setBackground: function (context, activeProp) {
      var element = $(context).find('.background');
      element.css({
        left: function () {
          switch(activeProp.which()) {
            case 'first':
              return activeProp.position.left+3;
            case 'last':
              return activeProp.position.left;
          }
        },
        width: function () {
          switch(activeProp.which()) {
            case 'first':
              return activeProp.width-3;
            case 'last':
              return activeProp.width-4;
          }
        }
      });
    },
    change: function (context) {
      $(context).find('input[type=radio]').change(function () {
        radioBtn.setup(context);
      });
    }
  };
  radioBtn.init($('.btn-group.custom'));
  $(window).resize(function () {
    radioBtn.init($('.btn-group.custom'));
  });

  $('#user-comments-enabler').on('change', function () {
    if(this.checked) {
      $('#user-comments-cont').slideDown();
    } else {
      $('#user-comments-cont').slideUp();
    }
  });

  $('#billing-data-enabler').on('change', function () {
    if(this.checked) {
      $('#billing-data').slideDown();
    } else {
      $('#billing-data').slideUp();
    }
  });

  $('#login-form-trigger').on('click', function () {
    $('#cta-login').slideUp();
    $('.login-form').slideDown();
  })
  $('#close-login-form').on('click', function () {
    $('#cta-login').slideDown();
    $('.login-form').slideUp();
  })

})();

// credit card

(function () {
  if(!!$('#cc-number').length) {
    $('#cc-number').payment('formatCardNumber');
    $('#cc-cvv').payment('formatCardCVC');

    // check card type
    var type = $('#cc-type');
    var type_g = $('#cc-type').closest('.form-group');
    var number = $('#cc-number');
    var number_g = $('#cc-number').closest('.form-group');

    var CCValidation = {
      init: function () {
        number.on('keyup', CCValidation.keyup);
        type.on('change', function () {
          CCValidation.valType();
          if(CCValidation.valType()) {
            CCValidation.valNumber();
          }
        });
      },

      keyup: function () {
        if(number.val().length > 3 && number.val().length < 19) {
          CCValidation.valType();
        } else if(number.val().length === 19) {
          CCValidation.valNumber();
        } else {
          CCValidation.valRemove();
        }
      },

      valRemove: function () {
        type_g.removeClass('has-success has-error');
        number_g.removeClass('has-success has-error');
      },

      valType: function () {
        CCValidation.valRemove();
        if(number.hasClass('unknown')) {
          type_g.addClass('has-error');
          number_g.addClass('has-error');
        } else if (number.hasClass('identified')) {
            if (number.hasClass('visa') && type.val() === 'visa') {
            type_g.addClass('has-success');
            return true;
          } else if (number.hasClass('mastercard') && type.val() === 'mastercard') {
            type_g.addClass('has-success');
            return true;
          } else if (number.hasClass('amex') && type.val() === 'amex') {
            type_g.addClass('has-success');
            return true;
          } else if (number.hasClass('jcb') && type.val() === 'jcb') {
            type_g.addClass('has-success');
            return true;
          } else if (number.hasClass('dinersclub') && type.val() === 'dinersclub') {
            type_g.addClass('has-success');
            return true;
          } else {
            type_g.addClass('has-error');
          }
        }
      },

      valNumber: function () {
        if($.payment.validateCardNumber( number.val() )) {
          number_g.addClass('has-success');
        } else {
          number_g.addClass('has-error');
        }
      }
    }
    CCValidation.init();  
  }  

})();


$('#mobile-fixed-map .close').on('click', function () {
  $('#mobile-fixed-map').removeClass('show');
});

$('#hotel-aside .nav-tabs a').on('click', function () {
  if(!$(this).parent().hasClass('active')) {
    $('.tab-content').delay(150).queue(function (next) {
      $(this).scrollTop(0);
      next();
    });
  }
})
$('.rewardsNav li a').click(function(){
  $(this).parents('.input-group-btn').find('.textlabel').text($(this).text());
  $(this).parents('.input-group-btn').find('.textlabel').val($(this).data('value'));
});

$('.rewardsCategory li a').click(function(){
  $(this).parents('.input-group-btn').find('.textlabel').text($(this).text());
  $(this).parents('.input-group-btn').find('.textlabel').val($(this).data('value'));
});

(function(){
    $(document).on("click", ".view-more-toggle", function(event){
        event.preventDefault();
        var target = $(this).data("target");
        var link = this;
        $(target).slideToggle("slow", function() {
            if (!$(link).hasClass("view-more-toggle-no-icon")) {
                var $icon = $(link).find("i");
                var newClass = $icon.hasClass("icon-arrow-down") ? "icon-arrow-up" : "icon-arrow-down";
                $icon.attr("class", newClass);
            }
        });
    });


     $(document)
     .on('click', ".row-aspro a.row", function(event){
        event.preventDefault();
        $(this).next().slideToggle('slow');
        $(this).closest('.row-aspro').toggleClass('active');
    })
     .on('click', ".row-faqs a.row", function(event){
        event.preventDefault();
        $(this).next().slideToggle('slow');
        $(this).closest('.row-faqs').toggleClass('active');
    });

    var setSameHeight = function(parents, childsSelector) {
        $parents = typeof(parents) == "string" ? $(parents) : parents;

        $parents.each(function(){
            var $divs = $(this).find(childsSelector);
            var heights = $divs.map(function(i, e){
                return $(e).height();
            }).get();
            var maxHeight = Math.max.apply(null, heights) || 0;

            if (maxHeight > 0) {
                $divs.height(maxHeight+"px");
            }
        });
    };

    var $responsiveTest = $("<div />", {'class': 'visible-xs visible-sm'}).appendTo($("body"));

    var setColsMdSameHeight = function() {
        $('.same-height-cols-md>div').css('height', 'auto');
        if (!$responsiveTest.is(":visible")) {
            setSameHeight('.same-height-cols-md', '>div');
        }
    };

    $(window).resize(setColsMdSameHeight)
    setColsMdSameHeight();
})();

// Interacción página problemas detalle
(function(){
  $('.btn-responder-reclamacion').on('click', function () {
    $(this).css("display","none");
    $('.form-enviar-reclamacion').fadeIn("fast");
  });
})();
// Fin

(function(){
  // Interacción página - rewards checkin paso 2
  $('.rw-checkin-p2-servicio-extra .tomar-servicio').on('click', function (event) {
    event.preventDefault();  
    $(this).closest('.rw-checkin-p2-servicio-extra').find(".anadir-servicio").slideToggle('slow');
    $(this).closest('.rw-checkin-p2-servicio-extra').addClass('active');
  });
  $('.rw-checkin-p2-servicio-extra .icon-close').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.rw-checkin-p2-servicio-extra').find(".anadir-servicio").fadeOut('slow', function() {
      $(this).closest('.rw-checkin-p2-servicio-extra').removeClass('active');
    });
  });
  $('.rw-checkin-p2-servicio-extra .btn-anadir').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.rw-checkin-p2-servicio-extra').find(".anadir-servicio").fadeOut('slow', function() {
      $(this).closest('.rw-checkin-p2-servicio-extra').removeClass('active');
      $(this).closest('.rw-checkin-p2-servicio-extra').addClass('servicio-tomado');
    });
  });
  $('.rw-checkin-p2-servicio-extra .modificar-servicio').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.rw-checkin-p2-servicio-extra').addClass('active');
    $(this).closest('.rw-checkin-p2-servicio-extra').find(".anadir-servicio").fadeIn('slow');
  });
  $('.rw-checkin-p2-servicio-extra .eliminar-servicio').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.rw-checkin-p2-servicio-extra').removeClass('servicio-tomado');
    $(this).closest('.rw-checkin-p2-servicio-extra').find('select').val('1');
  });
  // Fin
  // Interacción página - rewards checkin paso 3
  $('.rw-checkin-editar-huesped .editar-huesped').on('click',function () {
    $(this).parent().removeClass('rw-checkin-editar-huesped');
  });
  $('.rw-checkin-huesped .btn-cancelar').on('click',function () {
    $(this).closest('.rw-checkin-huesped').addClass('rw-checkin-editar-huesped');
  });  
  $('.rw-checkin-huesped .indicar-vuelo').on('click',function (event) {
      event.preventDefault();
      $(this).closest('.rw-checkin-huesped ').find('.numero-de-vuelo').slideToggle('slow');      
  });
  $('.checkbox-confirm').on('click',function () {
    if(this.checked) {
      $(".btn-finalizar-checkin").prop( "disabled", false );
    }else{
      $(".btn-finalizar-checkin").prop( "disabled", true );
    }
  });  
  // Fin
  // Interacción página - rewards checkin paso 3 huespedes 
  $('.recuperar-datos-cliente').on('click',function(){
    $('.rw-checkin-form-acompanante').slideDown();
    $(this).slideUp();
  });
  $('.rw-checkin-form-acompanante .icon-close').on('click', function () {
    $('.rw-checkin-form-acompanante').slideUp();
    $('.recuperar-datos-cliente').slideDown();
  });
})();

// Interacción página rewards modificar reserva
(function(){
  $('.rw-reserva-modificar-reserva .editar-datos, .rw-reserva-modificar-reserva .agregar-datos').on('click', function () {
    var $parent = $(this).closest('.rw-reserva-modificar-reserva');

    $(this).css("display","none");

    $parent.find(".datos-reserva").slideUp();
    $parent.find($(this).data("form") || "form").slideDown();
    $parent.find(".alerta-modificar").slideDown();

    radioBtn.init($parent.find('.btn-group.custom'));
  });

  $('.rw-reserva-modificar-reserva .btn-cancelar').on('click btn-cancelar.toggle', function () {
    var $parent = $(this).closest('.rw-reserva-modificar-reserva');
    $parent.find(".datos-reserva").slideDown();
    $parent.find(".alerta-modificar").slideUp();
    $parent.find("form").slideUp('slow', function() {
        $parent.find(".editar-datos").css("display","block");
        $parent.find(".agregar-datos").css("display","block");
    });
  });

})();
// Fin

(function(){

    var containerId = 'autocomplete-hotels-container';

    $('<div />', {'id': containerId}).appendTo($('body'));

    var $inputs = $(".autocomplete-hotels");

    if ($inputs.length == 0) {
        return;
    }

    $inputs.autocomplete({
        source: sourceHoteles,
        minLength: 2,
        appendTo: '#'+containerId,
        open: function( event, ui ) {
            // sugerencia-hoteles
            $(event.target).next().hide("slow");
        }
    });

    $inputs.on('focus', function(){
        // sugerencia-hoteles
        if (!$(this).val()) {
            $(this).next().show('slow');
        }
    });

    $('.autocomplete-hotels-wrapper .sugerencia-hoteles li').click(function(){
        var $wrapper = $(this).closest('.sugerencia-hoteles');
        $wrapper.prev().val($(this).data('value'));
        $wrapper.hide("slow");
    });

})();


$('.search-module-square').on('click', function () {
  $(this).removeClass('nofocus');
  $('.gestion-reserva').addClass('nofocus');
});
$('.gestion-reserva').on('click', function () {
  if(!$(this).hasClass('contact')) {
    $(this).removeClass('nofocus');
    $('.search-module-square').addClass('nofocus');
  }
});

$('.simple-form-validation').trigger('simpleValidation.validate');

$('.simple-form-validation[data-points]').submit(function(event){
    event.preventDefault();
    var $form = $(this);

    var $div = $('<div class="msg-points"><span>+'+$(this).data('points')+'</span> puntos</div>').slideUp();
    $div.insertAfter($form);

    $form.slideUp('slow');
    $div.slideDown('slow', function(){
        window.setTimeout(function(){
            $form.closest('article').find('.btn-cancelar').trigger('btn-cancelar.toggle');
            $div.slideUp('slow', function(){
                $div.remove();
            });
        }, 2000);
    });


(function(){

  $('#login-form').on('submit', function () {

      if ( $('#login-email').val() === '' || $('#login-password').val() === '' ) {
        $(this).find('.form-group').addClass('has-error');
        // error_control($('#login-form .has-error'));
        return false;
      }
  });

})();

$(document).on('click', '', function () { });
});

/*Class for module other country*/

(function () {

  $('.codigo-promocional-pais').click( function() {
    $('nav li:nth-child(4) a').toggleClass('active');
  });

})();

(function () {

  $(document).click( function(e) {
    if( $(e.target).closest('.search-module-square').length ) {
      $('main').addClass('focus-search').removeClass('focus-reservation');
    } else if( $(e.target).closest('.gestion-reserva').length ) {
      $('main').removeClass('focus-search').addClass('focus-reservation');
    } else {
      $('main').removeClass('focus-reservation focus-reservation');
    };
  });

})();

// Graphics on melia priv points
$(function () {

if(!!$('.chart-cont').length) {
  Chart.types.Doughnut.extend({
    name: "DoughnutAlt",
    initialize: function (data) {
      Chart.types.Doughnut.prototype.initialize.apply(this, arguments);

      // save the actual clear method
      var originalClear = this.clear;
      // override the clear method to draw the background after each clear
      this.clear = function () {
        originalClear.apply(this, arguments);

        var ctx = this.chart.ctx;
        // use any one of the segments to get the inner and outer radius and center x and y
        var firstSegment = this.segments[0];
        // adjust 0.3 to increaase / decrease the width of the background
        var gap = (firstSegment.outerRadius - firstSegment.innerRadius) * (1 - 0.6) / 2;

        ctx.save();
        ctx.fillStyle = "#EEE";
        ctx.beginPath();
        ctx.arc(firstSegment.x, firstSegment.y, firstSegment.outerRadius - gap, 0, 2 * Math.PI);
        ctx.arc(firstSegment.x, firstSegment.y, firstSegment.innerRadius + gap, 0, 2 * Math.PI, true);
        ctx.closePath();
        ctx.fill();
        ctx.restore();
      };
    }
  });
};

  if (!!$('#basic-points').length &&
      !!$('#promo-points').length &&
      !!$('#used-points').length &&
      !!$('#basic-points').length ) {
    var points = {
      earned: {
        basic: {
          value: parseInt($('#basic-points').html().replace(/\./g, '')),
          color: $('#basic-points').css('color')
        },
        promo: {
          value: parseInt($('#promo-points').html().replace(/\./g, '')),
          color: $('#promo-points').css('color')
        }
      },
      used: {
        value: parseInt($('#used-points').html().replace(/\./g, '')),
        color: $('#used-points').css('color')
      },
      blocked: {
        value: parseInt($('#blocked-points').html().replace(/\./g, '')),
        color: $('#blocked-points').css('color')
      }
    }
    var pointsTotal = [
        {
            value: points.earned.basic.value,
            color: points.earned.basic.color
        },
        {
            value: points.earned.promo.value,
            color: points.earned.promo.color
        }
    ];
    var pointsUsed = [
      {
        value: points.used.value,
        color: points.used.color
      },
      {
        value: points.earned.basic.value + points.earned.promo.value - points.used.value,
        color: function () {
          if (isIE(8, 'lte')) {
            return '#EEEEEE';
          } else {
            return 'transparent';
          }
        }()
      }
    ];
    var pointsBlocked = [
      {
          value: points.blocked.value,
          color: points.blocked.color
      },
      {
          value: points.earned.basic.value + points.earned.promo.value - points.blocked.value,
          color: function () {
          if (isIE(8, 'lte')) {
            return '#EEEEEE';
          } else {
            return 'transparent';
          }
        }()
      }
    ];
  }
  if (!!$('#pointsProfile').length) {
    var pointsProfile = [
      {
        value: $('#pointsProfile').data('points-earned'),
        color: '#0071C6'
      },
      {
        value: $('#pointsProfile').data('points-total') - $('#pointsProfile').data('points-earned'),
        color: function () {
          if (isIE(8, 'lte')) {
            return '#EEEEEE';
          } else {
            return 'transparent';
          }
        }()
      }
    ];
  }

  if (!!document.getElementById("pointsTotal")) {
    var pointsTotal_ctx = document.getElementById("pointsTotal").getContext("2d");
    var pointsTotal = new Chart(pointsTotal_ctx).DoughnutAlt(pointsTotal, {
    segmentShowStroke: false,
    segmentStrokeWidth : 0,
    percentageInnerCutout: 90,
    showTooltips: false,
    animationEasing: 'easeInOutCubic',
    responsive: true,
    animation: Modernizr.canvas
    });
    $('.chart-cont').height($('#pointsTotal').height());
    $(window).resize(function () {
      $('.chart-cont').height($('#pointsTotal').height());
    });
  }
  if (!!document.getElementById("pointsUsed")) {
    var pointsUsed_ctx = document.getElementById("pointsUsed").getContext("2d");
    var pointsUsed = new Chart(pointsUsed_ctx).DoughnutAlt(pointsUsed, {
      segmentShowStroke: false,
      segmentStrokeWidth : 0,
      percentageInnerCutout: 88,
      showTooltips: false,
      animationEasing: 'easeInOutCubic',
      responsive: true,
      animation: Modernizr.canvas
    });
  }
  if (!!document.getElementById("pointsBlocked")) {
    var pointsBlocked_ctx = document.getElementById("pointsBlocked").getContext("2d");
    var pointsBlocked = new Chart(pointsBlocked_ctx).DoughnutAlt(pointsBlocked, {
      segmentShowStroke: false,
      segmentStrokeWidth : 0,
      percentageInnerCutout: 86,
      showTooltips: false,
      animationEasing: 'easeInOutCubic',
      responsive: true,
      animation: Modernizr.canvas
    });
  }
  if (!!document.getElementById("pointsProfile")) {
    var pointsProfile_ctx = document.getElementById("pointsProfile").getContext("2d");
    var pointsProfile = new Chart(pointsProfile_ctx).DoughnutAlt(pointsProfile, {
      segmentShowStroke: false,
      segmentStrokeWidth : 0,
      percentageInnerCutout: 86,
      showTooltips: false,
      animationEasing: 'easeInOutCubic',
      responsive: true,
      animation: Modernizr.canvas
    });
  }

});

(function(){
    // .total-nights-content

    $(document).on("click", ".total-nights-content .btn-less, .total-nights-content .btn-more", function(event){
        event.preventDefault();
        var $input = $(this).closest(".total-nights-content").find("input");
        var val = parseInt($input.val());
        val = $(this).hasClass("btn-less") ? val-1 : val+1;
        if (val > 0) {
            $input.val(val);
        }
    });

    $(document).on("click", ".total-nights-content .btn", function(event){
        event.preventDefault();
        $(".total-nights span").text($(".total-nights-content input").val());
        $(".total-nights").popover("hide");
    });

    //Eliminar popup si se hace click fuera del popup
    $('.total-nights').click(function(event) {
      $('html').one('click',function() {
        $('.popover').hide();
      });
      event.stopPropagation();
    });


})();

// ASPRO INTERACTION
(function () {

  var pointsSummatory = {
    init: function (rows) {
      $(rows).each(function () {
        pointsSummatory.setup(this);
      });
    },
    setup: function (row) {
      $(row).find('select').on('change', function () {
        pointsSummatory.onchange(row);
      });
    },
    onchange: function (row) {
      var sum = pointsSummatory.sum(row);
      pointsSummatory.render(sum, row);
    },
    sum: function (row) {
      var c = 0;
      $(row).find('select').each(function () {
        $('[data-trigger="select-tickets"]').removeClass('disabled');
        var tickets = Number($(this).val());
        var points = Number($(this).closest('.person-section').find('.ticket-price').text().replace(/[.|,]/g, ''));
        c +=  tickets * points;
      });
      return  millarSeparator(c);
    },
    render: function (sum, row) {
      $(row).find('.sum-selected').html(sum);
    }
  }

  pointsSummatory.init('.row-aspro');


  var asproCart = {
    init: function (trigger) {
      $(trigger).each(function () {
        asproCart.setup(this);
      });
    },

    setup: function (trigger) {
      var model = asproCart.getModel();
      $(trigger).click(function () {
        var context = $(this).closest('article');
        var tickets = asproCart.getData(context);
        var text = asproCart.replaceData(model, tickets);
        asproCart.putItemCart(text, tickets['ID']);
        asproCart.changeModule(tickets, context);
        asproCart.countSelected();
      });
      $(document).on('click', '#selected-tickets .tickets a.delete-tickets', function () {
        asproCart.resetItem.call(this);
        asproCart.deleteItem.call(this);
        asproCart.countSelected();
      }); 
      $('.col-puntos .total a.modify-tickets').on('click', function () {
        asproCart.modifySelection.call(this);
        asproCart.countSelected();
      }); 
    },

    getData: function (context) {
      var tickets = {
        'ID': context.attr('id'),
        'place': context.find('[data-cart="title"]').text(),
        'ticket-price-child': function () {
          var price = context.find('[data-ticket-type="child"] .ticket-price').text()
          var price = price.replace(/[.|,]/g, '');
          return Number(price);
        }(),
        'children-tickets': context.find('[name="children"]').val(),
        'ticket-price-adult': function () {
          var price = context.find('[data-ticket-type="adult"] .ticket-price').text()
          var price = price.replace(/[.|,]/g, '');
          return Number(price);
        }(),
        'adults-tickets': context.find('[name="adults"]').val(),
        'ticket-price-senior': function () {
          var price = context.find('[data-ticket-type="senior"] .ticket-price').text()
          var price = price.replace(/[.|,]/g, '');
          return Number(price);
        }(),
        'seniors-tickets': context.find('[name="seniors"]').val(),
      };
      return tickets;
    },

    getModel: function () {
      return $('#selected-tickets .model').html();
    },

    replaceData: function (model, tickets) {
      var keys = Object.keys(tickets);
      for(var i = 0; i < keys.length; i++) {
        keys[i] = '{{' + keys[i] + '}}';
      }
      var re = new RegExp(keys.join('|'),"gi");
      return model.replace(re, function (matched) {
        matched = matched.replace(/[{,}]/g, '');
        return tickets[matched];
      });
    },

    putItemCart: function (text, ticketsID) {
      $('#selected-tickets #selected-' + ticketsID).remove();
      $('#selected-tickets').append('<li class="tickets" id="selected-' + ticketsID + '">' + text + '</li>');

      //Values equivalentes a 0, clase 'not-showed'
      var valSpan = $('#selected-tickets #selected-' + ticketsID + '.tickets .children span').text();
      spanContainer = $('#selected-tickets #selected-' + ticketsID + '.tickets .children');
      if (valSpan === '0') { spanContainer.addClass('not-showed');}

      var valSpan = $('#selected-tickets #selected-' + ticketsID + '.tickets .adults span').text();
      spanContainer = $('#selected-tickets #selected-' + ticketsID + '.tickets .adults');
      if (valSpan === '0') { spanContainer.addClass('not-showed');}
     
     var valSpan = $('#selected-tickets #selected-' + ticketsID + '.tickets .seniors span').text();
     spanContainer = $('#selected-tickets #selected-' + ticketsID + '.tickets .seniors');
     if (valSpan === '0') { spanContainer.addClass('not-showed');}

      $('#' + ticketsID + ' .ticket-options' ).addClass('not-showed');
      $('#' + ticketsID + ' .choosen-tickets' ).removeClass('not-showed');
      $('#' + ticketsID + ' .select-tickets').addClass('not-showed');
      $('#' + ticketsID + ' .modify-tickets').removeClass('not-showed');
  
    },

    countSelected: function () {
      var c = 0;
      $('#selected-tickets .tickets .children span, #selected-tickets .tickets .adults span, #selected-tickets .tickets .seniors span').each(function () {
        c += Number($(this).text());
      });
      $('.tickets-quantity').html(c);

       //Button
      if (c > 0) {
        $('#buy-tickets').removeClass('hidden');
        $('#total-points').removeClass('hidden');
       } else {
        $('#buy-tickets').addClass('hidden');
        $('#total-points').addClass('hidden');
       }

      //Total de puntos de tiquetes

      var sum = 0;
      $('.row-aspro .sum-selected').each(function() {
        sum += Number($(this).text().replace(/[\,,\.]/g, ''));
      });
      $("#total-points .sum-selected").text(millarSeparator(sum));

    },

    deleteItem: function () {
      $(this).closest('.tickets').remove();
    },

    modifySelection: function (){
      $('.select-tickets').removeClass('not-showed');
      $('.ticket-options' ).removeClass('not-showed');
      $('.choosen-tickets' ).addClass('not-showed');
      $('.modify-tickets').addClass('not-showed');
      $('.type-person-options').removeClass('not-showed');
    },

    resetItem: function () {
      var selectedId = $(this).closest('.tickets').attr('id');
      var id = selectedId.replace('selected-', ''); // a modificar en implementación
      //console.log(selectedId);
      var selects = $('#' + id + ' select');
      $(selects).val(0);
      pointsSummatory.onchange($('#' + id));

      $('#' + id + ' .ticket-options').removeClass('not-showed');
      $('#' + id + ' .choosen-tickets').addClass('not-showed');
      //Cambios boton
      $('#' + id + ' .select-tickets').removeClass('not-showed').addClass('disabled');
      $('#' + id + ' .modify-tickets').addClass('not-showed');
      $('#' + id + ' .type-person-options').removeClass('not-showed');
    },

    changeModule:function (tickets, context) {
      context.find('.choosen-tickets ul .choosen-children span').html(tickets['children-tickets']);
      context.find('.choosen-tickets ul .choosen-adults span').html(tickets['adults-tickets']);
      context.find('.choosen-tickets ul .choosen-seniors span').html(tickets['seniors-tickets']);
      context.find('.choosen-tickets ul li').each(function () {
      if($(this).find('span').html() !== '0') {
        $(this).closest('.choosen-tickets').removeClass('not-showed');
        $(this).closest('article').find('.type-person-options').addClass('not-showed');
        $(this).removeClass('not-showed');
      } else {
        $(this).addClass('not-showed');
      }

      });
    },
    
  };
  
  asproCart.init($('[data-trigger="select-tickets"]'));
})();


$('.dropdown.currency a, .dropdown.orderby a').on('click', function () {
  var val = $(this).text();
  $(this).closest('.dropdown').find('button').text(val);
})



/* ==========================================================================
   Grid column adjustment
   ========================================================================== */
function getColumns(module) {
  var cols = [[]],
      colWidth = 0,
      parentWidth = 0;

  $(module).each(function () {
    var $this = $(this),
        $parent = $parent || $this.parents('.destination-grid'),
        outerWidth = $this.outerWidth(),
        parentWidth = $parent.outerWidth();

    colWidth += outerWidth-1;

    if (colWidth <= parentWidth) {
      cols.slice(-1)[0].push($this);
    } else {
      cols.push([]);
      colWidth = outerWidth-1;
      cols.slice(-1)[0].push($this);
    }
    
  });

  return cols;
}

function getColumnsMaxHeight(arr) {
  var maxHeight = 0, column, height;

  $.each(arr, function (i, val) {
    height = val.outerHeight();
    maxHeight = height > maxHeight ? height : maxHeight;
  });

  return maxHeight;
}

function adjustColumns() {
  var columns = getColumns('.destination-grid [class^="col-"]'),
      height = 0;

  $.each(columns, function (i, column) {
    height = getColumnsMaxHeight(column);
    $.each(column, function (i, module) {
      var $this = $(this),
          $footer = $this.find('.footer'),
          $title = $this.find('.title'),
          $img = $this.find('.img-container');

      $footer.css('margin-top', '');
      $title.css('margin-bottom', '');
      $img.css({'box-sizing': '', 'padding-bottom': ''});

      var margin = height-$this.outerHeight();

      if (!$this.hasClass('col-md-8') && margin > 0) {
        margin = height-$this.outerHeight();

        $this.find('.footer').css({
          'margin-top': Math.floor(margin/2)+'px'
        });

        $this.find('.title').css('margin-bottom', Math.ceil(margin/2)+'px')
      } else if (margin > 0) {
        $this.find('.img-container').css({
          'box-sizing': 'content-box',
          'padding-bottom': margin
        });
      }
    });
  });
}



/* ==========================================================================
   Input error
   ========================================================================== */
$.subscribe('input-error', function (ev) {
  $('.has-error').each(function () {
    var $this     = $(this),
        $input    = $this.find('input, textarea'),
        message   = $this.data('error-msg'),
        $message  = $('<div class="bg-danger"><span>'+message+'</span><i></i></div>');

    $message.find('i').css({  'top': '-8px',
                              'left': '10px',
                              'width': '0',
                              'height': '0' });

    $input.on('input', function () {
      $this.removeClass('has-error');
      $message.remove();
    });

    $this.append($message);
  });
});



$(document).ready(adjustColumns);
var timeout;
$(window).on('resize', function () {
  if (timeout) clearTimeout(timeout);
  setTimeout(adjustColumns, 300);
});