/**
 * responsive-more.js
 * Author & copyright (c) 2015: Jordi Martí
 * http://jordimarti.es
 *
 * Dual MIT & GPL license
 *
 * Project Page: 
 *
 * The jQuery plugin for collapse paragraphs and lists through 'more' button.
 *
 * REQUIRES:
    - Modernizr.js Plugin. For enable media options. You can download on: https://github.com/Modernizr/Modernizr
 *
 */

 ;(function($) {

  $.fn.responsiveMore = function ( options ) {

    // Console bad options values
    var errorShow = function ( code, value ) {
      var error = 'Responsive-more.js says => Error #' + code + ': ';
      if (code === 1) error += 'Can\'t recognise mediaQueries string option value: \'' + value + '\'. ';
      if (code === 2) error += 'Can\'t recognise mediaQueries array option element: \''+ value +'\'. All array items must be integers. ';
      if (code === 3) error += 'mediaQueries array lenght must be from 1 to 4. ';
      if (code === 4) error += 'initialState option value must be \'collapsed\' OR \'expanded\'. ';
      error += 'Visit URL for further information.'
      console.log(error);
    }


    if (this.length !== 0) {

      this.each(function() {
        var container = $(this);
        var content = $(this).children('.more-content');
        var itemsCount = $(content).children().length;

        // Modernizr is required for responsive capabilites
        var responsive = typeof Modernizr !== 'undefined';

        // Top position of content children elements
        var itemPosition = function ( i ) {

          var parentPos = content.position().top;
          var itemsPos = [];

          content.children().each(function() {
            itemsPos[itemsPos.length] = $(this).position().top - parentPos;
          });

          return itemsPos[i];
        }


        // Showing media query in HTML tag
        var mediaQueries = {

          init: function (breakpoints) {
            var value = settings.mediaQueries;
            var breakpoints = mediaQueries.know(value);
            var queries = mediaQueries.define(breakpoints);
            mediaQueries.set(queries, breakpoints);
          },

          know: function (value) {

            if (typeof value === 'string') {
              if (value === 'bootstrap') {
                var breakpoints = { sm: 768, md: 992, lg: 1200 };
              }
              else if (value === 'foundation') {
                var breakpoints = { sm: 641, md: 1025, lg: 1441, xlg: 1921 };
              }
              else {
                errorShow(1);
                return false;
              }
            }

            else if (typeof value === 'number') {
              var breakpoints = { md: value };
            }

            else if (Array.isArray(value)) {
              var length = value.length;
              for (var i = 0; i < length; i++) { // validate if all items are integer
                if ( typeof value[i] !== 'number') {
                  errorShow(2, value[i]);
                  return false;
                }
              }
              if (length <= 2) {
                var md_pos = 0;
              } else if (length <= 4) {
                var md_pos = 1;
              } else {
                errorShow(3);
                return false;
              }
                var breakpoints = { md: value[md_pos] };
                if (length > 1) breakpoints.lg = value[md_pos+1];
                if (length > 2) breakpoints.sm = value[md_pos-1];
                if (length > 3) breakpoints.xlg = value[md_pos+2];
            }

            return breakpoints;
          },

          define: function (breakpoints) {
            var queries = {
              sm: '(max-width:' + (breakpoints.md-1) + 'px)',
              md: '(min-width:' + breakpoints.md + 'px)'
            }
            if (breakpoints.lg) {
              queries.md = '(min-width:' + breakpoints.md + 'px) and (max-width:' + (breakpoints.lg-1) + 'px)';
              queries.lg = '(min-width:' + breakpoints.lg + 'px)';
            }
            if (breakpoints.sm) {
              queries.xs = '(max-width:' + (breakpoints.sm-1) + 'px)';
              queries.sm = '(min-width:' + (breakpoints.sm) + 'px) and (max-width:' + (breakpoints.md-1) + 'px)';
            }
            if (breakpoints.xlg) {
              queries.lg = '(min-width:' + breakpoints.lg + 'px) and (max-width:' + (breakpoints.xlg-1) + 'px)';
              queries.xlg = '(min-width:' + breakpoints.xlg + 'px)';
            }
            return queries;
          },

          set: function (queries) {
            if ( queries.xs && Modernizr.mq(queries.xs) && !$('html').hasClass('media-xs')) {
              $('html')
                .removeClass('media-sm media-md media-lg media-xlg')
                .addClass('media-xs');
            }

            else if ( queries.sm && Modernizr.mq(queries.sm) && !$('html').hasClass('media-sm')) {
              $('html')
                .removeClass('media-xs media-md media-lg media-xlg')
                .addClass('media-sm');
            }

            else if ( queries.md && Modernizr.mq(queries.md) && !$('html').hasClass('media-md') ) {
              $('html')
                .removeClass('media-xs media-sm media-lg media-xlg')
                .addClass('media-md');
            }

            else if ( queries.lg && Modernizr.mq(queries.lg) && !$('html').hasClass('media-lg') ) {
              $('html')
                .removeClass('media-xs media-sm media-md media-xlg')
                .addClass('media-lg');
            }

            else if ( queries.lg && Modernizr.mq(queries.xlg) && !$('html').hasClass('media-xlg') ) {
              $('html')
                .removeClass('media-xs media-sm media-md media-lg')
                .addClass('media-lg');
            }
          }
        };

        // Default options
        $.fn.responsiveMore.defaults = {
          initialState: 'collapsed',
          moreText: 'view more [+]',
          moreIconClass: '',
          toggle: true,
          lessText: 'view less [-]',
          lessIconClass: '',
          transition: true,
          duration: 300,
          triggerClass: 'more-trigger',
          mediaQueries: '',
          itemsShowed: function () {
            if( responsive === true ) {
              var items = {
                xs: Math.floor(itemsCount / 2),
                sm: Math.floor(itemsCount / 2),
                md: Math.floor(itemsCount / 2),
                lg: Math.floor(itemsCount / 2),
                xlg: Math.floor(itemsCount / 2)
              }
              return items;
            } else {
              return Math.floor(itemsCount / 2);
            }
          }(),
          showedLines: 0,
        };

        var defaults = $.fn.responsiveMore.defaults;
        var settings = $.extend({}, defaults, options);

        // Overwrite options defined by data-attributes
        var dataAttr = container.data();
        // Inheritance of non-defined values of itemsShowed
        if (typeof settings.itemsShowed === 'object') {
          settings.itemsShowed.xs = settings.itemsShowed.xs ? settings.itemsShowed.xs : $.fn.responsiveMore.defaults.itemsShowed.xs;
          settings.itemsShowed.sm = settings.itemsShowed.sm ? settings.itemsShowed.sm : settings.itemsShowed.xs;
          settings.itemsShowed.md = settings.itemsShowed.md ? settings.itemsShowed.md : settings.itemsShowed.sm;
          settings.itemsShowed.lg = settings.itemsShowed.lg ? settings.itemsShowed.lg : settings.itemsShowed.md;
          settings.itemsShowed.xlg = settings.itemsShowed.xlg ? settings.itemsShowed.xlg : settings.itemsShowed.lg;
        }
        if (typeof settings.itemsShowed === 'number') {
          settings.itemsShowed.xs = settings.itemsShowed;
          settings.itemsShowed.sm = settings.itemsShowed;
          settings.itemsShowed.md = settings.itemsShowed;
          settings.itemsShowed.lg = settings.itemsShowed;
          settings.itemsShowed.xlg = settings.itemsShowed;
        }
        if (typeof settings.itemsShowed === 'undefined') {
          settings.itemsShowed = $.fn.responsiveMore.defaults.itemsShowed;
        }
        if (dataAttr.itemsShowed) {
          if (typeof dataAttr.itemsShowed === 'object') {
            dataAttr.itemsShowed.xs = dataAttr.itemsShowed.xs ? dataAttr.itemsShowed.xs : settings.itemsShowed.xs;
            dataAttr.itemsShowed.sm = dataAttr.itemsShowed.sm ? dataAttr.itemsShowed.sm : dataAttr.itemsShowed.xs;
            dataAttr.itemsShowed.md = dataAttr.itemsShowed.md ? dataAttr.itemsShowed.md : dataAttr.itemsShowed.sm;
            dataAttr.itemsShowed.lg = dataAttr.itemsShowed.lg ? dataAttr.itemsShowed.lg : dataAttr.itemsShowed.md;
            dataAttr.itemsShowed.xlg = dataAttr.itemsShowed.xlg ? dataAttr.itemsShowed.xlg : dataAttr.itemsShowed.lg;
          }
          if (typeof dataAttr.itemsShowed === 'number') {
            dataAttr.itemsShowed.xs = dataAttr.itemsShowed;
            dataAttr.itemsShowed.sm = dataAttr.itemsShowed;
            dataAttr.itemsShowed.md = dataAttr.itemsShowed;
            dataAttr.itemsShowed.lg = dataAttr.itemsShowed;
            dataAttr.itemsShowed.xlg = dataAttr.itemsShowed;
          }
          settings.itemsShowed = dataAttr.itemsShowed;
          if (responsive === false) settings.itemsShowed = settings.itemsShowed.md;
        }

        // Other dataAttr
        if (dataAttr.initialState) settings.collapsed = dataAttr.collapsed;
        if (dataAttr.moreText) settings.moreText = dataAttr.moreText;
        if (dataAttr.moreIconClass) settings.moreIconClass = dataAttr.moreIconClass;
        if (dataAttr.toggle) settings.toggle = dataAttr.toggle;
        if (dataAttr.lessText) settings.lessText = dataAttr.lessText;
        if (dataAttr.lessIconClass) settings.lessIconClass = dataAttr.lessIconClass;
        if (dataAttr.transition) settings.transition = dataAttr.transition;
        if (dataAttr.duration) settings.duration = dataAttr.transition;
        if (dataAttr.triggerClass) settings.triggerClass = dataAttr.triggerClass;
        if (dataAttr.mediaQueries) settings.duration = dataAttr.mediaQueries;
        if (dataAttr.showedLines) settings.showedLines = dataAttr.showedLines;


        

        if( responsive === true && settings.mediaQueries !== '' ) {
          mediaQueries.init(settings.mediaQueries);
          $(window).resize(function () {
            mediaQueries.init(settings.mediaQueries);
          });
        }

        var more = {

          _state: undefined,

          init: function () {
            more.validateOptions();
            if (typeof more._state === 'undefined') {
              more._state = settings.initialState;
            }

            more._height = more.height();

            if (typeof more._height === 'number' && more._state === 'collapsed') {
              more.toggle.collapse();
              more.trigger.show();

            } else if (typeof more._height === 'number' && more._state === 'expanded') {
              more.toggle.expand();
              more.trigger.show();

            } else if (typeof more._height !== 'number') {
              more.toggle.expand();
              more.trigger.hide();
              more.trigger.element = '';
            }
          },

          validateOptions: function () {
            if (settings.initialState &&
                settings.initialState !== 'expanded' &&
                settings.initialState !== 'collapsed') {
                  errorShow(4, settings.initialState);
            }
          },

          height: function() {

            if ( $('html').hasClass('media-xs') ) {
              var itemPos = itemPosition(settings.itemsShowed.xs);
            }

            else if ( $('html').hasClass('media-sm') ) {
              var itemPos = itemPosition(settings.itemsShowed.sm);
            }

            else if ( $('html').hasClass('media-md') ) {
              var itemPos = itemPosition(settings.itemsShowed.md);
            }

            else if ( $('html').hasClass('media-lg') ) {
              var itemPos = itemPosition(settings.itemsShowed.lg);
            }

            else if ( $('html').hasClass('media-xlg') ) {
              var itemPos = itemPosition(settings.itemsShowed.xlg);
            }

            else {
              if ( typeof settings.itemsShowed === 'number' ) {
                var itemPos = itemPosition(settings.itemsShowed);
              }
              if ( typeof settings.itemsShowed === 'object' && settings.itemsShowed.hasOwnProperty('md')) {
                var itemPos = itemPosition(settings.itemsShowed.md);
              }
            }

            return itemPos;
          },


          trigger: {

            show: function () {
              if( container.children('.' + settings.triggerClass).length === 0 ) {
                container.append('<a class="'+ settings.triggerClass +'"></a>');
                if( more._state === 'collapsed')
                  more.trigger.collapsedState();
                else
                  more.trigger.expandedState();
              }
            },

            collapsedState: function () {
              var element = more.trigger.element;
              var txt = settings.moreText;
              var icon = settings.moreIconClass;

              container.children('.' + settings.triggerClass).html(function () {
                if( icon !== '' )
                  return txt + '<i class="' + icon + '"></i>';
                else
                  return txt;
              });
            },

            expandedState: function () {
              var element = more.trigger.element;
              var txt = settings.lessText;
              var iconLess = settings.lessIconClass;
              var iconMore = settings.moreIconClass;

              if (iconLess !== '') {
                container.children('.' + settings.triggerClass).html(function () {
                  return txt + '<i class="' + iconLess + '"></i>';
                });

              } else if (iconLess === '' && iconMore !== '') {
                container.children('.' + settings.triggerClass).html(function () {
                  return txt + '<i class="' + iconMore + '"></i>';
                });

              } else {
                container.children('.' + settings.triggerClass).html(function () {
                  return txt;
                });
              }
            },

            hide: function (op) {
              if(container.children('.' + settings.triggerClass).length !== 0) {
                if(op === 'clicked') {
                  var height = '+='+ container.children('.' + settings.triggerClass).height();
                  container.css('margin-bottom', height);
                }
                container.children('.' + settings.triggerClass).remove();
              }
            },

            onClick: function () {
              if (more._state === 'collapsed') {
                more.toggle.expand();
                if (settings.toggle === true)
                  more.trigger.expandedState();
                else
                  more.trigger.hide('clicked');
                more._state = 'expanded';
              } else {
                more.toggle.collapse();
                if (settings.toggle === true)
                  more.trigger.collapsedState();
                else
                  more.trigger.hide('clicked');
                more._state = 'collapsed';
              }
            }
          },


          toggle: {

            collapse: function () {
              var styles = {

                overflow: 'hidden',

                transitionProperty: function() {
                  if(settings.transition === true)
                    return 'max-height';
                  else
                    return 'none';
                },

                transitionDuration: function() {
                  if(settings.transition === true) {
                    return settings.duration + 'ms';
                  }
                },

                maxHeight: more._height + 'px'
              };

              content.css(styles);

              more.collapsed = true;
            },

            expand: function () {

              var contentHeight = function () {
                var height = 0;
                content.children().each(function () {
                  height += $(this).outerHeight(true);
                });
                return height;
              };

              content.css('max-height', contentHeight());

              more.collapsed = false;
            }
          }
        };

        if ($('.more-container').length) {
          $(window).load(function() {
            more.init();
          });
          $(window).resize(function() { more.init(); });
          $(container).on('click', '.'+settings.triggerClass, function (event) {
            event.preventDefault();
            more.trigger.onClick();
          });
        }

      });
    }
  };

}(jQuery));