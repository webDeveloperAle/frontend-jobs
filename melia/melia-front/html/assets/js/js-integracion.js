$(function(){

  function exists(el) {
    if ( $(el).length ) return true;
    else return false;
  }

  $('header.navbar').affix({
    offset: {
      top: function () {
        var el = $('.ibmToolbars');
        if( exists(el) ) {
          return el.height();;
        } else {
          return $('header.navbar').height();;
        }
      }  
    }
  });

  if (exists('.edit-mode')) {
    $('main').css('padding-top', $('header.navbar').height());
  }

});