var Mapa = function (id,marcadores)
{
    this.id = id;
	this.width = '';
	this.height = '';
	this.style = '';
	this.zoom = 13;
	
    this.mostrar = function()
	{
		capa=document.getElementById(this.id);
		if(capa)
		{
			if(this.width!='') capa.style.width=this.width;
			if(this.height!='') capa.style.height=this.height;
			var infowindow;
			var bounds = new google.maps.LatLngBounds() ;
			var image = {
				url: 'assets/img/markerHotel.png',
				size: new google.maps.Size(38, 51),
				origin: new google.maps.Point(0,0),
				anchor: new google.maps.Point(19, 51)
			};
			var shape = {
				coords: [1, 1, 1, 37, 52, 37, 52 , 1],
				type: 'poly'
			};
			mapTypeId: google.maps.MapTypeId.ROADMAP
			var map = new google.maps.Map(document.getElementById(this.id), {mapTypeId: google.maps.MapTypeId.ROADMAP,zoom: 13});
			var markers = [];
			for (var i = 0; i < marcadores.length; i++) {
				var latLng = new google.maps.LatLng(marcadores[i][1],marcadores[i][2]);
				if(marcadores[i][3]) bounds.extend (latLng);
				var marker = new google.maps.Marker({position: latLng,map: map,icon: image,shape: shape,title: marcadores[i][0]});
				(function(i, marker) {
					google.maps.event.addListener(marker,'click',function() {
						if (!infowindow) {
							infowindow = new google.maps.InfoWindow();
						}
						var contentString  = '<div class="map-hotel">';
							contentString += '	<div class="cont">';
							contentString += '		<h6 class="h4"> <a href="">'+marcadores[i][0]+'</a></h6>';
							contentString += '		<div class="img-cont">';
							contentString += '			<img src="assets/img/dummy/dummy-room-grid-04.jpg">';
							contentString += '			<a class="media-icon media-only-icon" data-load-gallery="gallery-id-1" data-load-media="fotos-habitaciones" data-modal-media-tab="fotos" data-modal-id="hotel-x" href="javascript:void(0)"><i class="icon-camera"></i></a>';
							contentString += '		</div>';
							contentString += '		<div class="price">';
							contentString += '			<span class="before">155€</span>';
							contentString += '			<span class="now">';
							contentString += '				<span class="from">desde</span>132,60€';
							contentString += '			</span>';
							contentString += '		</div>';
							contentString += '		<p class="conditions">Precio por habitación y noche. <span>Impuestos incluidos</span></p>';
							contentString += '		<a href="javascript:void(0)" class="btn btn-primary">Ver habitaciones<i class="icon-next"></i></a>';
							contentString += '	</div>';
							contentString += '</div>';
						infowindow.setContent(contentString);
						infowindow.open(map, marker);
					});
				})(i, marker);
				markers.push(marker);
			}
			map.fitBounds (bounds);
			var clusterStyles = [
				{
					anchor:[10,0],
					textSize: 14,
					textColor: '#206699',
					url: 'assets/img/markerGroup3.png',
					height: 45,
					width: 35
				},
				{
					anchor:[15,0],
					textSize: 15,
					textColor: '#206699',
					url: 'assets/img/markerGroup2.png',
					height: 60,
					width: 45
				},
				{
					anchor:[20,0],
					textSize: 16,
					textColor: '#206699',
					url: 'assets/img/markerGroup.png',
					height: 76,
					width: 56
				}
			];
			var mcOptions = {
				styles: clusterStyles,
				minimumClusterSize: 1,
				maxZoom: 13
			};
			var markerCluster = new MarkerClusterer(map, markers,mcOptions);
		}
    }
}