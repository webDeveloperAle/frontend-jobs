// SEARCHER MICE

(function () {
  $('.mice .search input')
    .on('keydown', function () {
      $(this).closest('.form-group').find('.placeholder').hide();
    })
    .on('keyup change', function () {
      if($(this).val().length < 1) {
        $(this).closest('.form-group').find('.placeholder').show();  
      }
    });

  $('.mice .search #destination')
    .on('focus', function () {
      $(this).closest('.search').find('.here').show();
    });

  $('.mice .search .here').on('click', function () {
    $('#destination').closest('.form-group').find('.placeholder').hide();
    $('#destination').val($(this).data('value'));
  });
})();


// SEARCHER BAR MICE

(function () {


  // Searcher interaction
  $('#searched .destination, #searched .dates, #searched .rooms, #searched .modify')
    .hover(
      function () {
        $(this).closest('.reservation-search').addClass('hover');
      },
      function () {
        $(this).closest('.reservation-search').removeClass('hover')
      }
    )
    .click(function () {
      if($('body').hasClass('hotels-selected-open')) {
        $('body').removeClass('hotels-selected-open');
        setTimeout(function() {
          $('main').removeClass('showing-searched').addClass('showing-search');
          if(!!$(this).data('hotel')) {
            $('#destination').val($(this).data('hotel'));
          }
        }, 100);
        setTimeout(function() {
          $('.reservation-search').css('overflow', 'visible');
        }, 500);
      } else {
        $('main').removeClass('showing-searched').addClass('showing-search');
        if(!!$(this).data('hotel')) {
          $('#destination').val($(this).data('hotel'));
        }
        setTimeout(function() {
          $('.reservation-search').css('overflow', 'visible');
        }, 200);
      }
    });
    $('#close-search').click(function () {
      $('.reservation-search').css('overflow', 'hidden');
      $('main').addClass('showing-searched').removeClass('showing-search');
    })

})();


// HOTEL SELECTION

(function () {

  // Same width footer buttons
  $(function (){
    function maxW(object) {
      var maxW = 0;
      $(object).each(function () {
        var w = $(this).outerWidth();
        if (w > maxW) maxW = w;
      });
      return maxW;
    }
    var maxW = maxW($('.hotels-section .mice-hotel-checkout footer .btn'));
    $('.hotels-section .mice-hotel-checkout footer .btn').css('width', maxW);
  });
  
  $('#hotel-selection').modal({
    backdrop: 'static',
    keyboard: false,
    show: false
  });



  //Tooltips
  function stateTooltips() {
    $('[data-tooltip-disabled]').each(function () {
      if ($(this).hasClass('disabled')) {
        $(this).tooltip('enable');
      } else {
        $(this).tooltip('disable');
      }
    });
    
  }

  $('[data-tooltip-disabled]')
    .each(function () {
      $el = $(this);
      $el.tooltip({
        title: function () {
          return $el.data('tooltip-disabled');
        }(),
        container: 'body',
        placement: 'bottom'
      });
    });
  stateTooltips();



  var hotelSelection = {
    init: function (trigger) {
      $(trigger).each(function () {
        hotelSelection.setup(this);
      });
    },

    setup: function (trigger) {
      var model = hotelSelection.getModel();
      $(trigger).click(function () {
        var context = $(this).closest('article');
        var hotel = hotelSelection.getData(context);
        var text = hotelSelection.replaceData(model, hotel);
        hotelSelection.putHotel(text, hotel['hotel_ID']);
        hotelSelection.countSelected();
        hotelSelection.styleAdjusts();
      });
      $(document).on('click', '#hotels-selected .hotel a.delete-hotel', function () {
        var hotel = $(this).closest('.hotel').find('.hotelId').text();
        $('#' + hotel).removeClass('selected');
        $(this).closest('.hotel').remove();
        hotelSelection.countSelected();
        hotelSelection.styleAdjusts();
      });
      $(document).on('click', 'article.hotel a.delete-hotel', function () {
        var hotel = $(this).closest('.hotel').attr('id');
        $('#' + hotel).removeClass('selected');
        $('#hotels-selected').find('#selected-' + hotel).closest('.hotel').remove();
        hotelSelection.countSelected();
      });
    },

    getData: function (context) {
      var hotel = {
        hotel_ID: context.attr('id'),
        name: context.find('.title a').text(),
        stars: context.find('.stars-number').text(),
        starsLetters: function () {
          var stars = context.find('.stars-number').text();
          var toLetters = {1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five'};
          return stars.replace(/1|2|3|4|5/, function (matched) {
            return toLetters[matched];
          });
        }(),
        halls: context.find('.halls-number').text(),
        rooms: context.find('.rooms-number').text(),
        theatre_capacity: context.find('.theatre-capacity').text(),
      };
      return hotel;
    },

    getModel: function () {
      return $('#hotels-selected .model-hotel').html();
    },

    replaceData: function (model, hotel) {
      var hotelKeys = Object.keys(hotel);
      for(var i = 0; i < hotelKeys.length; i++) {
        hotelKeys[i] = '{{' + hotelKeys[i] + '}}';
      }
      var re = new RegExp(hotelKeys.join('|'),"gi");
      return model.replace(re, function (matched) {
        matched = matched.replace(/[{,}]/g, '');
        return hotel[matched];
      });
    },

    putHotel: function (text, hotelID) {
      if(!$('#hotels-selected #selected-' + hotelID).length) {
        $('#hotels-selected .general').append('<li class="hotel col-xs-4">' + text + '</li>');
        $('#' + hotelID).addClass('selected');
      }
    },

    countSelected: function () {
      var c = $('#hotels-selected .hotel').length;
      $('.hotels-counter').html(c).trigger('contentchange');
      if (c > 0) {
        $('#searched button').removeClass('disabled');
      } else {
        $('#searched button').addClass('disabled');
      }
      if (c === 3) {
        $('.modal').addClass('max-hotels');
        $('a.select-hotel').addClass('disabled').closest('.btn-cont').addClass('disabled');
      } else {
        $('.modal').removeClass('max-hotels');
        $('a.select-hotel').removeClass('disabled').closest('.btn-cont').removeClass('disabled');
      }
      stateTooltips();

    },

    styleAdjusts: function () {
      $('#hotels-selected .hotel').removeClass('first middle last');
      $('#hotels-selected .hotel:first').addClass('first');
      $('#hotels-selected .hotel:first').next().addClass('middle');
      $('#hotels-selected .hotel:first').next().next().addClass('last');
    }

  };
  
  hotelSelection.init($('.select-hotel'));



  // $(document).delegate('a.select-hotel', 'cssClassChanged', state);

  $(window).scroll(function () {
    $('[data-tooltip-disabled]').tooltip('hide');
  });


  // Miscellaneous
  $('#hotels-selected-trigger').on('click', function () {
    if (!$('body').hasClass('inTransition')) {
      $('body').toggleClass('hotels-selected-open inTransition');
      setTimeout(function() {
        $('body').removeClass('inTransition');
      }, 300);  
    }
  });

})();


//Remove promo

(function (){

  $('#packs-option li').each(function () {
    var $pack = $(this);
    $pack.find('.icon-close').click(function () {
      $pack.remove();
    });
  });

  // $('#date-option-1').each(function () {
  //   var $dateOp = $(this);
  //   $dateOp.find('.icon-close').click(function () {
  //     $dateOp.remove();
  //   });
  // });

})();

//Condicional eliminar capa de hotel

(function (){

  if ($('#second-hotel-op').length) {
    $('#first-hotel-op').each(function () {
      var $first = $(this);
      var $packTotal = $first.next("div");

      $first.find('.icon-close').click(function () {
        $first.remove();
        $packTotal.remove();
      $('#second-hotel-op .icon-close').hide();
      $('#second-hotel-op .border-top').hide();
      });
    });
  }

})();

(function (){

  if ($('#first-hotel-op').length){
    $('#second-hotel-op').each(function () {
      var $second = $(this);
        $second.find('.icon-close').click(function () {
          $second.remove();
          $('#first-hotel-op .icon-close').hide();
        });
    });
  };
})();


// 4TH STEP RADIO BUTTONS
(function () {
  $('input[type="radio"][data-target]').each(function () {
    $(this).change(function () {
      if ($(this).is(':checked')) {
        var $el = $(this);
        $('.form-tab').hide()
        $($el.data('target')).show();
      }
    });
  });
})();

//Open and close tabs

(function(){

  $(document).on('click', ".row-hotel-facts a.header-link", function(event){
        event.preventDefault();
        $(this).next().slideToggle('slow');
        $(this).closest('.row-hotel-facts').toggleClass('active');
  });

  $(document).on('click', '.row-hotel-charac a.header-link', function(event){
        event.preventDefault();
        $(this).next().slideToggle('slow');
        $(this).closest('.row-hotel-charac').toggleClass('active');
  });

  $(document).on('change', '.food-drinks', function(event){
        $(this).next().slideToggle('slow');
        if(!$('.food-drinks-check').is(':checked')) {
          $('.food-options').find('input[type="checkbox"]').prop('checked', false);
        }
        $(this).closest('.food-drinks').toggleClass('active');
  });

})();


//3RD STEP

(function () {

  // Hide show modules
  var $radios = $('[name="hallsandrooms"]');
  $radios.on('change', function () {
    var target = [];
    $radios.each(function () {
      var $radio = $(this);
      var dataTarget = $(this).data('target-content');
      if(typeof dataTarget === 'object') {
          dataTarget.forEach(function (currentValue) {
            if ( ! $radio.is(':checked') ) {
              if (target.indexOf(currentValue) < 0) {
                $(currentValue).addClass('not-showed');
              }
            } else {
              target.push(currentValue);
              $(currentValue).removeClass('not-showed');
            }
          });
        } else if (typeof dataTarget === 'string') {
          if ( ! $radio.is(':checked') ) {
            if (target.indexOf(dataTarget) < 0) {
              $(dataTarget).addClass('not-showed');
            }
          } else {
            target.push(dataTarget);
            $(dataTarget).removeClass('not-showed');
          }
        }
    });
  });

})();

//mice-hotel-form.php
(function () {

//comment boxes on click
  $('.rooms-form .form-group .custom-checkbox').on('change', function () {
        $(this).next().slideToggle('slow');
        $(this).closest('.rooms-form .form-group .custom-checkbox').toggleClass('active');
  });

  $(document).on('change', '.comments-halls', function () {
        $(this).next().slideToggle('slow');
        $(this).closest('.comments-halls').toggleClass('active');
  });

  $(document).on('change', '.comments-audiovisual', function () {
        $(this).next().slideToggle('slow');
        $(this).closest('.comments-audiovisual').toggleClass('active');
  });

//Nothing tab no content
  $('#halls,#rooms,#hallsRooms').on('click', function () {
      $('.hotel-form').removeClass('not-showed');
  });

  $('#nothing').on('click', function () {
      $('.hotel-form').addClass('not-showed');
  });

//Sum hotel rooms
  $('.type-rooms').change(function () {

    var $context = $(this).closest('fieldset.rooms-counter');
    var sum = 0;

    $context.find('.type-rooms').each(function() {
      if($(this).val()!=""){
        sum += Number($(this).val());
      }
    });
    $context.find($(".total-rooms")).text(sum);
  });

//First hotel room tab opened
  $(document).ready(function() {
      $('.hotel-form .row-hotel-charac:first').addClass('active');
       $('.hotel-content:first').show();
  });

})();


(function () {

  var hallSelection = {

    init: function (context) {
      $(context).each(function () {
        hallSelection.clicks(this);
        hallSelection.addHall(this);
      })
    },

    clicks: function (context) {
      $(context).on('click' ,'.add-hall', function () {
          hallSelection.addHall(context);
        })

      .on('click', '.delete-hall', function () {
          if ($(context).find($('.hall').length > 1)){
            hallSelection.removeHall(this, context);
          } else {
            return false;
          };
      });

    },

    addHall: function (context) {
      var model = hallSelection.getModel(context);
      var c = hallSelection.countHalls(context);
      var hall = hallSelection.replaceData(model, '{{hall-number}}', c+1);
      var hall = hallSelection.replaceData(hall, '{{hall-counter}}', c);
      hallSelection.putHall(context, hall, c);
    },

    putHall: function (context, hall, c) {
      $(context).find($('.halls-cont')).append('<section class="hall clearfix" data-hall-number="' + c + '">' + hall +'</section>');   
      if ($(context).find($('.halls-cont .hall')).length > 1){
        $(context).find($('.delete-hall')).removeClass('hidden');}
    },

    replaceData: function (string, handler, value) {
      var re = new RegExp(handler, "g");
      return string.replace(re, value);
    },

    removeHall: function (trigger, context) {
      var i = $(trigger).closest('[data-hall-number]').data('hall-number');
      $(trigger).closest('.hall').remove();
      hallSelection.renameHall(i, context);
      if ($(context).find($('.halls-cont .hall')).length === 1) {
        $(context).find($('.delete-hall')).addClass('hidden');}
    },

    renameHall: function (index, context) {
      var c = hallSelection.countHalls(context);
      while(index < c) {
        var $hall = $(context).find('section.hall').eq(index);
        $hall.find('.hallNumber').html(function () {
          var text = $(this).html();
          var re = new RegExp(index+2, "g");
          text = text.replace(re, index+1);
          return text;
        });
        $hall.data('hall-number', index);

        $hall.find('[for]').each(function () {
          var forVal = $(this).attr('for');
          var re = new RegExp(index+1, "g");
          forVal = forVal.replace(re, index);
          $(this).attr('for', forVal);
        })

        $hall.find('[id]').each(function () {
          var forVal = $(this).attr('id');
          var re = new RegExp(index+1, "g");
          forVal = forVal.replace(re, index);
          $(this).attr('id', forVal);
        })
        index++;
      }
    },

    getModel: function (context) {
      return $(context).find('.model-hall').html();
    },

    countHalls: function (context) {
      return $(context).find('section.hall').length;
    }

  };
  
  hallSelection.init('.day .halls');



  $(document).on('change', '#all-days', function(container) {
    container.find($('.all-hotels').toggleClass('hidden'));

  });


//Cambio de icono cuando formulario esta lleno.

  var validation = {
    
    type: undefined,

    init: function (c) {
      validation.type = $('input[name="hallsandrooms"]:checked').attr('id');
      validation.setup(c);
    },

    setup: function (c) {
      $(c).each(function () {
        var status = validation.all(this);
        var $status = $(this).find('.status');
        validation.check($status, status);
      });
    },

    all: function (c) {
      var status = false;
      if (validation.type === 'rooms') {
        status = validation.validate.rooms(c);
      } else if (validation.type === 'halls') {
        status = validation.validate.halls(c);
      } else if (validation.type === 'hallsRooms') {
        status = validation.validate.rooms(c);
        if (status === true) {
          status = validation.validate.halls(c);
        }
      }
      return status;
    },

    check: function (o, status) {
      if (status === false) {
        $(o).removeClass('status-check');
      } else {
        $(o).addClass('status-check');
      }
    },

    validate: {
      rooms: function (c) {
        var validation = false;
        $(c).find('.rooms-counter input').each(function () {
          var val = $(this).val();
          if (val !== '' && Number(val) > 0) {
            validation = true;
          }
        });
        return validation;
      },
      halls: function (c) {
        var validation = true;
        $(c).find('.hall input[type="text"], .hall select').each(function () {
          if ($(this).is('input') && $(this).val() === '') {
            validation = false;
          } else if ($(this).is('select') && $(this).val() === null) {
            validation = false;
          }
        });
        return validation;
      },
    }
  };

  validation.init('.row-hotel-charac');
  $(document).on('change', $('.row-hotel-charac input, .row-hotel-charac select'), function (e) {
    var c = $(e.target).closest('.row-hotel-charac');
    validation.init(c);
  });
  $('input[name="hallsandrooms"]').on('change', function () {
    validation.init('.row-hotel-charac');
  });
  $(document).on('click', '.add-hall, .delete-hall', function (e) {
    var c = $(e.target).closest('.row-hotel-charac');
    validation.init(c);
  });


  var copyData = {

    init: function () {
      copyData.setup();
    },

    setup: function () {
      $('#copy-all-days').click(function () {
        var c = $(this).closest('.day');
        var roomsData = copyData.getRooms(c);
        var hallsData = copyData.getHalls(c);
        c = $(c).closest('.hotel-form');
        copyData.copyRooms(c, roomsData);
        copyData.copyHalls(c, hallsData);
        validation.init('.row-hotel-charac');
        $('#copy-all-hotels').show();
      });
      $('#copy-all-hotels').click(function () {
        var c = $(this).closest('.day');
        var roomsData = copyData.getRooms(c);
        var hallsData = copyData.getHalls(c);
        c = $(c).closest('form').find('.hotel-form');
        copyData.copyRooms(c, roomsData);
        copyData.copyHalls(c, hallsData);
        validation.init('.row-hotel-charac');
      });
    },

    getRooms: function (c) {
      var rooms = {};
      rooms['single'] = $(c).find('[data-ID="single"]').val();
      rooms['double'] = $(c).find('[data-ID="double"]').val();
      rooms['suite'] = $(c).find('[data-ID="suite"]').val();
      rooms['total'] = $(c).find('.total-rooms').text();
      rooms['comments'] = ($(c).find('.room-comments-enabler').is(':checked')) ? true : false;
      rooms['commentsContent'] = $(c).find('.room-comments textarea').val();
      return rooms;
    },

    getHalls: function (c) {
      var hallsData = {};
      var halls = [];
      
      var num = $(c).find('.halls .hall').length;
      for (var i = 0; i < num; i++) {
        var hall = {};
        hall['mount'] = $(c).find('.hall').eq(i).find('select').val();
        hall['assistants'] = $(c).find('.hall').eq(i).find('input').val();
        hall['comments'] = $(c).find('.hall').eq(i).find('.hall-comments-enabler').prop('checked');
        hall['commentsContent'] = $(c).find('.hall').eq(i).find('.user-comments textarea').val();
        hall['audiovisual'] = $(c).find('.hall').eq(i).find('.audiovisual-comments-enabler').prop('checked');
        hall['audiovisualContent'] = $(c).find('.hall').eq(i).find('.audiovisual-comments textarea').val();
        halls.push(hall);
      }
      hallsData['halls'] = halls;


      hallsData['foodDrinks'] = $(c).find('.food-drinks-check').prop('checked');
      if(hallsData['foodDrinks']) {
        hallsData['foodDrinksChoices'] = [];
        $(c).find('.food-options :checked').each(function () {
            hallsData['foodDrinksChoices'].push($(this).data('type'));
        });
      }
      return hallsData;
    },
 
    copyRooms: function (c, rooms) {
      $(c).find('[data-ID="single"]').val(rooms.single);
      $(c).find('[data-ID="double"]').val(rooms.double);
      $(c).find('[data-ID="suite"]').val(rooms.suite);
      $(c).find('.total-rooms').text(rooms.total);
      $(c).find('.room-comments textarea').val(rooms.commentsContent);
      if(rooms.comments) {
        $(c).find('.room-comments-enabler').prop('checked', true);
        $(c).find('.room-comments').show();
      } else {
        $(c).find('.room-comments-enabler').prop('checked', false);
        $(c).find('.custom-checkbox').addClass('active')
        $(c).find('.room-comments').closest('.comment-form').hide();
      }
    },

    copyHalls: function (c, hallsData) {
      var halls = hallsData.halls;
      if (halls.length === 1) $('.delete-hall').addClass('hidden');
      $(c).find('.day').find('.halls .hall').remove();
      for (var i = 0; i < halls.length; i++) {
        var context = $(c).find('.day');
        context.each(function (index, el) {
          var context = $(el).find('.halls');
          hallSelection.addHall(context);
          var context = $(context).find('[data-hall-number=' + i + ']');
          $(context).find('select').val(halls[i].mount);
          $(context).find('input').val(halls[i].assistants);
          $(context).find('.user-comments textarea').val(halls[i].commentsContent);
          $(context).find('.audiovisual-comments textarea').val(halls[i].audiovisualContent);
          if(halls[i].comments) {
            $(context).find('.hall-comments-enabler').prop('checked', true);
            $(context).find('.user-comments').show();
          } else {
            $(context).find('.hall-comments-enabler').prop('checked', false);
            $(context).find('.user-comments').hide();
          }
          if(halls[i].audiovisual) {
            $(context).find('.audiovisual-comments-enabler').prop('checked', true);
            $(context).find('.audiovisual-comments').show();
          } else {
            $(context).find('.audiovisual-comments-enabler').prop('checked', false);
            $(context).find('.audiovisual-comments').hide();
          }
        });
      };
      hallsData['foodDrinks'] = $(c).find('.food-drinks-check').prop('checked');
      if(hallsData['foodDrinks']) {
        $(c).find('.food-options').show();
        $(c).find('.food-drinks-check').prop('checked', true);
          var choices = hallsData['foodDrinksChoices'];
          $(c).find('.day [data-type]').prop('checked', false);
          for (var i = 0; i < choices.length; i++) {
            $(c).find('[data-type="' + choices[i] + '"]').prop('checked', true);
          }
      } else {
        $(c).find('.food-options').hide();
        $('.food-drinks-check').prop('checked', false);
        $(c).find('[data-type]').prop('checked', false);
      }
    }
  };

  copyData.init();

})();