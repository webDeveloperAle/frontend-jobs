function openModalMedia(options) {
    var options = options || {};
    var $link = $("<a />").data(options);
    $(document).trigger('modalMedia.open', [$link]);
}

function closeModalMedia() {
    $(document).trigger('modalMedia.close');
}

(function(){

    var $modalMedia = $("#modal-media");
    var $navTabs = $modalMedia.find(".nav-tabs");
    var $tabContent = $modalMedia.find(".tab-content");
    var $galleryIntro = $modalMedia.find(".grid-gallery-intro");
    var $galleryWrapper = $modalMedia.find(".grid-gallery-wrapper");
    var $galleryVideoWrapper = $modalMedia.find(".grid-gallery-video-wrapper");

    var showModalPage = function(page) {
        /*
        page = 1: vista inicial
        page = 2: intro
        page = 3: gallery
        page = 4: video gallery
        */
        switch(page) {
            case 1:
                $tabContent.slideDown("slow");
                $galleryIntro.slideUp("slow");
                $galleryWrapper.slideUp("slow");
                $galleryVideoWrapper.slideUp("slow");
            break;
            case 2:
                $tabContent.slideUp("slow");
                $galleryIntro.slideDown("slow");
                $galleryWrapper.slideUp("slow");
                $galleryVideoWrapper.slideUp("slow");
            break;
            case 3:
                $tabContent.slideUp("slow");
                $galleryIntro.slideUp("slow");
                $galleryWrapper.slideDown("slow");
                $galleryVideoWrapper.slideUp("slow");
            break;
            case 4:
                $tabContent.slideUp("slow");
                $galleryIntro.slideUp("slow");
                $galleryWrapper.slideUp("slow");
                $galleryVideoWrapper.slideDown("slow");
            break;
        }
    }

    var openModalIfClosed = function(relatedTarget) {
        var isOpen = ($modalMedia.data('bs.modal') || {isShown: false}).isShown;
        if (!isOpen) {
            $modalMedia.modal('show', relatedTarget);
        }
    };

    var showTab = function(tabNumber) {
        $navTabs.find("a[href='#tab-"+tabNumber+"']").tab('show');
    };

    /* handler que abre el modal */
    $(document).on(
        "click",
        "a[data-modal-id]",
        function(event){
            event.preventDefault();
            openModal($(this));
        }
    );

    $(document).on('modalMedia.open', function(event, $link){
        openModal($link);
    });

    $(document).on('modalMedia.close', function(event, $link){
        $modalMedia.modal('hide');
    });

    var openModal = function($link) {
        openModalIfClosed($link);

        var data = $link.data();

        var showModalPageNumber = 1;

        if (data.loadMedia) {
            showModalPageNumber = 2;
        }

        if (data.loadGallery) {
            showModalPageNumber = 3;
        }

        if (data.loadVideo) {
            showModalPageNumber = 4;
        }

        if (data.modalMediaTab) {
            showTab(data.modalMediaTab);
        }

        showModalPage(showModalPageNumber);
    };

    $(document).on(
        "click",
        "#modal-media a[data-modal-back]",
        function(event) {
            event.preventDefault();

            var backToPage = $(this).data("modal-back");
            showModalPage(backToPage);
        }
    );

    $navTabs.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        showModalPage(1);
    });

    var updateCarouselCount = function() {
        var $carouselMedia = $('#carousel-media');
        var $items = $carouselMedia.find(".item");
        var currentIndex = $items.index($items.filter(".active"));
        var total = $items.length;

        var text = (currentIndex + 1) + " / " + total;

        $('.carousel-media-counter').text(text);
    };

    $(document).on('slid.bs.carousel', '#carousel-media', updateCarouselCount);

})();