(function(){
  $('.aside-mispuntos2 .editar-datos').on('click', function () {
    var $parent = $(this).closest('.aside-mispuntos2 ');

    $parent.find(".image-form").slideUp();
    $parent.find($(this).data("change-picture") || "change-picture").slideDown();
    $parent.find(".change-picture").slideDown();
    $parent.find(".editar-datos").css("display","block");

  });

  $('.aside-mispuntos2 .btn-cancelar').on('click btn-cancelar.toggle', function () {
    var $parent = $(this).closest('.aside-mispuntos2');
    $parent.find(".image-form").slideDown();
    $parent.find(".change-picture").slideUp();
    $parent.find("form").slideUp('slow', function() {
        $parent.find(".editar-datos").css("display","block");
        $parent.find(".change-picture").css("display","block");
    });
  });

})();

(function () {

  $('a[data-scroll-to]').on('click', function () {
    var fixedEl = ['header', 'section.rwdpro-main'];
    scrollToAnchor($(this).data('scroll-to'), fixedEl);
    return false;
  })

})();

// SEARCHER MICE

(function () {
  $('.mice .search input')
    .on('keydown', function () {
      $(this).closest('.form-group').find('.placeholder').hide();
    })
    .on('keyup change', function () {
      if($(this).val().length < 1) {
        $(this).closest('.form-group').find('.placeholder').show();  
      }
    });

  $('.mice .search #destination')
    .on('focus', function () {
      $(this).closest('.search').find('.here').show();
    });

  $('.mice .search .here').on('click', function () {
    $('#destination').closest('.form-group').find('.placeholder').hide();
    $('#destination').val($(this).data('value'));
  });
})();