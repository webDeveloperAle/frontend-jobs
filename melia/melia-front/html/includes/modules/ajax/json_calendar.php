<?php

/*

JSON Config:

$data = [
            monthNumber => [
                dayNumber => [
                    'price' => '...',
                    'class' => 'better-price|no-disponibility|current-selected-date',
                    'tooltip' => '...',
                    'bottom-text' => '...'
                ]
            ],
            ...
        ];
*/

$today = date('j');
$currentMonth = date('n');

$months = [$currentMonth, $currentMonth == 12 ? 1 : $currentMonth+1];

foreach($months as $month) {

    foreach(range(1, 31) as $day) {

        if ($month == $currentMonth && $day < $today) {
            continue;
        }

        $data[$month][$day] = ['price' => '165\20AC'];
        if ($day %5 == 0) {
            $data[$month][$day]['class'] = 'better-price';
        }
        if ($day %4 == 0) {
            $data[$month][$day]['class'] = 'no-disponibility';
        }
        if ($day %6 == 0) {
            $data[$month][$day]['class'] = $data[$month][$day]['price'] = '';
            $data[$month][$day]['tooltip'] = 'Para entrar este dia necesitas una estancia mínima de <strong>4 noches</strong>.';
            $data[$month][$day]['disabled-text'] = $day % 4 == 0 ? 'Mínimo de noches' : 'Fecha de entrada no disponible';
        }
        if ($day == $today && $month == $currentMonth) {
            $data[$month][$day]['class'] = 'current-selected-date';
        }
    }

}

echo json_encode($data);